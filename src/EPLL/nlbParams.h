#ifndef NLBPARAMS_H_INCLUDED
#define NLBPARAMS_H_INCLUDED

namespace VideoNLB
{

/**
 * @brief Structures of parameters dedicated to NL-Bayes process
 *
 * @param sigma: value of noise;
 * @param sizePatch: size of patches (sizePatch x sizePatch);
 * @param nSimilarPatches: minimum number of similar patches wanted;
 * @param sizeSearchWindow: size of the search window around the reference patch;
 * @param boundary: must be > sizeSearchWindow. Boundary kept around sub-images when the image is
 *        subdivided for parallelization;
 * @param offSet: step between two similar patches;
 * @param useHomogeneousArea: if true, use the homogeneous area trick;
 * @param gamma: threshold to detect homogeneous area;
 * @param beta: parameter used to estimate the covariance matrix;
 * @param tau: parameter used to determine similar patches;
 * @param isFirstStep: true if the first step of the algorithm is wanted;
 * @param doPasteBoost: if true, patches near denoised similar patches will not be used as reference
 *        patches;
 * @param verbose: if true, print informations.
 **/
struct nlbParams
{
	float sigma;
	unsigned sizePatch;        // depends on sigma
	unsigned sizePatchTime;    // user given
	unsigned nSimilarPatches;  // depends on sigma, sizeSearchTimeRange (1 channel) or sizePatch (3 channels)
	unsigned sizeSearchWindow; // depends on nSimilarPatches
	unsigned sizeSearchTimeRangeFwd; // how many forward  frames in search cube
	unsigned sizeSearchTimeRangeBwd; // how many backward frames in search cube
	unsigned boundary;         // depends on sizeSearchWindow
	unsigned offSet;           // depends on sizePatch
	unsigned offSetTime;       // depends on sizePatchTime
	bool useHomogeneousArea;
	float gamma;
	unsigned rank;             // rank of covariance matrix
	float beta;                // depends on sigma
	float tau;                 // depends on sizePatch
	bool isFirstStep;
	bool doPasteBoost;
	bool verbose;

	// Parameters for the multiscale denoising
	int scales; 		   // Number of scales to use
	float ratio;		   // Ratio between the scales

	// Parameters for the trees
	int type;		   // type of the tree to be used (0: local search,  1: VP-tree, 2: KD-tree)
	int reranking;		   // if reranking or not
	int manager;		   // type of patch manager (0: classic, 1: dimensionality reduction)
	int nbTrees;		   // number of trees to be used in the forest
	float flat;

	float weightGamma;
	bool useAggregWeights;
};
}

#endif
