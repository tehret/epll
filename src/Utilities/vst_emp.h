//
// TOP
//

#ifndef VSTEMP_HEADER_H
#define VSTEMP_HEADER_H

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "LibVideoT.hpp"

#define SCALE 1

// VST based on ponomarenko curves

class VST {
	public:
		static void dirVst(Video<float>& input, std::vector<std::vector<std::pair<float, float> > >& noise_curve)
		{
			for(unsigned x = 0; x < input.sz.width; ++x)
			for(unsigned y = 0; y < input.sz.height; ++y)
			for(unsigned t = 0; t < input.sz.frames; ++t)
			for(unsigned c = 0; c < input.sz.channels; ++c) // In our case channels=1
			{
				float temp = 0;
				int i = 0;
				if(input(x,y,t,c) < noise_curve[t][0].first)
					temp += (input(x,y,t,c)/noise_curve[t][0].second);
				else
				{
					temp += (noise_curve[t][0].first/noise_curve[t][0].second);
					while((i+1 < noise_curve[t].size()) && (input(x,y,t,c) > noise_curve[t][i+1].first))
					{
						temp += ((noise_curve[t][i+1].first - noise_curve[t][i].first)/noise_curve[t][i].second);
						++i;
					}
					temp += ((input(x,y,t,c) - noise_curve[t][i].first)/noise_curve[t][i].second);
				}
				input(x,y,t,c) = 15*temp;
			}
		};

		static void invVst(Video<float>& input, float a, float b, float mini)
		{
			// TODO
		};
};
#endif
