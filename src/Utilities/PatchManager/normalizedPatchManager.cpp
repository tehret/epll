/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file normalizedPatchManager.cpp
 * @brief Patch manager using normalized patches (with different type of mean and variance) 
 *
 * @author Thibaud Ehret <ehret.thibaud@gmail.com>
 **/

#include "normalizedPatchManager.h"

NormalizedPatchManager::NormalizedPatchManager(Video<float> const& video, int sizeP, int sizePt, int channels, int tpMean, int tpVar, int sigma_, float flat)
{
	vid = &video;
	sizePatch = sizeP;
	sizePatchTime = sizePt;
	nbChannels = channels;

	typeNormMean = tpMean;
	typeNormVar = tpVar;
	sigma = sigma_;

	// Compute the mean and the variance of the patches
	int width = vid->sz.width - sizeP + 1;
	int height = vid->sz.height - sizeP + 1;
	int time = vid->sz.frames - sizePt + 1;

	meanCoeff.resize(width, height, vid->sz.frames, nbChannels);
	varCoeff.resize(width, height, time, nbChannels);

	std::vector<unsigned> allPatches = std::vector<unsigned>(width * height * time);

	for(unsigned px = 0, i = 0; px < width; ++px)
		for(unsigned py = 0; py < height; ++py)
			for(unsigned pt = 0; pt < ((typeNormMean == 2) ? vid->sz.frames : time); ++pt, ++i)
				for(unsigned pc = 0; pc < nbChannels; ++pc)
				{
					float mean = 0.f;
						for (unsigned hy = 0, u = 1; hy < sizeP; hy++)
							for (unsigned hx = 0; hx < sizeP; hx++, ++u)
							{
								mean += ((*vid)(px + hx, py + hy, pt, pc) - mean) / (float)u;
							}
					meanCoeff(px,py,pt,pc) = mean;
				}




	// Here we compute the threshold for the chi square test TODO
	threshold = 0;//boost::math::gamma_p_inv((sizeP * sizeP * sizePt * channels - sizePt) / 2, flat) * 2;
}

void NormalizedPatchManager::getAllPatches(std::vector<unsigned>& allPatches)
{
	int width = vid->sz.width - sizePatch + 1;
	int height = vid->sz.height - sizePatch + 1;
	int time = vid->sz.frames - sizePatchTime + 1;
	for(unsigned px = 0, i = 0; px < width; ++px)
		for(unsigned py = 0; py < height; ++py)
			for(unsigned pt = 0; pt < time; ++pt, ++i)
				allPatches[i] = vid->sz.index(px, py, pt, 0);
}

int NormalizedPatchManager::getNbPatches()
{
	int width = vid->sz.width - sizePatch + 1;
	int height = vid->sz.height - sizePatch + 1;
	int time = vid->sz.frames - sizePatchTime + 1;
	return width * height * time;
}

float NormalizedPatchManager::distance(unsigned patch1, unsigned patch2)
{

	unsigned px, py, pt, pc;
	vid->sz.coords(patch1, px, py, pt, pc);

	unsigned qx, qy, qt, qc; 
	vid->sz.coords(patch2, qx, qy, qt, qc);

	const int sPx = sizePatch;
	const int sPt = sizePatchTime;

	float dist = 0.f, dif;
	for (unsigned hc = 0; hc < nbChannels; ++hc)
		for (unsigned ht = 0; ht < sPt; ht++)
			for (unsigned hy = 0; hy < sPx; hy++)
				for (unsigned hx = 0; hx < sPx; hx++)
				{
					//dist += (dif = ((*vid)(px + hx, py + hy, pt + ht, hc) -  meanCoeff(px, py, (typeNormMean == 2) ? pt : pt + ht, 0)/ varCoeff(px,py,(typeNormVar == 2) ? pt : pt + ht,0))
					//		- ((*vid)(qx + hx, qy + hy, qt + ht, hc) - meanCoeff(qx, qy, (typeNormMean == 2) ? qt : qt + ht, 0)) / varCoeff(qx,qy,(typeNormVar == 2) ? qt : qt + ht,0)) * dif;
					dist += (dif = (((*vid)(px + hx, py + hy, pt + ht, hc) -  meanCoeff(px, py, pt + ht, hc)) - ((*vid)(qx + hx, qy + hy, qt + ht, hc) - meanCoeff(qx, qy, qt + ht, hc)))) * dif;

				}
	// FIXME Modify the coefficient 255.f which is not valid when var norm
	return std::sqrt(dist / (sPt * sPx * sPx * nbChannels)) / 255.f;
}

// In this case it is exactly the same as distance since all is done with the original position for the patches
float NormalizedPatchManager::distanceOrigPos(unsigned patch1, unsigned patch2)
{
	unsigned px, py, pt, pc;
	vid->sz.coords(patch1, px, py, pt, pc);

	unsigned qx, qy, qt, qc; 
	vid->sz.coords(patch2, qx, qy, qt, qc);

	const int sPx = sizePatch;
	const int sPt = sizePatchTime;

	float dist = 0.f, dif;
	for (unsigned hc = 0; hc < nbChannels; ++hc)
		for (unsigned ht = 0; ht < sPt; ht++)
			for (unsigned hy = 0; hy < sPx; hy++)
				for (unsigned hx = 0; hx < sPx; hx++)
				{
					//dist += (dif = ((*vid)(px + hx, py + hy, pt + ht, hc) -  meanCoeff(px, py, (typeNormMean == 2) ? pt : pt + ht, 0)/ varCoeff(px,py,(typeNormVar == 2) ? pt : pt + ht,0))
					//		- ((*vid)(qx + hx, qy + hy, qt + ht, hc) - meanCoeff(qx, qy, (typeNormMean == 2) ? qt : qt + ht, 0)) / varCoeff(qx,qy,(typeNormVar == 2) ? qt : qt + ht,0)) * dif;
					dist += (dif = (((*vid)(px + hx, py + hy, pt + ht, hc) -  meanCoeff(px, py, pt + ht, hc)) - ((*vid)(qx + hx, qy + hy, qt + ht, hc) - meanCoeff(qx, qy, qt + ht, hc)))) * dif;
				}


	// FIXME Modify the coefficient 255.f which is not valid when var norm
	return std::sqrt(dist / (sPt * sPx * sPx * nbChannels)) / 255.f;
}

float NormalizedPatchManager::distanceLocal(unsigned patch1, unsigned patch2)
{
	unsigned px, py, pt, pc;
	vid->sz.coords(patch1, px, py, pt, pc);

	unsigned qx, qy, qt, qc; 
	vid->sz.coords(patch2, qx, qy, qt, qc);

	const int sPx = sizePatch;
	const int sPt = sizePatchTime;

	float dist = 0.f, dif;
	for (unsigned hc = 0; hc < nbChannels; ++hc)
		for (unsigned ht = 0; ht < sPt; ht++)
			for (unsigned hy = 0; hy < sPx; hy++)
				for (unsigned hx = 0; hx < sPx; hx++)
				{
					dist += (dif = (((*vid)(px + hx, py + hy, pt + ht, hc) -  meanCoeff(px, py, pt + ht, hc)) - ((*vid)(qx + hx, qy + hy, qt + ht, hc) - meanCoeff(qx, qy, qt + ht, hc)))) * dif;
				}


	// FIXME Modify the coefficient 255.f which is not valid when var norm
	return std::sqrt(dist / (sPt * sPx * sPx * nbChannels)) / 255.f;
}

void NormalizedPatchManager::loadGroupStep1(std::vector<std::vector<float> >& group, std::vector<unsigned>& list, int kNN, unsigned pidx)
{
	for (unsigned c  = 0; c < vid->sz.channels; c++)
		for (unsigned ht = 0, k = 0; ht < sizePatchTime; ht++)
			for (unsigned hy = 0;        hy < sizePatch; hy++)
				for (unsigned hx = 0;        hx < sizePatch; hx++)
					for (unsigned n = 0; n < kNN; n++, k++)
					{
						//group[c][k] = (((*vid)(c * vid->sz.wh + list[n] + ht * vid->sz.whc + hy * vid->sz.width + hx) - meanCoeff(c * vid->sz.wh + list[n] + (typeNormMean == 2) ? ht * vid->sz.whc : 0)) / varCoeff(c * vid->sz.wh + list[n] + (typeNormMean == 2) ? ht * vid->sz.whc : 0) + meanCoeff(c * vid->sz.wh + (typeNormMean == 2) ? ht * vid->sz.whc : 0 + pidx)) * varCoeff(c * vid->sz.wh + (typeNormMean == 2) ? ht * vid->sz.whc : 0 + pidx);
						group[c][k] = (((*vid)(c * vid->sz.wh + list[n] + ht * vid->sz.whc + hy * vid->sz.width + hx) - meanCoeff(c * vid->sz.wh + list[n] + (typeNormMean == 2) ? ht * vid->sz.whc : 0)) + meanCoeff(c * vid->sz.wh + (typeNormMean == 2) ? ht * vid->sz.whc : 0 + pidx));
					}
}

// FIXME Normalization of the pathc of the noisy video using a good approximation (for now we are using the coefficients of the firrst approximation step)
void NormalizedPatchManager::loadGroupStep2(std::vector<float>& group, std::vector<unsigned>& list, Video<float> const& v, std::vector<float>& group2, int kNN, unsigned pidx)
{
	for (unsigned c  = 0, k = 0; c < v.sz.channels; c++)
		for (unsigned ht = 0; ht < sizePatchTime; ht++)
			for (unsigned hy = 0;        hy < sizePatch; hy++)
				for (unsigned hx = 0;        hx < sizePatch; hx++)
					for (unsigned n = 0; n < kNN; n++, k++)
					{
						//group[k] = (((*vid)(c * vid->sz.wh + list[n] + ht * vid->sz.whc + hy * vid->sz.width + hx) - meanCoeff(c * vid->sz.wh + list[n] + (typeNormMean == 2) ? ht * vid->sz.whc : 0)) / varCoeff(c * vid->sz.wh + list[n] + (typeNormMean == 2) ? ht * vid->sz.whc : 0) + meanCoeff(c * vid->sz.wh + (typeNormMean == 2) ? ht * vid->sz.whc : 0 + pidx)) * varCoeff(c * vid->sz.wh + (typeNormMean == 2) ? ht * vid->sz.whc : 0 + pidx);
						//group2[k] = ((v(c * vid->sz.wh + list[n] + ht * vid->sz.whc + hy * vid->sz.width + hx) - meanCoeff(c * vid->sz.wh + list[n] + (typeNormMean == 2) ? ht * vid->sz.whc : 0)) / varCoeff(c * vid->sz.wh + list[n] + (typeNormMean == 2) ? ht * vid->sz.whc : 0) + meanCoeff(c * vid->sz.wh + (typeNormMean == 2) ? ht * vid->sz.whc : 0 + pidx)) * varCoeff(c * vid->sz.wh + (typeNormMean == 2) ? ht * vid->sz.whc : 0 + pidx);
						group[k] = (((*vid)(c * vid->sz.wh + list[n] + ht * vid->sz.whc + hy * vid->sz.width + hx) - meanCoeff(c * vid->sz.wh + list[n] + (typeNormMean == 2) ? ht * vid->sz.whc : 0)) + meanCoeff(c * vid->sz.wh + (typeNormMean == 2) ? ht * vid->sz.whc : 0 + pidx));
						group2[k] = ((v(c * vid->sz.wh + list[n] + ht * vid->sz.whc + hy * vid->sz.width + hx) - meanCoeff(c * vid->sz.wh + list[n] + (typeNormMean == 2) ? ht * vid->sz.whc : 0)) + meanCoeff(c * vid->sz.wh + (typeNormMean == 2) ? ht * vid->sz.whc : 0 + pidx));
					}
}

int NormalizedPatchManager::getDim()
{
	return sizePatch * sizePatch * sizePatchTime * nbChannels;
}

float NormalizedPatchManager::getValueAtDim(unsigned id, unsigned dim)
{

	unsigned px, py, pt, pc;
	vid->sz.coords(id, px, py, pt, pc);

	unsigned hx, hy, ht, hc;
	unsigned l = dim;
	hx = l % sizePatch;
	l /= sizePatch;
	hy = l % sizePatch;
	l /= sizePatch;
	ht = l % sizePatchTime;
	l /= sizePatchTime;
	hc = l;

	//return ((*vid)(px + hx, py + hy, pt + ht, hc) - meanCoeff(px, py, pt, 0)) / varCoeff(px, py, pt, 0);
	return ((*vid)(px + hx, py + hy, pt + ht, hc) - meanCoeff(px, py, pt, hc));

}

float NormalizedPatchManager::realDistance(unsigned patch1, unsigned patch2)
{
	unsigned px, py, pt, pc;
	vid->sz.coords(patch1, px, py, pt, pc);

	unsigned qx, qy, qt, qc; 
	vid->sz.coords(patch2, qx, qy, qt, qc);

	const int sPx = sizePatch;
	const int sPt = sizePatchTime;

	float dist = 0.f, dif;
	for (unsigned hc = 0; hc < nbChannels; ++hc)
		for (unsigned ht = 0; ht < sPt; ht++)
			for (unsigned hy = 0; hy < sPx; hy++)
				for (unsigned hx = 0; hx < sPx; hx++)
				{
					//dist += (dif = ((*vid)(px + hx, py + hy, pt + ht, hc) -  meanCoeff(px, py, (typeNormMean == 2) ? pt : pt + ht, 0)/ varCoeff(px,py,(typeNormVar == 2) ? pt : pt + ht,0))
					//		- ((*vid)(qx + hx, qy + hy, qt + ht, hc) - meanCoeff(qx, qy, (typeNormMean == 2) ? qt : qt + ht, 0)) / varCoeff(qx,qy,(typeNormVar == 2) ? qt : qt + ht,0)) * dif;
					dist += (dif = (((*vid)(px + hx, py + hy, pt + ht, hc) -  meanCoeff(px, py, pt + ht, hc)) - ((*vid)(qx + hx, qy + hy, qt + ht, hc) - meanCoeff(qx, qy, qt + ht, hc)))) * dif;

				}
	// FIXME Modify the coefficient 255.f which is not valid when var norm
	return std::sqrt(dist / (sPt * sPx * sPx * nbChannels)) / 255.f;
}

float NormalizedPatchManager::getValueAtDimLocal(unsigned id, unsigned dim)
{
	unsigned px, py, pt, pc;
	vid->sz.coords(id, px, py, pt, pc);

	unsigned hx, hy, ht, hc;
	unsigned l = dim;
	hx = l % sizePatch;
	l /= sizePatch;
	hy = l % sizePatch;
	l /= sizePatch;
	ht = l % sizePatchTime;
	l /= sizePatchTime;
	hc = l;

	//return ((*vid)(px + hx, py + hy, pt + ht, hc) - meanCoeff(px, py, pt, 0)) / varCoeff(px, py, pt, 0);
	return ((*vid)(px + hx, py + hy, pt + ht, hc) - meanCoeff(px, py, pt, hc));
}

int NormalizedPatchManager::getType()
{
	return 2;
}

void NormalizedPatchManager::computeRealDistance(std::vector<float>& distances, std::vector<unsigned> patches, unsigned pidx)
{
	for(int i = 0; i < patches.size(); ++i)
	{
		distances[i] = distance(pidx, patches[i]);
	}
}

unsigned NormalizedPatchManager::getNeighboringPatch(unsigned patch, int type, int sp, int st)
{
	unsigned px,py,pt,pc;
	vid->sz.coords(patch, px, py, pt, pc);

	int width = vid->sz.width - sizePatch + 1;
	int height = vid->sz.height - sizePatch + 1;
	int time = vid->sz.frames - sizePatchTime + 1;

	if(type == 0)
	{
		if(px+sp < width)
			return vid->sz.index(px+sp,py,pt,0);
		return patch;
	}
	else if(type == 1)
	{
		if(py+sp < height)
			return vid->sz.index(px,py+sp,pt,0);
		return patch;
	}
	else if(type == 2)
	{
		if(pt+sp < time)
			return vid->sz.index(px,py,pt+st,0);
		return patch;
	}
	else if(type == 3)
	{
		if(((int)px)-sp >= 0)
			return vid->sz.index(px-sp,py,pt,0);
		return patch;
	}
	else if(type == 4)
	{
		if(((int)py)-sp >= 0)
			return vid->sz.index(px,py-sp,pt,0);
		return patch;
	}
	else if(type == 5)
	{
		if(((int)pt)-st >= 0)
			return vid->sz.index(px,py,pt-st,0);
		return patch;
	}
	else
	{
		// This type of propagation doesn't exist ...
		return 0;
	}
}

unsigned NormalizedPatchManager::getInverseNeighboringPatch(unsigned patch, int type, int sp, int st)
{
	unsigned px,py,pt,pc;
	vid->sz.coords(patch, px, py, pt, pc);

	int width = vid->sz.width - sizePatch + 1;
	int height = vid->sz.height - sizePatch + 1;
	int time = vid->sz.frames - sizePatchTime + 1;

	if(type == 3)
	{
		if(px+sp < width)
			return vid->sz.index(px+sp,py,pt,0);
		return patch;
	}
	else if(type == 4)
	{
		if(py+sp < height)
			return vid->sz.index(px,py+sp,pt,0);
		return patch;
	}
	else if(type == 5)
	{
		if(pt+sp < time)
			return vid->sz.index(px,py,pt+st,0);
		return patch;
	}
	else if(type == 0)
	{
		if(((int)px)-sp >= 0)
			return vid->sz.index(px-sp,py,pt,0);
		return patch;
	}
	else if(type == 1)
	{
		if(((int)py)-sp >= 0)
			return vid->sz.index(px,py-sp,pt,0);
		return patch;
	}
	else if(type == 2)
	{
		if(((int)pt)-st >= 0)
			return vid->sz.index(px,py,pt-st,0);
		return patch;
	}
	else
	{
		// This type of propagation doesn't exist ...
		return 0;
	}
}

void NormalizedPatchManager::getNeighbors(std::vector<std::pair<unsigned, int> >& listNeighbors, unsigned pidx)
{
	unsigned px,py,pt,pc;
	vid->sz.coords(pidx, px, py, pt, pc);

	if(px + sizePatch/2 <= vid->sz.width - sizePatch)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px + sizePatch/2, py, pt, 0), 0));
	}	
	else if(py + sizePatch/2 <= vid->sz.height - sizePatch)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px, py + sizePatch/2, pt, 0), 1));
	}
	else if(pt + sizePatchTime/2 <= vid->sz.frames - sizePatchTime)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px, py, pt + sizePatchTime/2, 0), 2));
	}
	else if(px - sizePatch/2 >= 0)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px - sizePatch/2, py, pt, 0), 3));
	}
	else if(py - sizePatch/2 >= 0)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px, py - sizePatch/2, pt, 0), 4));
	}
	else if(pt - sizePatchTime/2 >= 0)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px, py, pt - sizePatchTime/2, 0), 5));
	}
}

// Not defined because it is not used at the moment (only for the DR case)
int NormalizedPatchManager::hashPatch(unsigned pidx, bool useRot, std::vector<std::vector<float> >& rot, int nbTop, int& secondBin, std::vector<std::pair<int,int> >& otherPossibilities, bool getMorePatches)
{
	// TODO
	secondBin = 0;
	return 0;
}

bool NormalizedPatchManager::chiSquareTest(unsigned pidx, bool verbose)
{
	unsigned px, py, pt, pc;
	vid->sz.coords(pidx, px, py, pt, pc);

	const int sPx = sizePatch;
	const int sPt = sizePatchTime;

	float norm = 0.f, dif;
	for (unsigned hc = 0; hc < nbChannels; ++hc)
		for (unsigned ht = 0; ht < sPt; ht++)
			for (unsigned hy = 0; hy < sPx; hy++)
				for (unsigned hx = 0; hx < sPx; hx++)
					norm += ((dif = ((*vid)(px + hx, py + hy, pt + ht, hc) -  meanCoeff(px, py, (typeNormMean != 2) ? pt : pt + ht, hc))) * dif);

	if(verbose)
		printf("Result of the chi square test: %f for a threshold %f\n", norm / (sigma * sigma), threshold);
	return ((norm / (sigma*sigma)) < threshold);
}

float NormalizedPatchManager::retrieveMean(unsigned pidx, unsigned c)
{
	unsigned px, py, pt, pc;
	vid->sz.coords(pidx, px, py, pt, pc);

	return meanCoeff(px,py,pt,c);
}

NormalizedPatchManager::~NormalizedPatchManager()
{

}
