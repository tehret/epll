/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PATCHMANAGER_HEADER_H 
#define PATCHMANAGER_HEADER_H 

#include <vector>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "../Utilities.h"
#include "../LibVideoT.hpp"


/**
 *
 * Basis for different patch manager
 *
 */

class PatchManager 
{
	public:
		/**
		 * @brief Load all patches with the internal indexing system 
		 *
		 * @param allPatches: Used to return the indexing
		 */
		virtual void getAllPatches(std::vector<unsigned>& allPatches) = 0;

		/**
		 * @brief Return the total number of patches
		 *
		 * @return number of patches
		 */
		virtual int getNbPatches() = 0;

		/** 
		 * @brief Compute the distance between the first patch and the second patch. The first patch is indexed directly from the video itself. The second patch is indexed by the local system.
		 *
		 * @param id1: Index of the first patch (in the video)
		 * @param id2: index of the second patch (in the local indexing)
		 *
		 * @return distance between the two patches
		 */
		virtual float distance(unsigned id1, unsigned id2) = 0;

		/** 
		 * @brief Compute the distance between the first patch and the second patch. The two patches are directly indexed from the video itself.
		 *
		 * @param id1: Index of the first patch (in the video)
		 * @param id2: index of the second patch (in the video)
		 *
		 * @return distance between the two patches
		 */
		virtual float distanceOrigPos(unsigned id1, unsigned id2) = 0;

		/** 
		 * @brief Compute the distance between the first patch and the second patch. The two patches are from the local indexing.
		 *
		 * @param id1: Index of the first patch (in the local indexing)
		 * @param id2: index of the second patch (in the local indexing)
		 *
		 * @return distance between the two patches
		 */
		virtual float distanceLocal(unsigned id1, unsigned id2) = 0;

		/** 
		 * @brief Given a list of patches indexed using the local system, load them into groups for the first step of the denoising algorithm
		 *
		 * @param group: Used to return the group of patches
		 * @param list: Used to return the index of the patches (in the video)
		 * @param kNN: Number of patches to load
		 * @param pidx: query patch
		 */
		virtual void loadGroupStep1(std::vector<std::vector<float> >& group, std::vector<unsigned>& list, int kNN, unsigned pidx) = 0;

		/** 
		 * @brief Given a list of patches indexed using the local system, load them into groups for the second step of the denoising algorithm
		 *
		 * @param group: Used to return the group of patches
		 * @param list: Used to return the index of the patches (in the video)
		 * @param v: The noisy version of the video
		 * @param group2: Used to return the group of patches from the noisy video
		 * @param kNN: Number of patches to load
		 * @param pidx: query patch
		 */
		virtual void loadGroupStep2(std::vector<float>& group, std::vector<unsigned>& list, Video<float> const& v, std::vector<float>& group2, int kNN, unsigned pidx) = 0;

		// Return 
		/** 
		 * @brief Get the dimension of a patch
		 *
		 * @return the dimension
		 */
		virtual int getDim() = 0;

		/** 
		 * @brief Compute the real distance between the first patch and the second patch. The first patch is indexed directly from the video itself. The second patch is indexed by the local system.
		 *
		 * @param id1: Index of the first patch (in the video)
		 * @param id2: index of the second patch (in the local system)
		 *
		 * @return distance between the two patches
		 */
		virtual float realDistance(unsigned pidx, unsigned id) = 0;

		/** 
		 * @brief Get the type of the patch manager
		 *
		 * @return an integer corresponding to the type.
		 */
		virtual int getType() = 0;

		/** 
		 * @brief Compute the real distance from a list of patches to a query patch. Useful to compare the distance while working with dimensionality reduction
		 *
		 * @param distances: Used to return the list of distances
		 * @param patches: List of the patches (in the local system)
		 * @param pidx: The reference patch
		 */
		virtual void computeRealDistance(std::vector<float>& distances, std::vector<unsigned> patches, unsigned pidx) = 0;

		/** 
		 * @brief Return the value of the patch at a specific dimension (used to compute a KD-tree)
		 *
		 * @param id1: Index of the patch (in the video)
		 * @param dim: The query dimension
		 *
		 * @return value at the given dimension
		 */
		virtual float getValueAtDim(unsigned id, unsigned dim) = 0;

		/** 
		 * @brief Return the value of the patch at a specific dimension (used to compute a KD-tree)
		 *
		 * @param id1: Index of the patch (in the local system)
		 * @param dim: The query dimension
		 *
		 * @return value at the given dimension
		 */
		virtual float getValueAtDimLocal(unsigned id, unsigned dim) = 0;

		/**
		 * @brief Compute the index of the neighboring patch in the specified direction
		 *
		 * @param patch: Query patch
		 * @param type: direction for the new patch. 0: x+decx. 1: y+decx. 2: t+dect. 3: x-decx. 4: y-decx. 5: t-dect
		 * @param decx: spatial shift
		 * @param decy: temporal shift
		 *
		 * @return the index of the new patch
		 */
		virtual unsigned getNeighboringPatch(unsigned patch, int type, int decx, int dect) = 0;

		/**
		 * @brief Compute the index of the neighboring patch in the specified direction (in the inverse direction compared to the previous function)
		 *
		 * @param patch: Query patch
		 * @param type: direction for the new patch. 0: x-decx. 1: y-decx. 2: t-dect. 3: x+decx. 4: y+decx. 5: t+dect
		 * @param decx: spatial shift
		 * @param decy: temporal shift
		 *
		 * @return the index of the new patch
		 */
		virtual unsigned getInverseNeighboringPatch(unsigned patch, int type, int decx, int dect) = 0;

		/** 
		 * @brief Compute a list of neighbors of the query patch as well as their respective position
		 *
		 * @param listNeighbors: Used to return the neighbors and their respective position
		 * @param pidx: Query patch
		 */
		virtual void getNeighbors(std::vector<std::pair<unsigned, int> >& listNeighbors, unsigned pidx) = 0;

		/**
		 * Not used in the current framework
		 */
		virtual int hashPatch(unsigned pidx, bool useRot, std::vector<std::vector<float> >& rot, int nbTop, int& secondBin, std::vector<std::pair<int, int> >& otherOpportunities, bool getMorePatches = false) = 0;

		/**
		 * @brief Chi square test on a patch to know if it is only noise or not
		 *
		 * @param pidx: The query patch
		 * @param verbose: Print temporal computation that can be useful
		 *
		 * @return true if the patch is only noise according to the test
		 */
		virtual bool chiSquareTest(unsigned pidx, bool verbose = false) = 0;

		/**
		 * @brief Retrieve the mean to be substracted from the patch in normalized patch manager
		 *
		 * @param pidx: the query patch
		 * @param c: the channel
		 *
		 * @return the mean
		 */
		virtual float retrieveMean(unsigned pidx, unsigned c) = 0;

		/** 
		 * @brief Transform a patch indexed in the local system to the video
		 *
		 * @param pidx: the query patch 
		 *
		 * @return the index of pidx in the video
		 */
		virtual unsigned transformId(unsigned pidx) = 0;

		/** 
		 * @brief Takes a list of indexes of patches in the local system and modify it so that it the indexes are now in the video
		 *
		 * @param indexes: The indexes of the patch to be modified 
		 */
		virtual void getRealPatches(std::vector<unsigned>& indexes) = 0;

		/** 
		 * @brief Get the size of the video on which the patch manager is based
		 *
		 * @return the size of the video
		 */
		virtual const VideoSize* infoVid() = 0;
	private:
};
#endif
