/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATABASEPM_HEADER_H
#define DATABASEPM_HEADER_H

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "../Utilities.h"
#include "../LibVideoT.hpp"
#include "patchManager.h"

/**
 *
 * Patch manager defined on an external database
 *
 */

class DatabasePatchManager: public PatchManager {
	public:
		/**
		 * @brief Basic constructor
		 *
		 * @param video: The database on which the manager will be based on
		 * @param sizeP: Spatial size for the patches
		 * @param sizePt: Temporal size for the patches
		 * @param channels: Number of channels from the video to be used
		 */
		DatabasePatchManager(Video<float> const& video, int sizeP, int sizePt, int channels);

		/**
		 * check patchManager.h for information on these functions
		 * @{
		 */
		void getAllPatches(std::vector<unsigned>& allPatches);
		int getNbPatches();
		float distance(unsigned id1, unsigned id2);
		float distanceOrigPos(unsigned id1, unsigned id2);
		float distanceLocal(unsigned id1, unsigned id2);
		void loadGroupStep1(std::vector<std::vector<float> >& group, std::vector<unsigned>& list, int kNN, unsigned pidx);
		void loadGroupStep2(std::vector<float>& group, std::vector<unsigned>& list, Video<float> const& v, std::vector<float>& group2, int kNN, unsigned pidx);

		int getDim();

		float getValueAtDim(unsigned id, unsigned dim);
		float getValueAtDimLocal(unsigned id, unsigned dim);
		float realDistance(unsigned pidx, unsigned id);
		int getType();

		void computeRealDistance(std::vector<float>& distances, std::vector<unsigned> patches, unsigned pidx);

		unsigned getNeighboringPatch(unsigned patch, int type, int decx, int dect);
		unsigned getInverseNeighboringPatch(unsigned patch, int type, int decx, int dect);
		void getNeighbors(std::vector<std::pair<unsigned, int> >& listNeighbors, unsigned pidx);
		
		const VideoSize* infoVid() {return &(vid->sz);};

		int hashPatch(unsigned pidx, bool useRot, std::vector<std::vector<float> >& rot, int nbTop, int& secondBin, std::vector<std::pair<int, int> >& otherPossibilities, bool getMorePatches = false);

		unsigned transformId(unsigned pidx) {return pidx;};
		bool chiSquareTest(unsigned pidx, bool verbose){return false;};
		float retrieveMean(unsigned pidx, unsigned c){return 0.f;};

		void setCurrentVideo(Video<float>& cur);
		void getRealPatches(std::vector<unsigned>& indexes) {};
		/**
		 * @}
		 */

		~DatabasePatchManager();
	private:
		/// The database on which the manager is based (for now it is on the form of a video
		const Video<float>* vid;
		/// The video that will be used alongside the database
		const Video<float>* current;
		/// Spatial size of a patch
		int sizePatch;
		/// Temporal size of a patch
		int sizePatchTime;
		/// Number of chanels from the video to use (0 to nbChannels=
		int nbChannels;
};
#endif
