/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NORMPM_HEADER_H
#define NORMPM_HEADER_H

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "../Utilities.h"
#include "../LibVideoT.hpp"
#include "patchManager.h"
#include <unordered_map>
#include <boost/math/special_functions/gamma.hpp>


/**
 *
 * Basic normalized patch manager. Patches directly match the video mean and variance normalized.
 *
 */

class NormalizedPatchManager: public PatchManager {
	public:
		/**
		 * @brief Basic constructor
		 *
		 * @param video: The video on which the manager will be based on
		 * @param sizeP: Spatial size for the patches
		 * @param sizePt: Temporal size for the patches
		 * @param channels: Number of channels from the video to be used
		 * @param tpMean: type of mean to be used
		 * @param tpVar: type of variance to be used (not used currently)
		 * @param sigma_: noise level for the chi square test
		 * @param flat: threshold for the chi square test (between 0 and 1)
		 */
		NormalizedPatchManager(Video<float> const& video, int sizeP, int sizePt, int channels, int tpMean, int tpVar, int sigma_, float flat);

		/**
		 * check patchManager.h for information on these functions
		 * @{
		 */
		void getAllPatches(std::vector<unsigned>& allPatches);
		int getNbPatches();
		float distance(unsigned id1, unsigned id2);
		float distanceOrigPos(unsigned id1, unsigned id2);
		float distanceLocal(unsigned id1, unsigned id2);
		void loadGroupStep1(std::vector<std::vector<float> >& group, std::vector<unsigned>& list, int kNN,  unsigned pidx);
		void loadGroupStep2(std::vector<float>& group, std::vector<unsigned>& list, Video<float> const& v, std::vector<float>& group2, int kNN, unsigned pidx);

		int getDim();

		float getValueAtDim(unsigned id, unsigned dim);
		float getValueAtDimLocal(unsigned id, unsigned dim);
		float realDistance(unsigned pidx, unsigned id);
		int getType();

		void computeRealDistance(std::vector<float>& distances, std::vector<unsigned> patches, unsigned pidx);

		unsigned getNeighboringPatch(unsigned patch, int type, int decx, int dect);
		unsigned getInverseNeighboringPatch(unsigned patch, int type, int decx, int dect);
		void getNeighbors(std::vector<std::pair<unsigned, int> >& listNeighbors, unsigned pidx);
		
		const VideoSize* infoVid() {return &(vid->sz);};

		int hashPatch(unsigned pidx, bool useRot, std::vector<std::vector<float> >& rot, int nbTop, int& secondBin, std::vector<std::pair<int, int> >& otherPossibilities, bool getMorePatches = false);

		unsigned transformId(unsigned pidx) {return pidx;};

		bool chiSquareTest(unsigned pidx, bool verbose = false);
		float retrieveMean(unsigned pidx, unsigned c);
		void getRealPatches(std::vector<unsigned>& indexes) {};
		/**
		 * @}
		 */

		~NormalizedPatchManager();
	private:
		/// The video on which the manager is based
		const Video<float>* vid;
		/// Spatial size of a patch
		int sizePatch;
		/// Temporal size of a patch
		int sizePatchTime;
		/// Number of chanels from the video to use (0 to nbChannels=
		int nbChannels;
		/// Noise level for the chi square test
		int sigma;

		/// Type of mean to be computed
		/// 0 -> nothing 
		/// 1 -> global
		/// 2 -> frame by frame
		int typeNormMean;

		/// Type of variance to be computed
		/// For now it is not used
		/// 0 -> nothing 
		/// 1 -> global
		/// 2 -> frame by frame
		int typeNormVar;

		/// Stock of the mean of each patch
		Video<float> meanCoeff;
		/// Stock of the variance of each patch
		Video<float> varCoeff;

		/// Threshold for the chi square test
		float threshold;
};
#endif
