/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATABASENPM_HEADER_H
#define DATABASENPM_HEADER_H

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "../Utilities.h"
#include "../LibVideoT.hpp"
#include "patchManager.h"

/**
 *
 * Normalized patch manager defined on an external database
 *
 */

class DatabaseNormalizedPatchManager: public PatchManager {
	public:
		DatabaseNormalizedPatchManager(Video<float> const& video, int sizeP, int sizePt, int channels);
		void getAllPatches(std::vector<unsigned>& allPatches);
		int getNbPatches();
		float distance(unsigned id1, unsigned id2);
		float distanceOrigPos(unsigned id1, unsigned id2);
		float distanceLocal(unsigned id1, unsigned id2);
		void loadGroupStep1(std::vector<std::vector<float> >& group, std::vector<unsigned>& list, int kNN, unsigned pidx);
		void loadGroupStep2(std::vector<float>& group, std::vector<unsigned>& list, Video<float> const& v, std::vector<float>& group2, int kNN, unsigned pidx);

		int getDim();

		float getValueAtDim(unsigned id, unsigned dim);
		float getValueAtDimLocal(unsigned id, unsigned dim);
		// Return always 0 because it should not be used
		float realDistance(unsigned pidx, unsigned id);
		int getType();

		void computeRealDistance(std::vector<float>& distances, std::vector<unsigned> patches, unsigned pidx);

		unsigned getNeighboringPatch(unsigned patch, int type, int decx, int dect);
		unsigned getInverseNeighboringPatch(unsigned patch, int type, int decx, int dect);
		void getNeighbors(std::vector<std::pair<unsigned, int> >& listNeighbors, unsigned pidx);
		
		const VideoSize* infoVid() {return &(vid->sz);};

		int hashPatch(unsigned pidx, bool useRot, std::vector<std::vector<float> >& rot, int nbTop, int& secondBin, std::vector<std::pair<int, int> >& otherPossibilities, bool getMorePatches = false);

		unsigned transformId(unsigned pidx) {return pidx;};
		bool chiSquareTest(unsigned pidx, bool verbose){return false;};
		float retrieveMean(unsigned pidx, unsigned c){return 0.f;};

		void setCurrentVideo(Video<float>& cur);

		void getRealPatches(std::vector<unsigned>& indexes) {};

		~DatabaseNormalizedPatchManager();
	private:
		const Video<float>* vid;
		const Video<float>* current;
		int sizePatch;
		int sizePatchTime;
		int nbChannels;
		Video<float> meanCoeff1;
		Video<float> meanCoeff2;
};
#endif
