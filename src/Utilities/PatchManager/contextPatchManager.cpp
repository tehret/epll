/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file contextPatchManager.cpp
 * @brief Patch manager using context patches (see paper "Con-Patch: When a Patch Meets its Context" Romano & Elad)
 *
 * @author Thibaud Ehret <ehret.thibaud@gmail.com>
 **/

#include "contextPatchManager.h"

static int rand_gen(int i)
{
	return std::rand() % i;
}

ContextPatchManager::ContextPatchManager(Video<float> const& vid_, int sizeP, int sizePt, int channels, unsigned sizeR, unsigned sizeRt, float alpha_, unsigned sigma_)
{
	vid = &vid_;

	sizePatch = sizeP;
	sizePatchTime = sizePt;
	nbChannels = channels;

	sizeRegion = sizeR;
	sizeRegionTime = sizeRt;
	alpha = alpha_;
	sigma = sigma_;

	context.resize(vid_.sz.width, vid_.sz.height, vid_.sz.frames, nbBins);

	int sWx = sizeR;
	int sWy = sizeR;
	int sWt_b = sizeRt;
	int sWt_f = sizeRt;

	// Compute all the distance that needs to be used
	// Compute the context and bin it
	
	const int sPx = sizePatch;
	const int sPt = sizePatchTime;
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic) num_threads(4)
#endif
	for(unsigned x = 0; x < vid_.sz.width; ++x)
	for(unsigned y = 0; y < vid_.sz.height; ++y)
	for(unsigned t = 0; t < vid_.sz.frames; ++t)
	{
		float nbEl = 0;

		// Define the region Wx and Wt -> check localSearch for that
		unsigned rangex[2];
		unsigned rangey[2];
		unsigned ranget[2];

#ifdef CENTRED_SEARCH
		rangex[0] = std::max(0, (int)x - (sWx-1)/2);
		rangey[0] = std::max(0, (int)y - (sWy-1)/2);
		ranget[0] = std::max(0, (int)t -  sWt_b   );

		rangex[1] = std::min((int)vid->sz.width  - sPx, (int)x + (sWx-1)/2);
		rangey[1] = std::min((int)vid->sz.height - sPx, (int)y + (sWy-1)/2);
		ranget[1] = std::min((int)vid->sz.frames - sPt, (int)t +  sWt_f   );
#else
		int shift_x = std::min(0, (int)x - (sWx-1)/2); 
		int shift_y = std::min(0, (int)y - (sWy-1)/2); 
		int shift_t = std::min(0, (int)t -  sWt_b   ); 

		shift_x += std::max(0, (int)x + (sWx-1)/2 - (int)vid->sz.width  + sPx); 
		shift_y += std::max(0, (int)y + (sWy-1)/2 - (int)vid->sz.height + sPx); 
		shift_t += std::max(0, (int)t +  sWt_f    - (int)vid->sz.frames + sPt); 

		rangex[0] = std::max(0, (int)x - (sWx-1)/2 - shift_x);
		rangey[0] = std::max(0, (int)y - (sWy-1)/2 - shift_y);
		ranget[0] = std::max(0, (int)t -  sWt_b    - shift_t);

		rangex[1] = std::min((int)vid->sz.width  - sPx, (int)x + (sWx-1)/2 - shift_x);
		rangey[1] = std::min((int)vid->sz.height - sPx, (int)y + (sWy-1)/2 - shift_y);
		ranget[1] = std::min((int)vid->sz.frames - sPt, (int)t +  sWt_f    - shift_t);
#endif
		// TODO Maybe modify the step
		for(unsigned wx = rangex[0]; wx < rangex[1]; wx+= 4)
		for(unsigned wy = rangey[0]; wy < rangey[1]; wy+= 4)
		for(unsigned wt = ranget[0]; wt <= ranget[1]; wt+= 4)
		{
			if(wx == x && wy == y && wt == t)
				continue;
			float dist = 0.f, dif;
			for (unsigned hc = 0; hc < nbChannels; ++hc)
			for (unsigned ht = 0; ht < sPt; ht++)
			for (unsigned hy = 0; hy < sPx; hy++)
			for (unsigned hx = 0; hx < sPx; hx++)
			{
				dist += (dif = (*vid)(x + hx, y + hy, t + ht, hc)
						- (*vid)(wx + hx, wy + hy, wt + ht, hc)) * dif;
			}

			//printf("%f\n", dist);
			//dist = std::exp(-dist/(2*sigma));
			dist = exp(- std::sqrt(dist / (sPt * sPx * sPx * nbChannels)) / (2*sigma));

			int inCaseOfPb = std::floor(10.*dist);
			++context(x,y,t,(inCaseOfPb < 10)? inCaseOfPb : 9);
			++nbEl;
		}

		for(unsigned c = 0; c < nbBins; ++c)
			context(x,y,t,c) /= nbEl;
	}
}

float ContextPatchManager::distanceOrigPos(unsigned patch1, unsigned patch2)
{
	unsigned px, py, pt, pc;
	vid->sz.coords(patch1, px, py, pt, pc);

	unsigned qx, qy, qt, qc; 
	vid->sz.coords(patch2, qx, qy, qt, qc);

	const int sPx = sizePatch;
	const int sPt = sizePatchTime;

	float dist = 0.f, dif;
	for (unsigned hc = 0; hc < nbChannels; ++hc)
		for (unsigned ht = 0; ht < sPt; ht++)
			for (unsigned hy = 0; hy < sPx; hy++)
				for (unsigned hx = 0; hx < sPx; hx++)
					dist += (dif = (*vid)(px + hx, py + hy, pt + ht, hc)
							- (*vid)(qx + hx, qy + hy, qt + ht, hc)) * dif;

	for(unsigned b = 0; b < nbBins; ++b)
		dist += alpha * alpha * ((dif = context(px, py, pt, b)
							- context(qx, qy, qt, b)) * dif);

	return std::sqrt(dist / (sPt * sPx * sPx * nbChannels + nbBins));
}

float ContextPatchManager::distanceLocal(unsigned patch1, unsigned patch2)
{
	unsigned px, py, pt, pc;
	vid->sz.coords(patch1, px, py, pt, pc);

	unsigned qx, qy, qt, qc; 
	vid->sz.coords(patch2, qx, qy, qt, qc);

	const int sPx = sizePatch;
	const int sPt = sizePatchTime;

	float dist = 0.f, dif;
	for (unsigned hc = 0; hc < nbChannels; ++hc)
		for (unsigned ht = 0; ht < sPt; ht++)
			for (unsigned hy = 0; hy < sPx; hy++)
				for (unsigned hx = 0; hx < sPx; hx++)
					dist += (dif = (*vid)(px + hx, py + hy, pt + ht, hc)
							- (*vid)(qx + hx, qy + hy, qt + ht, hc)) * dif;

	for(unsigned b = 0; b < nbBins; ++b)
		dist += alpha * alpha * ((dif = context(px, py, pt, b)
							- context(qx, qy, qt, b)) * dif);

	return std::sqrt(dist / (sPt * sPx * sPx * nbChannels + nbBins));
}

void ContextPatchManager::loadGroupStep1(std::vector<std::vector<float> >& group, std::vector<unsigned>& list, int kNN, unsigned pidx)
{
	for (unsigned c  = 0; c < vid->sz.channels; c++)
		for (unsigned ht = 0, k = 0; ht < sizePatchTime; ht++)
			for (unsigned hy = 0;        hy < sizePatch; hy++)
				for (unsigned hx = 0;        hx < sizePatch; hx++)
					for (unsigned n = 0; n < kNN; n++, k++)
						group[c][k] = (*vid)(c * vid->sz.wh + list[n] + ht * vid->sz.whc + hy * vid->sz.width + hx);
}

void ContextPatchManager::loadGroupStep2(std::vector<float>& group, std::vector<unsigned>& list, Video<float> const& v, std::vector<float>& group2, int kNN, unsigned pidx)
{
	for (unsigned c  = 0, k = 0; c < v.sz.channels; c++)
		for (unsigned ht = 0; ht < sizePatchTime; ht++)
			for (unsigned hy = 0;        hy < sizePatch; hy++)
				for (unsigned hx = 0;        hx < sizePatch; hx++)
					for (unsigned n = 0; n < kNN; n++, k++)
					{
						group[k] = (*vid)(c * vid->sz.wh + list[n] + ht * vid->sz.whc + hy * vid->sz.width + hx);
						group2[k] = v(c * vid->sz.wh + list[n] + ht * vid->sz.whc + hy * vid->sz.width + hx);
					}
	// Local patches are not interesting, only return the true patches
}

int ContextPatchManager::getDim()
{
	// Dimension of a regular patch plpus its context
	return sizePatch * sizePatch * sizePatchTime * nbChannels + nbBins;
}

float ContextPatchManager::getValueAtDim(unsigned id, unsigned dim)
{
	unsigned px, py, pt, pc;
	vid->sz.coords(id, px, py, pt, pc);

	if(dim < sizePatch * sizePatch * sizePatchTime)
	{
		unsigned hx, hy, ht, hc;
		unsigned l = dim;
		hx = l % sizePatch;
		l /= sizePatch;
		hy = l % sizePatch;
		l /= sizePatch;
		ht = l % sizePatchTime;
		l /= sizePatchTime;
		hc = l;
		return (*vid)(px + hx, py + hy, pt + ht, hc);
	}
	else 
		return context(px, py,pt,dim - sizePatch * sizePatch * sizePatchTime); 
}

float ContextPatchManager::getValueAtDimLocal(unsigned id, unsigned dim)
{
	unsigned px, py, pt, pc;
	vid->sz.coords(id, px, py, pt, pc);

	if(dim < sizePatch * sizePatch * sizePatchTime)
	{
		unsigned hx, hy, ht, hc;
		unsigned l = dim;
		hx = l % sizePatch;
		l /= sizePatch;
		hy = l % sizePatch;
		l /= sizePatch;
		ht = l % sizePatchTime;
		l /= sizePatchTime;
		hc = l;
		return (*vid)(px + hx, py + hy, pt + ht, hc);
	}
	else 
		return context(px, py,pt,dim - sizePatch * sizePatch * sizePatchTime); 
}

float ContextPatchManager::realDistance(unsigned patch1, unsigned id)
{
	unsigned px, py, pt, pc;
	vid->sz.coords(patch1, px, py, pt, pc);

	unsigned qx, qy, qt, qc; 
	vid->sz.coords(id, qx, qy, qt, qc);

	const int sPx = sizePatch;
	const int sPt = sizePatchTime;

	float dist = 0.f, dif;
	for (unsigned hc = 0; hc < nbChannels; ++hc)
		for (unsigned ht = 0; ht < sPt; ht++)
			for (unsigned hy = 0; hy < sPx; hy++)
				for (unsigned hx = 0; hx < sPx; hx++)
					dist += (dif = (*vid)(px + hx, py + hy, pt + ht, hc)
							- (*vid)(qx + hx, qy + hy, qt + ht, hc)) * dif;
	return std::sqrt(dist / (sPt * sPx * sPx * nbChannels)) / 255.f;
}

int ContextPatchManager::getType()
{
	// TODO find a good id for this context manager
	return 9;
}

void ContextPatchManager::computeRealDistance(std::vector<float>& distances, std::vector<unsigned> patches, unsigned pidx)
{
	for(int i = 0; i < patches.size(); ++i)
	{
		distances[i] = realDistance(pidx, patches[i]);
	}
}

unsigned ContextPatchManager::getNeighboringPatch(unsigned patch, int type, int sp, int st)
{
	unsigned px,py,pt,pc;
	vid->sz.coords(patch, px, py, pt, pc);

	int width = vid->sz.width - sizePatch - 1;
	int height = vid->sz.height - sizePatch - 1;
	int time = vid->sz.frames - sizePatchTime - 1;

	if(type == 0)
	{
		if(px+sp < width)
			return vid->sz.index(px+sp,py,pt,0);
		return patch;
	}
	else if(type == 1)
	{
		if(py+sp < height)
			return vid->sz.index(px,py+sp,pt,0);
		return patch;
	}
	else if(type == 2)
	{
		if(pt+sp < time)
			return vid->sz.index(px,py,pt+st,0);
		return patch;
	}
	else if(type == 3)
	{
		if(((int)px)-sp >= 0)
			return vid->sz.index(px-sp,py,pt,0);
		return patch;
	}
	else if(type == 4)
	{
		if(((int)py)-sp >= 0)
			return vid->sz.index(px,py-sp,pt,0);
		return patch;
	}
	else if(type == 5)
	{
		if(((int)pt)-st >= 0)
			return vid->sz.index(px,py,pt-st,0);
		return patch;
	}
	else
	{
		// This type of propagation doesn't exist ...
		return 0;
	}
}

unsigned ContextPatchManager::getInverseNeighboringPatch(unsigned patch, int type, int sp, int st)
{
	unsigned px,py,pt,pc;
	vid->sz.coords(patch, px, py, pt, pc);

	int width = vid->sz.width - sizePatch - 1;
	int height = vid->sz.height - sizePatch - 1;
	int time = vid->sz.frames - sizePatchTime - 1;

	if(type == 3)
	{
		if(px+sp < width)
			return vid->sz.index(px+sp,py,pt,0);
		return patch;
	}
	else if(type == 4)
	{
		if(py+sp < height)
			return vid->sz.index(px,py+sp,pt,0);
		return patch;
	}
	else if(type == 5)
	{
		if(pt+sp < time)
			return vid->sz.index(px,py,pt+st,0);
		return patch;
	}
	else if(type == 0)
	{
		if(((int)px)-sp >= 0)
			return vid->sz.index(px-sp,py,pt,0);
		return patch;
	}
	else if(type == 1)
	{
		if(((int)py)-sp >= 0)
			return vid->sz.index(px,py-sp,pt,0);
		return patch;
	}
	else if(type == 2)
	{
		if(((int)pt)-st >= 0)
			return vid->sz.index(px,py,pt-st,0);
		return patch;
	}
	else
	{
	unsigned px,py,pt,pc;
	vid->sz.coords(patch, px, py, pt, pc);

	int width = vid->sz.width - sizePatch - 1;
	int height = vid->sz.height - sizePatch - 1;
	int time = vid->sz.frames - sizePatchTime - 1;

	if(type == 3)
	{
		if(px+sp < width)
			return vid->sz.index(px+sp,py,pt,0);
		return patch;
	}
	else if(type == 4)
	{
		if(py+sp < height)
			return vid->sz.index(px,py+sp,pt,0);
		return patch;
	}
	else if(type == 5)
	{
		if(pt+sp < time)
			return vid->sz.index(px,py,pt+st,0);
		return patch;
	}
	else if(type == 0)
	{
		if(((int)px)-sp >= 0)
			return vid->sz.index(px-sp,py,pt,0);
		return patch;
	}
	else if(type == 1)
	{
		if(((int)py)-sp >= 0)
			return vid->sz.index(px,py-sp,pt,0);
		return patch;
	}
	else if(type == 2)
	{
		if(((int)pt)-st >= 0)
			return vid->sz.index(px,py,pt-st,0);
		return patch;
	}
	else
	{
		// This type of propagation doesn't exist ...
		return 0;
	}
		// This type of propagation doesn't exist ...
		return 0;
	}
}
void ContextPatchManager::getNeighbors(std::vector<std::pair<unsigned, int> >& listNeighbors, unsigned pidx)
{
	unsigned px,py,pt,pc;
	vid->sz.coords(pidx, px, py, pt, pc);

	if((int)px + sizePatch/2 <= (int)vid->sz.width - sizePatch)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px + sizePatch/2, py, pt, 0), 0));
	}	
	if((int)py + sizePatch/2 <= (int)vid->sz.height - sizePatch)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px, py + sizePatch/2, pt, 0), 1));
	}
	if((int)pt + sizePatchTime/2 <= (int)vid->sz.frames - sizePatchTime)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px, py, pt + sizePatchTime/2, 0), 2));
	}
	if((int)px - sizePatch/2 >= 0)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px - sizePatch/2, py, pt, 0), 3));
	}
	if((int)py - sizePatch/2 >= 0)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px, py - sizePatch/2, pt, 0), 4));
	}
	if((int)pt - sizePatchTime/2 >= 0)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px, py, pt - sizePatchTime/2, 0), 5));
	}
}

int ContextPatchManager::hashPatch(unsigned pidx, bool useRot, std::vector<std::vector<float> >& rot, int nbTop, int& secondBin, std::vector<std::pair<int, int> >& otherPossibilities, bool getMorePatches)
{
	// TODO
	secondBin = 0;
	return 0;
}

ContextPatchManager::~ContextPatchManager()
{

}

int ContextPatchManager::exportPM(char* path)
{
	
}

ContextPatchManager::ContextPatchManager(Video<float> const& vid_, int sizeP, int sizePt, int channels, int nbd, char* path)
{

}

void ContextPatchManager::getAllPatches(std::vector<unsigned>& allPatches)
{
	int width = vid->sz.width - sizePatch + 1;
	int height = vid->sz.height - sizePatch + 1;
	int time = vid->sz.frames - sizePatchTime + 1;
	for(unsigned px = 0, i = 0; px < width; ++px)
		for(unsigned py = 0; py < height; ++py)
			for(unsigned pt = 0; pt < time; ++pt, ++i)
				allPatches[i] = vid->sz.index(px, py, pt, 0);

}

int ContextPatchManager::getNbPatches()
{
	int width = vid->sz.width - sizePatch + 1;
	int height = vid->sz.height - sizePatch + 1;
	int time = vid->sz.frames - sizePatchTime + 1;
	return width * height * time;
}

float ContextPatchManager::distance(unsigned patch1, unsigned patch2)
{
	unsigned px, py, pt, pc;
	vid->sz.coords(patch1, px, py, pt, pc);

	unsigned qx, qy, qt, qc; 
	vid->sz.coords(patch2, qx, qy, qt, qc);

	const int sPx = sizePatch;
	const int sPt = sizePatchTime;

	float dist = 0.f, dif;
	for (unsigned hc = 0; hc < nbChannels; ++hc)
		for (unsigned ht = 0; ht < sPt; ht++)
			for (unsigned hy = 0; hy < sPx; hy++)
				for (unsigned hx = 0; hx < sPx; hx++)
					dist += (dif = (*vid)(px + hx, py + hy, pt + ht, hc)
							- (*vid)(qx + hx, qy + hy, qt + ht, hc)) * dif;

	for(unsigned b = 0; b < nbBins; ++b)
		dist += alpha * alpha * ((dif = context(px, py, pt, b)
							- context(qx, qy, qt, b)) * dif);

	return std::sqrt(dist / (sPt * sPx * sPx * nbChannels + nbBins));
}
