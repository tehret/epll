/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file databaseNormalizedPatchManager.cpp
 * @brief Patch manager using normalized patches (in mean) from a given database different from the query
 *
 * @author Thibaud Ehret <ehret.thibaud@gmail.com>
 **/
#include "databaseNormalizedPatchManager.h"

DatabaseNormalizedPatchManager::DatabaseNormalizedPatchManager(Video<float> const& video, int sizeP, int sizePt, int channels)
{
	vid = &video;
	sizePatch = sizeP;
	sizePatchTime = sizePt;
	nbChannels = channels;

	int width = vid->sz.width - sizeP + 1;
	int height = vid->sz.height - sizeP + 1;
	int time = vid->sz.frames - sizePt + 1;

	meanCoeff1.resize(width, height, vid->sz.frames, nbChannels);

	std::vector<unsigned> allPatches = std::vector<unsigned>(width * height * time);

	// Compute the mean for rach patch of the database
	for(unsigned px = 0, i = 0; px < width; ++px)
		for(unsigned py = 0; py < height; ++py)
			for(unsigned pt = 0; pt < vid->sz.frames; ++pt, ++i)
				for(unsigned pc = 0; pc < nbChannels; ++pc)
				{
					float mean = 0.f;
					for (unsigned hy = 0, u = 1; hy < sizeP; hy++)
						for (unsigned hx = 0; hx < sizeP; hx++, ++u)
						{
							mean += ((*vid)(px + hx, py + hy, pt, pc) - mean) / (float)u;
						}
					meanCoeff1(px,py,pt,pc) = mean;
				}
}

void DatabaseNormalizedPatchManager::getAllPatches(std::vector<unsigned>& allPatches)
{
	int width = vid->sz.width - sizePatch + 1;
	int height = vid->sz.height - sizePatch + 1;
	int time = vid->sz.frames - sizePatchTime + 1;
	for(unsigned px = 0, i = 0; px < width; ++px)
		for(unsigned py = 0; py < height; ++py)
			for(unsigned pt = 0; pt < time; ++pt, ++i)
				allPatches[i] = vid->sz.index(px, py, pt, 0);
}

int DatabaseNormalizedPatchManager::getNbPatches()
{
	int width = vid->sz.width - sizePatch + 1;
	int height = vid->sz.height - sizePatch + 1;
	int time = vid->sz.frames - sizePatchTime + 1;
	return width * height * time;
}

float DatabaseNormalizedPatchManager::distance(unsigned patch1, unsigned patch2)
{
	unsigned px, py, pt, pc;
	current->sz.coords(patch1, px, py, pt, pc);

	unsigned qx, qy, qt, qc; 
	vid->sz.coords(patch2, qx, qy, qt, qc);

	const int sPx = sizePatch;
	const int sPt = sizePatchTime;

	// Compute basic l2 distance
	float dist = 0.f, dif;
	for (unsigned hc = 0; hc < nbChannels; ++hc)
		for (unsigned ht = 0; ht < sPt; ht++)
			for (unsigned hy = 0; hy < sPx; hy++)
				for (unsigned hx = 0; hx < sPx; hx++)
				{
					dist += (dif = (((*current)(px + hx, py + hy, pt + ht, hc) -  meanCoeff2(px, py, pt + ht, hc))							
							- ((*vid)(qx + hx, qy + hy, qt + ht, hc) - meanCoeff1(qx, qy, qt + ht, hc)))) * dif;
				}
	// Normalize it
	return std::sqrt(dist / (sPt * sPx * sPx * nbChannels)) / 255.f;
}
// cf distance
float DatabaseNormalizedPatchManager::distanceOrigPos(unsigned patch1, unsigned patch2)
{
	unsigned px, py, pt, pc;
	current->sz.coords(patch1, px, py, pt, pc);

	unsigned qx, qy, qt, qc; 
	vid->sz.coords(patch2, qx, qy, qt, qc);

	const int sPx = sizePatch;
	const int sPt = sizePatchTime;

	float dist = 0.f, dif;
	for (unsigned hc = 0; hc < nbChannels; ++hc)
		for (unsigned ht = 0; ht < sPt; ht++)
			for (unsigned hy = 0; hy < sPx; hy++)
				for (unsigned hx = 0; hx < sPx; hx++)
				{
					dist += (dif = (((*current)(px + hx, py + hy, pt + ht, hc) -  meanCoeff2(px, py, pt + ht, hc))							
							- ((*vid)(qx + hx, qy + hy, qt + ht, hc) - meanCoeff1(qx, qy, qt + ht, hc)))) * dif;
				}
	return std::sqrt(dist / (sPt * sPx * sPx * nbChannels)) / 255.f;
}

float DatabaseNormalizedPatchManager::distanceLocal(unsigned patch1, unsigned patch2)
{
	unsigned px, py, pt, pc;
	vid->sz.coords(patch1, px, py, pt, pc);

	unsigned qx, qy, qt, qc; 
	vid->sz.coords(patch2, qx, qy, qt, qc);

	const int sPx = sizePatch;
	const int sPt = sizePatchTime;

	float dist = 0.f, dif;
	for (unsigned hc = 0; hc < nbChannels; ++hc)
		for (unsigned ht = 0; ht < sPt; ht++)
			for (unsigned hy = 0; hy < sPx; hy++)
				for (unsigned hx = 0; hx < sPx; hx++)
					dist += (dif = (((*vid)(px + hx, py + hy, pt + ht, hc) -  meanCoeff1(px, py, pt + ht, hc))							
							- ((*vid)(qx + hx, qy + hy, qt + ht, hc) - meanCoeff1(qx, qy, qt + ht, hc)))) * dif;
	return std::sqrt(dist / (sPt * sPx * sPx * nbChannels)) / 255.f;
}

void DatabaseNormalizedPatchManager::loadGroupStep1(std::vector<std::vector<float> >& group, std::vector<unsigned>& list, int kNN, unsigned pidx)
{
	// TODO
	for (unsigned c  = 0; c < vid->sz.channels; c++)
		for (unsigned ht = 0, k = 0; ht < sizePatchTime; ht++)
			for (unsigned hy = 0;        hy < sizePatch; hy++)
				for (unsigned hx = 0;        hx < sizePatch; hx++)
					for (unsigned n = 0; n < kNN; n++, k++)
						group[c][k] = (((*vid)(c * vid->sz.wh + list[n] + ht * vid->sz.whc + hy * vid->sz.width + hx) - meanCoeff1(c * vid->sz.wh + list[n] + ht * vid->sz.whc)) + meanCoeff2(c * current->sz.wh + ht * current->sz.whc + pidx));
}

void DatabaseNormalizedPatchManager::loadGroupStep2(std::vector<float>& group, std::vector<unsigned>& list, Video<float> const& v, std::vector<float>& group2, int kNN, unsigned pidx)
{
	for (unsigned c  = 0, k = 0; c < v.sz.channels; c++)
		for (unsigned ht = 0; ht < sizePatchTime; ht++)
			for (unsigned hy = 0;        hy < sizePatch; hy++)
				for (unsigned hx = 0;        hx < sizePatch; hx++)
					for (unsigned n = 0; n < kNN; n++, k++)
					{
						group[k] = ((*vid)(c * vid->sz.wh + list[n] + ht * vid->sz.whc + hy * vid->sz.width + hx) - meanCoeff1(c * vid->sz.wh + list[n] + ht * vid->sz.whc)) + meanCoeff2(c * current->sz.wh + ht * vid->sz.whc + pidx);
						group2[k] = ((*vid)(c * vid->sz.wh + list[n] + ht * vid->sz.whc + hy * vid->sz.width + hx) - meanCoeff1(c * vid->sz.wh + list[n] + ht * vid->sz.whc)) + meanCoeff2(c * current->sz.wh + ht * vid->sz.whc + pidx);
					}
}

int DatabaseNormalizedPatchManager::getDim()
{
	return sizePatch * sizePatch * sizePatchTime * nbChannels;
}

float DatabaseNormalizedPatchManager::getValueAtDim(unsigned id, unsigned dim)
{
	unsigned px, py, pt, pc;
	vid->sz.coords(id, px, py, pt, pc);

	unsigned hx, hy, ht, hc;
	unsigned l = dim;
	hx = l % sizePatch;
	l /= sizePatch;
	hy = l % sizePatch;
	l /= sizePatch;
	ht = l % sizePatchTime;
	l /= sizePatchTime;
	hc = l;
	return (*vid)(px + hx, py + hy, pt + ht, hc);
}

float DatabaseNormalizedPatchManager::realDistance(unsigned patch1, unsigned patch2)
{
	unsigned px, py, pt, pc;
	current->sz.coords(patch1, px, py, pt, pc);

	unsigned qx, qy, qt, qc; 
	vid->sz.coords(patch2, qx, qy, qt, qc);

	const int sPx = sizePatch;
	const int sPt = sizePatchTime;

	// Compute basic l2 distance
	float dist = 0.f, dif;
	for (unsigned hc = 0; hc < nbChannels; ++hc)
		for (unsigned ht = 0; ht < sPt; ht++)
			for (unsigned hy = 0; hy < sPx; hy++)
				for (unsigned hx = 0; hx < sPx; hx++)
				{
					dist += (dif = (((*current)(px + hx, py + hy, pt + ht, hc) -  meanCoeff2(px, py, pt + ht, hc))							
							- ((*vid)(qx + hx, qy + hy, qt + ht, hc) - meanCoeff1(qx, qy, qt + ht, hc)))) * dif;
				}
	// Normalize it
	return std::sqrt(dist / (sPt * sPx * sPx * nbChannels)) / 255.f;
}

float DatabaseNormalizedPatchManager::getValueAtDimLocal(unsigned id, unsigned dim)
{
	unsigned px, py, pt, pc;
	vid->sz.coords(id, px, py, pt, pc);

	unsigned hx, hy, ht, hc;
	unsigned l = dim;
	hx = l % sizePatch;
	l /= sizePatch;
	hy = l % sizePatch;
	l /= sizePatch;
	ht = l % sizePatchTime;
	l /= sizePatchTime;
	hc = l;
	return (*vid)(px + hx, py + hy, pt + ht, hc);
}

int DatabaseNormalizedPatchManager::getType()
{
	return 5;
}

void DatabaseNormalizedPatchManager::computeRealDistance(std::vector<float>& distances, std::vector<unsigned> patches, unsigned pidx)
{
	for(int i = 0; i < patches.size(); ++i)
	{
		distances[i] = distance(pidx, patches[i]);
	}
}

unsigned DatabaseNormalizedPatchManager::getNeighboringPatch(unsigned patch, int type, int sp, int st)
{
	unsigned px,py,pt,pc;
	vid->sz.coords(patch, px, py, pt, pc);

	int width = vid->sz.width - sizePatch + 1;
	int height = vid->sz.height - sizePatch + 1;
	int time = vid->sz.frames - sizePatchTime + 1;

	if(type == 0)
	{
		if(px+sp < width)
			return vid->sz.index(px+sp,py,pt,0);
		return patch;
	}
	else if(type == 1)
	{
		if(py+sp < height)
			return vid->sz.index(px,py+sp,pt,0);
		return patch;
	}
	else if(type == 2)
	{
		if(pt+sp < time)
			return vid->sz.index(px,py,pt+st,0);
		return patch;
	}
	else if(type == 3)
	{
		if(((int)px)-sp >= 0)
			return vid->sz.index(px-sp,py,pt,0);
		return patch;
	}
	else if(type == 4)
	{
		if(((int)py)-sp >= 0)
			return vid->sz.index(px,py-sp,pt,0);
		return patch;
	}
	else if(type == 5)
	{
		if(((int)pt)-st >= 0)
			return vid->sz.index(px,py,pt-st,0);
		return patch;
	}
	else
	{
		// This type of propagation doesn't exist ...
		return 0;
	}
}

unsigned DatabaseNormalizedPatchManager::getInverseNeighboringPatch(unsigned patch, int type, int sp, int st)
{
	unsigned px,py,pt,pc;
	vid->sz.coords(patch, px, py, pt, pc);

	int width = vid->sz.width - sizePatch + 1;
	int height = vid->sz.height - sizePatch + 1;
	int time = vid->sz.frames - sizePatchTime + 1;

	if(type == 3)
	{
		if(px+sp < width)
			return vid->sz.index(px+sp,py,pt,0);
		return patch;
	}
	else if(type == 4)
	{
		if(py+sp < height)
			return vid->sz.index(px,py+sp,pt,0);
		return patch;
	}
	else if(type == 5)
	{
		if(pt+sp < time)
			return vid->sz.index(px,py,pt+st,0);
		return patch;
	}
	else if(type == 0)
	{
		if(((int)px)-sp >= 0)
			return vid->sz.index(px-sp,py,pt,0);
		return patch;
	}
	else if(type == 1)
	{
		if(((int)py)-sp >= 0)
			return vid->sz.index(px,py-sp,pt,0);
		return patch;
	}
	else if(type == 2)
	{
		if(((int)pt)-st >= 0)
			return vid->sz.index(px,py,pt-st,0);
		return patch;
	}
	else
	{
		// This type of propagation doesn't exist ...
		return 0;
	}
}

void DatabaseNormalizedPatchManager::getNeighbors(std::vector<std::pair<unsigned, int> >& listNeighbors, unsigned pidx)
{
	unsigned px,py,pt,pc;
	vid->sz.coords(pidx, px, py, pt, pc);

	if((int)px + sizePatch/2 <= (int)vid->sz.width - sizePatch)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px + sizePatch/2, py, pt, 0), 0));
	}	
	if((int)py + sizePatch/2 <= (int)vid->sz.height - sizePatch)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px, py + sizePatch/2, pt, 0), 1));
	}
	if((int)pt + sizePatchTime/2 <= (int)vid->sz.frames - sizePatchTime)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px, py, pt + sizePatchTime/2, 0), 2));
	}
	if((int)px - sizePatch/2 >= 0)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px - sizePatch/2, py, pt, 0), 3));
	}
	if((int)py - sizePatch/2 >= 0)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px, py - sizePatch/2, pt, 0), 4));
	}
	if((int)pt - sizePatchTime/2 >= 0)
	{
		listNeighbors.push_back(std::make_pair(vid->sz.index(px, py, pt - sizePatchTime/2, 0), 5));
	}
}

// Not defined because it is not used at the moment (only for the DR case)
int DatabaseNormalizedPatchManager::hashPatch(unsigned pidx, bool useRot, std::vector<std::vector<float> >& rot, int nbTop, int& secondBin, std::vector<std::pair<int,int> >& otherPossibilities, bool getMorePatches)
{
	// TODO
	secondBin = 0;
	return 0;
}

DatabaseNormalizedPatchManager::~DatabaseNormalizedPatchManager()
{

}

void DatabaseNormalizedPatchManager::setCurrentVideo(Video<float>& cur)
{
	current = &cur;

	int width = current->sz.width - sizePatch + 1;
	int height = current->sz.height - sizePatch + 1;
	int time = current->sz.frames - sizePatchTime + 1;

	meanCoeff2.resize(width, height, current->sz.frames, nbChannels);

	// Compute the mean for each patch of the reference video
	for(unsigned px = 0, i = 0; px < width; ++px)
		for(unsigned py = 0; py < height; ++py)
			for(unsigned pt = 0; pt < current->sz.frames; ++pt, ++i)
				for(unsigned pc = 0; pc < nbChannels; ++pc)
				{
					float mean = 0.f;
					for (unsigned hy = 0, u = 1; hy < sizePatch; hy++)
						for (unsigned hx = 0; hx < sizePatch; hx++, ++u)
						{
							mean += ((*current)(px + hx, py + hy, pt, pc) - mean) / (float)u;
						}
					meanCoeff2(px,py,pt,pc) = mean;
				}
}
