/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file normalizedDimReducPatchManager.cpp
 * @brief Patch manager using normalized dimensionality reducted patches (using a PCA estimation)
 * 
 * @author Thibaud Ehret <ehret.thibaud@gmail.com>
 **/

#include "normalizedDimReducPatchManager.h"

static int rand_gen(int i)
{
	return std::rand() % i;
}

NormalizedDimReducPatchManager::NormalizedDimReducPatchManager(Video<float> const& vid_, int sizeP, int sizePt, int channels, int nbd)
{
	
	//Select some patches on which do the PCA dimension reduction
	// Construct the set of all patches
	
	vid = &vid_;
	
	int width = vid->sz.width - sizeP + 1;
	int height = vid->sz.height - sizeP + 1;
	int time = vid->sz.frames - sizePt + 1;

	sizePatch = sizeP;
	sizePatchTime = sizePt;
	nbChannels = channels;

	nbDim = nbd;


	smallPatches = std::vector<std::vector<float> >(width * height * time, std::vector<float>(nbDim));

	allPatches = std::vector<unsigned>(width * height * time);

	for(unsigned px = 0, i = 0; px < width; ++px)
		for(unsigned py = 0; py < height; ++py)
			for(unsigned pt = 0; pt < time; ++pt, ++i)
				allPatches[i] = vid->sz.index(px, py, pt, 0);
	
	// Get something like 1000 patches from the all set
	std::srand( unsigned(std::time(0))); 
	std::random_shuffle(allPatches.begin(), allPatches.end(), rand_gen);

	std::vector<float> samples(NBSAMPLES * sizeP * sizeP * sizePt * nbChannels);
	
	const unsigned w   = vid->sz.width;
	const unsigned wh  = vid->sz.wh;
	const unsigned whc = vid->sz.whc;

	for (unsigned c  = 0; c < nbChannels; c++)
	for (unsigned ht = 0, k = 0; ht < sizePt; ht++)
	for (unsigned hy = 0;        hy < sizeP; hy++)
	for (unsigned hx = 0;        hx < sizeP; hx++)
	for (unsigned n  = 0; n < NBSAMPLES; n++, k++)
		samples[k] = (*vid)(c * wh + allPatches[n] + ht * whc + hy * w + hx);
	const unsigned sPC = sizeP * sizeP * sizePt * nbChannels;

	matWorkspace mat;
	mat.groupTranspose.resize(NBSAMPLES * sPC);
	mat.tmpMat          .resize(NBSAMPLES * sPC);
	mat.covMat          .resize(NBSAMPLES * sPC);
	mat.covMatTmp       .resize(NBSAMPLES * sPC);
	mat.baricenter      .resize(sPC);

	centerData(samples, mat.baricenter, NBSAMPLES, sPC);
	covarianceMatrix(samples, mat.covMat, NBSAMPLES, sPC);
	int info = matrixEigs(mat.covMat, sPC, nbDim, mat.covEigVals, mat.covEigVecs);

	eigVectors = mat.covEigVecs;

	float mean[nbDim];
	for(int i = 0; i < nbDim; ++i)
		mean[i] = 0;

	for(unsigned position = 0; position < width*height*time; ++position)
	{
		float* eigenVec = mat.covEigVecs.data();
		for(unsigned k = 0; k < nbDim; ++k)
		{
			float proj = 0.f;
			for (unsigned c = 0, k = 0; c < nbChannels; c++)
				for (unsigned ht = 0; ht < sizePt; ht++)
					for (unsigned hy = 0; hy < sizeP; hy++)
						for (unsigned hx = 0; hx < sizeP; hx++)
						{
							proj += (*eigenVec * (*vid)(c * wh + ht * whc + hy * w + hx + allPatches[position]));
							++eigenVec;
						}
			smallPatches[position][k] = proj;
			mean[k] += (proj - mean[k]) / (float) (position + 1);
		}
	}

	for(unsigned position = 0; position < width*height*time; ++position)
	{
		for(unsigned k = 0; k < nbDim; ++k)
		{
			smallPatches[position][k] -= mean[k];
		}
	}


	
	reverseMapping.resize(vid->sz);
	for(unsigned i = 0; i < allPatches.size(); ++i)
	{
		reverseMapping(allPatches[i]) = i;
	}
}

void NormalizedDimReducPatchManager::getAllPatches(std::vector<unsigned>& allP)
{
	for(int i = 0; i < getNbPatches(); ++i)
	{
		allP[i] = i;
	}
}

int NormalizedDimReducPatchManager::getNbPatches()
{
	return smallPatches.size();
}

float NormalizedDimReducPatchManager::distance(unsigned pidx, unsigned id)
{
	unsigned corresPos = reverseMapping(pidx);

	float result = 0.f;
	float dif;
	for(unsigned i = 0; i < nbDim; ++i)
	{
		result += (dif = smallPatches[corresPos][i] - smallPatches[id][i]) * dif; 
	}
	return std::sqrt(result);
}

float NormalizedDimReducPatchManager::distanceOrigPos(unsigned pidx1, unsigned pidx2)
{
	unsigned corresPos1 = reverseMapping(pidx1);
	unsigned corresPos2 = reverseMapping(pidx2);

	float result = 0.f;
	float dif;
	for(unsigned i = 0; i < nbDim; ++i)
	{
		result += (dif = smallPatches[corresPos1][i] - smallPatches[corresPos2][i]) * dif; 
	}
	return std::sqrt(result);
}

float NormalizedDimReducPatchManager::distanceLocal(unsigned pidx1, unsigned pidx2)
{
	float result = 0.f;
	float dif;
	for(unsigned i = 0; i < nbDim; ++i)
	{
		result += (dif = smallPatches[pidx1][i] - smallPatches[pidx2][i]) * dif; 
	}
	return std::sqrt(result);
}

void NormalizedDimReducPatchManager::loadGroupStep1(std::vector<std::vector<float> >& group, std::vector<unsigned>& list, int kNN, unsigned pidx)
{
	// Local patches are not interesting, only return the true patches
	for(int i = 0; i < list.size(); ++i)
	{
		list[i] = allPatches[list[i]];
	}

	for (unsigned c  = 0; c < vid->sz.channels; c++)
		for (unsigned ht = 0, k = 0; ht < sizePatchTime; ht++)
			for (unsigned hy = 0;        hy < sizePatch; hy++)
				for (unsigned hx = 0;        hx < sizePatch; hx++)
					for (unsigned n = 0; n < kNN; n++, k++)
						group[c][k] = (*vid)(c * vid->sz.wh + list[n] + ht * vid->sz.whc + hy * vid->sz.width + hx);


}

void NormalizedDimReducPatchManager::loadGroupStep2(std::vector<float>& group, std::vector<unsigned>& list, Video<float> const& v, std::vector<float>& group2, int kNN, unsigned pidx)
{
	// Local patches are not interesting, only return the true patches
	for(int i = 0; i < list.size(); ++i)
	{
		list[i] = allPatches[list[i]];
	}

	for (unsigned c  = 0, k = 0; c < vid->sz.channels; c++)
		for (unsigned ht = 0; ht < sizePatchTime; ht++)
			for (unsigned hy = 0;        hy < sizePatch; hy++)
				for (unsigned hx = 0;        hx < sizePatch; hx++)
					for (unsigned n = 0; n < kNN; n++, k++)
					{
						group[k] = (*vid)(c * vid->sz.wh + list[n] + ht * vid->sz.whc + hy * vid->sz.width + hx);
						group2[k] = v(c * vid->sz.wh + list[n] + ht * vid->sz.whc + hy * vid->sz.width + hx);
					}

}

int NormalizedDimReducPatchManager::getDim()
{
	return nbDim;
}

float NormalizedDimReducPatchManager::getValueAtDim(unsigned id, unsigned dim)
{
	return smallPatches[reverseMapping(id)][dim];
}

float NormalizedDimReducPatchManager::getValueAtDimLocal(unsigned id, unsigned dim)
{
	return smallPatches[id][dim];
}

float NormalizedDimReducPatchManager::realDistance(unsigned patch1, unsigned id)
{
	unsigned patch2 = allPatches[id];	

	unsigned px, py, pt, pc;
	vid->sz.coords(patch1, px, py, pt, pc);

	unsigned qx, qy, qt, qc; 
	vid->sz.coords(patch2, qx, qy, qt, qc);

	const int sPx = sizePatch;
	const int sPt = sizePatchTime;

	float dist = 0.f, dif;
	for (unsigned hc = 0; hc < nbChannels; ++hc)
		for (unsigned ht = 0; ht < sPt; ht++)
			for (unsigned hy = 0; hy < sPx; hy++)
				for (unsigned hx = 0; hx < sPx; hx++)
					dist += (dif = (*vid)(px + hx, py + hy, pt + ht, hc)
							- (*vid)(qx + hx, qy + hy, qt + ht, hc)) * dif;
	return std::sqrt(dist / (sPt * sPx * sPx * nbChannels)) / 255.f;

}

int NormalizedDimReducPatchManager::getType()
{
	return 3;
}

void NormalizedDimReducPatchManager::computeRealDistance(std::vector<float>& distances, std::vector<unsigned> patches, unsigned pidx)
{
	for(int i = 0; i < patches.size(); ++i)
	{
		distances[i] = realDistance(pidx, patches[i]);
	}
}

unsigned NormalizedDimReducPatchManager::getNeighboringPatch(unsigned patch, int type, int sp, int st)
{
	unsigned realId = allPatches[patch];
	unsigned px,py,pt,pc;
	vid->sz.coords(realId, px, py, pt, pc);
	if(type == 0)
	{
		unsigned newId = vid->sz.index(px+sp,py,pt,0);
		return reverseMapping(newId);
	}
	else if(type == 1)
	{
		unsigned newId = vid->sz.index(px,py+sp,pt,0);
		return reverseMapping(newId);
	}
	else if(type == 2)
	{
		unsigned newId = vid->sz.index(px,py,pt+st,0);
		return reverseMapping(newId);
	}
	else if(type == 3)
	{
		unsigned newId = vid->sz.index(px-sp,py,pt,0);
		return reverseMapping(newId);
	}
	else if(type == 4)
	{
		unsigned newId = vid->sz.index(px,py-sp,pt,0);
		return reverseMapping(newId);
	}
	else if(type == 5)
	{
		unsigned newId = vid->sz.index(px,py,pt-st,0);
		return reverseMapping(newId);
	}
	else
	{
		// This type doesn't correspond to anything
		return 0;
	}
}

unsigned NormalizedDimReducPatchManager::getInverseNeighboringPatch(unsigned patch, int type, int sp, int st)
{
	unsigned realId = allPatches[patch];
	unsigned px,py,pt,pc;
	vid->sz.coords(realId, px, py, pt, pc);
	if(type == 3)
	{
		unsigned newId = vid->sz.index(px+sp,py,pt,0);
		return reverseMapping(newId);
	}
	else if(type == 4)
	{
		unsigned newId = vid->sz.index(px,py+sp,pt,0);
		return reverseMapping(newId);
	}
	else if(type == 5)
	{
		unsigned newId = vid->sz.index(px,py,pt+st,0);
		return reverseMapping(newId);
	}
	else if(type == 0)
	{
		unsigned newId = vid->sz.index(px-sp,py,pt,0);
		return reverseMapping(newId);
	}
	else if(type == 1)
	{
		unsigned newId = vid->sz.index(px,py-sp,pt,0);
		return reverseMapping(newId);
	}
	else if(type == 2)
	{
		unsigned newId = vid->sz.index(px,py,pt-st,0);
		return reverseMapping(newId);
	}
	else
	{
		// This type doesn't correspond to anything
		return 0;
	}

}

void NormalizedDimReducPatchManager::getNeighbors(std::vector<std::pair<unsigned, int> >& listNeighbors, unsigned pidx)
{
	unsigned px,py,pt,pc;
	vid->sz.coords(pidx, px, py, pt, pc);

	if(px + sizePatch/2 <= vid->sz.width - sizePatch)
	{
		listNeighbors.push_back(std::make_pair(reverseMapping(vid->sz.index(px + sizePatch/2, py, pt, 0)), 0));
	}	
	else if(py + sizePatch/2 <= vid->sz.height - sizePatch)
	{
		listNeighbors.push_back(std::make_pair(reverseMapping(vid->sz.index(px, py + sizePatch/2, pt, 0)), 1));
	}
	else if(pt + sizePatchTime/2 <= vid->sz.frames - sizePatchTime)
	{
		listNeighbors.push_back(std::make_pair(reverseMapping(vid->sz.index(px, py, pt + sizePatchTime/2, 0)), 2));
	}
	else if(px - sizePatch/2 >= 0)
	{
		listNeighbors.push_back(std::make_pair(reverseMapping(vid->sz.index(px - sizePatch/2, py, pt, 0)), 3));
	}
	else if(py - sizePatch/2 >= 0)
	{
		listNeighbors.push_back(std::make_pair(reverseMapping(vid->sz.index(px, py - sizePatch/2, pt, 0)), 4));
	}
	else if(pt - sizePatchTime/2 >= 0)
	{
		listNeighbors.push_back(std::make_pair(reverseMapping(vid->sz.index(px, py, pt - sizePatchTime/2, 0)), 5));
	}
}

int NormalizedDimReducPatchManager::hashPatch(unsigned pidx, bool useRot, std::vector<std::vector<float> >& rot, int nbTop, int& secondBin, std::vector<std::pair<int,int> >& otherOpportunities, bool getMorePatches)
{
	// FIXME These are fixed for now, meaning the vectors used are only to be of length 64 and G can be up to 5
	int coeff[] = {1, 64, 4096, 262144, 16777216};
	int coeff2[] = {1, 2, 4, 8, 16, 32};

	// load the patch in a vector
	std::vector<std::pair<float,int> > v(nbDim);

	if(useRot)
	{
		// Compute the rotation of the patch
		for(int i = 0; i < nbDim; ++i)
		{
			float res = 0;
			for(int j = 0; j < nbDim; ++j)
			{
				res += smallPatches[pidx][j] * rot[i][j];	
			}
			v[i] = std::make_pair(res, i);
		}
	}
	else
	{
		for(int i = 0; i < nbDim; ++i)
		{
			v[i] = std::make_pair(std::abs(smallPatches[pidx][i]), i);
		}
	}

	// find the nbTop maximum value in the patch
	// and return the corresponding index
	std::partial_sort(v.begin(), v.begin() + nbTop, v.end(), comparaisonFirst);
	
	std::vector<int> top(nbTop);
	for(int i = 0; i < nbTop; ++i)
		top[i] = v[i].second;

	secondBin = 0;
	for(int counting = 0; counting < nbTop; ++counting)
		secondBin += ((smallPatches[pidx][top[counting]] < 0) ? 0 : 1) * coeff2[counting];	

	int firstBin = 0;
	for(int counting = 0; counting < nbTop; ++counting)
		firstBin += top[counting] * coeff[counting];
	
	// Create a list of second hand bin that can be of interest
	if(getMorePatches)
	{
		top.resize(nbTop - 1);	
		for(int i = 0; i < nbTop - 1; ++i)
			top[i] = v[i].second;
		std::sort(top.begin(), top.end());

		int positionWhereToAdd = 0;
		for(int addingThisOne = 0; addingThisOne < 64; ++addingThisOne)
		{
			if(addingThisOne == top[positionWhereToAdd])
			{
				positionWhereToAdd++;
				continue;
			}
			// Compute the position of the bins
			int fb = 0;
			int sb = 0;
			for(int plouf = 0; plouf < positionWhereToAdd; ++plouf)
			{
				fb += top[plouf] * coeff[plouf];
				sb += ((smallPatches[pidx][top[plouf]] < 0) ? 0 : 1) * coeff2[plouf];
			}
			fb += addingThisOne * coeff[positionWhereToAdd];
			sb += ((smallPatches[pidx][addingThisOne] < 0) ? 0 : 1) * coeff2[positionWhereToAdd];
			for(int plouf = positionWhereToAdd + 1; plouf < nbTop - 1; ++plouf)
			{
				fb += top[plouf - 1] * coeff[plouf];
				sb += ((smallPatches[pidx][top[plouf - 1]] < 0) ? 0 : 1) * coeff2[plouf];
			}
			otherOpportunities.push_back(std::make_pair(fb,sb));
		}
	}

	return firstBin;
}

NormalizedDimReducPatchManager::~NormalizedDimReducPatchManager()
{

}

int NormalizedDimReducPatchManager::exportPM(char* path)
{

	FILE *f;
	f = fopen(path, "w");

	// Export type for verification purpose
	fprintf(f, "%d\n", 0);

	fprintf(f, "%d\n", nbDim);
	fprintf(f, "%d\n", eigVectors.size());

	for(int i = 0; i < eigVectors.size(); ++i)
		fprintf(f, "%f ", eigVectors[i]);

	fclose(f);
}

NormalizedDimReducPatchManager::NormalizedDimReducPatchManager(Video<float> const& vid_, int sizeP, int sizePt, int channels, int nbd, char* path)
{
	
	//Select some patches on which do the PCA dimension reduction
	// Construct the set of all patches
	
	vid = &vid_;
	
	int width = vid->sz.width - sizeP + 1;
	int height = vid->sz.height - sizeP + 1;
	int time = vid->sz.frames - sizePt + 1;

	sizePatch = sizeP;
	sizePatchTime = sizePt;
	nbChannels = channels;

	nbDim = nbd;


	smallPatches = std::vector<std::vector<float> >(width * height * time, std::vector<float>(nbDim));

	allPatches = std::vector<unsigned>(width * height * time);

	for(unsigned px = 0, i = 0; px < width; ++px)
		for(unsigned py = 0; py < height; ++py)
			for(unsigned pt = 0; pt < time; ++pt, ++i)
				allPatches[i] = vid->sz.index(px, py, pt, 0);
	
	// Get something like 1000 patches from the all set
	const unsigned w   = vid->sz.width;
	const unsigned wh  = vid->sz.wh;
	const unsigned whc = vid->sz.whc;

	FILE *f;
	f = fopen(path, "w");

	int tp;
	fscanf(f, "%d\n", &tp);
	int allDim;
	fscanf(f, "%d\n", &allDim);
	if(tp != 0 || allDim != nbd)
	{
		printf("Incompatible file, rebuilding the patch manager\n");
		std::srand( unsigned(std::time(0))); 
		std::random_shuffle(allPatches.begin(), allPatches.end(), rand_gen);

		std::vector<float> samples(NBSAMPLES * sizeP * sizeP * sizePt * nbChannels);

		for (unsigned c  = 0; c < nbChannels; c++)
			for (unsigned ht = 0, k = 0; ht < sizePt; ht++)
				for (unsigned hy = 0;        hy < sizeP; hy++)
					for (unsigned hx = 0;        hx < sizeP; hx++)
						for (unsigned n  = 0; n < NBSAMPLES; n++, k++)
							samples[k] = (*vid)(c * wh + allPatches[n] + ht * whc + hy * w + hx);
		const unsigned sPC = sizeP * sizeP * sizePt * nbChannels;

		matWorkspace mat;
		mat.groupTranspose.resize(NBSAMPLES * sPC);
		mat.tmpMat          .resize(NBSAMPLES * sPC);
		mat.covMat          .resize(NBSAMPLES * sPC);
		mat.covMatTmp       .resize(NBSAMPLES * sPC);
		mat.baricenter      .resize(sPC);

		centerData(samples, mat.baricenter, NBSAMPLES, sPC);
		covarianceMatrix(samples, mat.covMat, NBSAMPLES, sPC);
		int info = matrixEigs(mat.covMat, sPC, nbDim, mat.covEigVals, mat.covEigVecs);

		eigVectors = mat.covEigVecs;
	}
	else
	{
		int szEigVectors;
		fscanf(f, "%d\n", &szEigVectors);

		for(int i = 0; i < szEigVectors; ++i)
			fscanf(f, "%f ", &eigVectors[i]);
	}

	fclose(f);


	for(unsigned position = 0; position < width*height*time; ++position)
	{
		float* eigenVec = eigVectors.data();
		for(unsigned k = 0; k < nbDim; ++k)
		{
			float proj = 0.f;
			for (unsigned c = 0, k = 0; c < nbChannels; c++)
				for (unsigned ht = 0; ht < sizePt; ht++)
					for (unsigned hy = 0; hy < sizeP; hy++)
						for (unsigned hx = 0; hx < sizeP; hx++)
						{
							proj += (*eigenVec * (*vid)(c * wh + ht * whc + hy * w + hx + allPatches[position]));
							++eigenVec;
						}
			smallPatches[position][k] = proj;
		}
	}
	
	reverseMapping.resize(vid->sz);
	for(unsigned i = 0; i < allPatches.size(); ++i)
	{
		reverseMapping(allPatches[i]) = i;
	}
}
