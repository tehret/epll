/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VPTREE_HEADER_H
#define VPTREE_HEADER_H

#include "partitionTree.h"
#include <queue>
#include "../../NlBayes/nlbParams.h"
#include "prmTree.h"
#include <cfloat>
#include <stack>

using namespace VideoNLB;


/**
 *
 * VP-tree
 *
 */

class VPTree : public PartitionTree
{
	public:
		/**
		 * @brief Constructors: It is recommended to set epsilon to 0 for now, not everything has been set up for epsilon > 0
		 *
		 * @param pm_: Patch manager on which the tree is constructed
		 * @param p_param: Parameters from the denoising algorithms
		 * @param nbVP: Number of candidate vantage points used during the computation of the VP-tree
		 * @param subS: Number of distance to compute to estimate the median distance during the computation of the VP-tree
		 *
		 * @{
		 */
		VPTree(float epsilon, PatchManager& pm_, const nlbParams &p_param, int nbVP, int subS);
		VPTree(PatchManager& pm_, PrmTree& paramTree);
		/**
		 * @}
		 */

		/**
		 * Same as the previous constructors but the maximum capacity of the leaves is set up manually
		 *
		 * @param sizeLeaves: The maximum size of a bin (in number of elements)
		 */
		VPTree(float epsilon, PatchManager& pm_, const nlbParams &p_param, int nbVP, int subS, int sizeLeaves);

		/**
		 * 
		 * @brief Same as the previous constructors without computation, the tree is loaded from the data pointed by the path
		 * @{
		 */
		VPTree(PatchManager& pm_, PrmTree& paramTree, char* path);
		VPTree(float epsilon, PatchManager& pm_, const nlbParams &p_param, int nbVP, int subS, char* path);
		VPTree(float epsilon, PatchManager& pm_, const nlbParams &p_param, int nbVP, int subS, int sizeLeaves, char* path);
		/**
		 * @}
		 */

		/**
		 *
		 * Check partitionTree.h for informations on the following functions
		 * @{
		 */
		int estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank);
		int estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank);
		int estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool rerank);
		int backpropagation(unsigned pidx, std::vector<unsigned>& nearestNeigh, std::vector<float>& distancesTo);
		unsigned whichBin(unsigned pidx);
		int retrieveFromTree(std::vector<std::pair<float, unsigned> >& localResult, const unsigned pidx, bool rerank);
		int estimateClassicFromBins(std::vector<float> &output, std::vector<unsigned> &index, std::vector<unsigned> bins, const unsigned pidx, bool rerank);


		void retrieveNode(std::vector<unsigned>::iterator& beg, std::vector<unsigned>::iterator& end, unsigned bin);
		void typeBallToVideo(Video<float>& output);
		void groupingType2Balls(std::vector<std::vector<unsigned> >& indexes);

		void extractBallsType2(std::vector<std::vector<unsigned> >& listPatches, float epsilon);

		int exportTree(char* path);
		void updatePM(PatchManager& pm_) {pm = &pm_;};
		VPTree* copy() {return new VPTree(*this);};
		/**
		 * @}
		 */

		~VPTree()
		{
			root->~Node();
		}
	private:
		/**
		 * @brief Structures of the node defining a KD-tree
		 *
		 * @param id: Each node of a tree has a unique id
		 * @param left,right: Beginning and end of the bin (if the node is a leaf) in the list of patches
		 * @param vantagePoint: Index corresponding to the vantage point of the split
		 * @param splitDistance: Distance to the vantage point separating the left children to the right children 
		 * @param diameter: diameter of the ball containing the Node
		 * @param child1: Pointer to the left child of this node in the tree
		 * @param child2: Pointer to the right child of this node in the tree
		 * @param stopCondition: Is 1 if the stop condition is not enough elements left to construct a new Node, 2 if the diameter is small enough (require setting an epsilon)
		 */
		struct Node
		{
			unsigned id;
			unsigned left, right;

			unsigned vantagePoint;
			float splitDistance;

			float diameter;

			struct Node* child1;
			struct Node* child2;	

			int stopCondition;

			~Node()
			{
				if (child1) child1->~Node();
				if (child2) child2->~Node();
			}
		};

		/// For fast access of the bins of the tree
		std::vector<Node*> fastAccessBins;

		/// Number of elements to be returned for each search
		int kNN;
		/// Max nb of elem in a leaf
		int leaf_max_size;
		/// Max diameter of a leaf
		float leaf_max_diameter;
		/// Nb of selection for the vp
		const int nbSelectionVP;
		/// Nb of points to consider during the creation of the tree
		const int subSampling;

		/// Root of the KDTree 
		Node* root;

		nlbParams param;

		/// PatchManager
		PatchManager* pm;

		/// index of the elements on which the tree is based
		std::vector<unsigned> idxPatches;

		/// Move the elements according to their position relatively to the pivot
		unsigned partition(unsigned left, unsigned right, unsigned pivot, std::vector<float>& distances, unsigned n, unsigned l);

		/// Fast computation of the median along a specific dimension for a set of element delimited by left and right 
		float quickselect(unsigned left, unsigned right, std::vector<float>& distances);

		/// Split the elements recursively and return a node each time, main function used to construct the tree 
		Node* splitTree(unsigned left, unsigned right, float diameter, unsigned& binId, unsigned& type2);

		/// Return the index to the vantage point that will be used for the construction of the node
		unsigned computeVP(unsigned left, unsigned right);
		/// Used to do back tracking if needed, not useful for the patch search studied 
		int investigate(unsigned pidx, std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs>& file, Node* here, Node* avoid);
		/// Load data from the given file to create a tree
		int load(FILE* f);

		/// Not useful in the current framework
		void recExtractBallsType2(std::vector<std::vector<unsigned> >& listPatches, float epsilon, Node* no);
};

#endif
