/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef FORESTMANAGER_HEADER_H
#define FORESTMANAGER_HEADER_H

#include <vector>
#include "partitionTree.h"
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <queue>
#include "../Utilities.h"
#include "../../NlBayes/nlbParams.h"
#include "../PatchManager/patchManager.h"
#include <unordered_map>
#include <queue>

#ifdef _OPENMP
#include <omp.h>
#endif

using namespace VideoNLB;

class ComparePairsFast {
	public:
		bool operator()(std::pair<int,unsigned>& a, std::pair<int,unsigned>& b)
		{
			return a.first > b.first;
		}
};

/**
 *
 * Used to manipulate multiple partition trees
 *
 */

class ForestManager
{
	public:	

		/**
		 * @brief Construct a basic forest manager from a list of trees
		 *
		 * @param pm_: The patch manager to be used with the forest (it is not necessarely the same as the one used for the construction of the trees)
		 * @param forest_: The list of trees to be used to create the forest
		 * @param kNN: The number of nearest neighbors to be computed during each search
		 * @param param: Parameter used to construct the trees
		 *
		 **/
		ForestManager(PatchManager& pm_, std::vector<PartitionTree*>& forest_, int kNN, nlbParams param);

		/**
		 * @brief Copy a forest manager
		 **/
		ForestManager(ForestManager& toCopy);

		/**
		 * @brief Empty constructor
		 **/

		ForestManager() {};

		/**
		 * @brief Compute the nearest neighbors and return them in the form used during the first of the denoising algorithm
		 *
		 * @param o_group3d: Used to return the patches corresponding to the nearest neighbors
		 * @param o_index: Used to return the position of the patches corresponding to the nearest neighbors
		 * @param pidx: The query patch
		 * @param rerank: If using a dimensionality reduction, rerank the results using the true distances
		 * @param improveLocal: If true a small search around pidx is done to find new candidates (overlaps with the search vith local reinforcement)
		 *
		 * @return the number of nearest neighbors computed, (currently it is always kNN if there wasn't any problem)
		 **/
		int estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool improveLocal = false);

		/**
		 * @brief Compute the nearest neighbors and return them in the form used during the second of the denoising algorithm
		 *
		 * @param i_imNoisy: the noisy dataset
		 * @param o_group3dNoisy: Used to return the noisy patches corresponding to the nearest neighbors
		 * @param o_group3dBasic: Used to return the patches from step 1 corresponding to the nearest neighbors
		 * @param o_index: Used to return the position of the patches corresponding to the nearest neighbors
		 * @param pidx: The query patch
		 * @param rerank: If using a dimensionality reduction, rerank the results using the true distances
		 * @param improveLocal: If true a small search around pidx is done to find new candidates (overlaps with the search vith local reinforcement)
		 *
		 * @return the number of nearest neighbors computed, (currently it is always kNN if there wasn't any problem)
		 **/
		int estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool improveLocal = false);

		/**
		 * @brief Compute the nearest neighbors and return their position and the distance to each of them
		 *
		 * @param output: Used to return the distance to the patches corresponding to the nearest neighbors
		 * @param index: Used to return the position of the patches corresponding to the nearest neighbors
		 * @param pidx: The query patch
		 * @param rerank: If using a dimensionality reduction, rerank the results using the true distances
		 * @param improveLocal: If true a small search around pidx is done to find new candidates (overlaps with the search vith local reinforcement)
		 *
		 * @return the number of nearest neighbors computed, (currently it is always kNN if there wasn't any problem)
		 **/
		int estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool rerank, bool improveLocal = false);

		/**
		 * @brief Compute the nearest neighbors using only specific bins and return their position and the distance to each of them
		 *
		 * @param output: Used to return the distance to the patches corresponding to the nearest neighbors
		 * @param index: Used to return the position of the patches corresponding to the nearest neighbors
		 * @param bins: List of the bins for each tree where the candidates are picked from
		 * @param pidx: The query patch
		 * @param rerank: If using a dimensionality reduction, rerank the results using the true distances
		 * @param improveLocal: If true a small search around pidx is done to find new candidates (overlaps with the search vith local reinforcement)
		 *
		 * @return the number of nearest neighbors computed, (currently it is always kNN if there wasn't any problem)
		 **/
		int estimateClassicFromBins(std::vector<float> &output, std::vector<unsigned> &index, std::vector<std::vector<unsigned> > bins, const unsigned pidx, bool rerank, bool verbose = false);

		/**
		 * @brief Compute the bins in which the query patch is
		 *
		 * @param bins: Used to return the bins
		 * @param pidx: The query patch
		 **/
		void getBins(std::vector<unsigned>& bins, unsigned pidx);

		/**
		 * @brief Compute the nearest neighbors and return their position and the distance to each of them
		 *
		 * @param output: Used to return the distance to the patches corresponding to the nearest neighbors
		 * @param index: Used to return the position of the patches corresponding to the nearest neighbors
		 * @param pidx: The query patch
		 * @param rerank: If using a dimensionality reduction, rerank the results using the true distances
		 * @param verbose: If true print some information during the computation
		 *
		 * @return the number of nearest neighbors computed, (currently it is always kNN if there wasn't any problem)
		 **/
		int retrieveFromForest(std::vector<float>& output, std::vector<unsigned>& index, const unsigned pidx, bool rerank, bool verbose = false);

		/**
		 * @brief Get the number of trees in the forest
		 *
		 * @return number of trees
		 **/
		int getNbTrees() { return forest.size(); };

		/**
		 *
		 * Not used in the current framework
		 *
		 **/
		void count(std::vector<unsigned>& listElements, std::unordered_map<unsigned, int>& mapping, const unsigned pidx, bool verbose = false);
		void countNeighbors(std::vector<unsigned>& listElements, std::unordered_map<unsigned, int>& mapping, int type, const unsigned pidx, bool verbose = false);
		void groupingType2Balls(std::vector<std::vector<unsigned> >& ball_indexes);
		void extractBallsType2(std::vector<std::vector<unsigned>>& listPatches, float epsilon);


		/**
		 * @brief Update the patch manager to a new one
		 *
		 * @param pm_: the new patch manager
		 **/
		void updatePM(PatchManager& pm_);

		
		/**
		 * @brief Get the patch manager used by the forest
		 *
		 * @return the patch manager
		 **/
		PatchManager* getpm() {return  pm;};
		int getkNN() {return kNN;};



		/**
		 * @brief Get the parameters used to create the forest manager
		 *
		 * @return the parameter
		 **/
		nlbParams getp_params() {return p_params;};

		/**
		 * @brief Access the trees contained in the forest
		 *
		 * @return list of pointer to the trees
		 **/
		std::vector<PartitionTree*> getforest() {return forest;};

	private:
		PatchManager *pm;
		std::vector<PartitionTree*> forest;
		int kNN;
		nlbParams p_params;

};
#endif
