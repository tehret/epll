/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file vptree.cpp
 * @brief Specific type of partition tree: VP-tree
 *
 * @author Thibaud Ehret <ehret.thibaud@gmail.com>
 **/

#include "vptree.h"

// TODO Searches can be speed up by using a reverse index directly to the bin

VPTree::VPTree(float epsilon, PatchManager &pm_, const nlbParams &p_param, int nbVP, int subS) : nbSelectionVP(nbVP), subSampling(subS)
{
	int k = p_param.nSimilarPatches; 
	std::srand( unsigned(std::time(0))); 

	kNN = k;
	// We assure that we have at least k elements in the bins with a median split
	leaf_max_size = 2*k + 1;

	leaf_max_diameter = epsilon;
	pm = &pm_;

	int nbPatches = pm->getNbPatches();

	param = p_param;

	idxPatches = std::vector<unsigned>(nbPatches);

	pm->getAllPatches(idxPatches);

	unsigned binId = 0;
	unsigned type2 = 0;
	root = splitTree(0, idxPatches.size()-1, FLT_MAX, binId, type2); 

}

VPTree::VPTree(PatchManager& pm_, PrmTree& paramTree) : nbSelectionVP(paramTree.nbVP), subSampling(paramTree.subS)
{
	float epsilon = paramTree.epsilon;
	nlbParams p_param = paramTree.prms;
	int k = p_param.nSimilarPatches; 
	std::srand( unsigned(std::time(0))); 

	kNN = k;
	// We assure that we have at least k elements in the bins with a median split
	leaf_max_size = 2*k + 1;

	leaf_max_diameter = epsilon;
	pm = &pm_;

	int nbPatches = pm->getNbPatches();

	param = p_param;

	idxPatches = std::vector<unsigned>(nbPatches);

	pm->getAllPatches(idxPatches);

	unsigned binId = 0;
	unsigned type2 = 0;
	root = splitTree(0, idxPatches.size()-1, FLT_MAX, binId, type2); 


}

VPTree::VPTree(float epsilon, PatchManager& pm_, const nlbParams &p_param, int nbVP, int subS, int sizeLeaves) : nbSelectionVP(nbVP), subSampling(subS)
{
	int k = p_param.nSimilarPatches; 
	// Initialize the random number generator
	std::srand( unsigned(std::time(0))); 

	kNN = k;
	leaf_max_size = sizeLeaves;

	leaf_max_diameter = epsilon;
	pm = &pm_;

	int nbPatches = pm->getNbPatches();

	param = p_param;

	idxPatches = std::vector<unsigned>(nbPatches);
	pm->getAllPatches(idxPatches);

	unsigned binId = 0;
	unsigned type2 = 0;
	root = splitTree(0, idxPatches.size()-1, FLT_MAX, binId, type2); 
}

static int rand_gen(int i)
{
	return std::rand() % i;
}

int VPTree::estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank)
{
	Node* currentNode = this->root; 
	// Find the leaf in which the patch resides
	while(currentNode->child1 != NULL && currentNode->child2 != NULL)
	{
		if(pm->distance(pidx, currentNode->vantagePoint) > currentNode->splitDistance)
			currentNode = currentNode->child2;
		else
			currentNode = currentNode->child1;
	}

	if(1 == currentNode->stopCondition)
	{
		// < 2*k element, therefore we keep the best ones

		std::vector<std::pair<float, unsigned> > distancePair(currentNode->right - currentNode->left + 1);

		// Do a bruteforce search inside this bin

		for(unsigned i = currentNode->left, j = 0; i <= currentNode->right; ++i,++j)
		{
			float dist = 0.f;
			if(rerank && pm->getType() != 0)
				dist = pm->realDistance(pidx,idxPatches[i]);
			else
				dist = pm->distance(pidx,idxPatches[i]);
			distancePair[j] = std::make_pair(dist, i);
		}
		std::partial_sort(distancePair.begin(), distancePair.begin() + kNN, distancePair.end(), comparaisonFirst);
		for (int i = 0; i < kNN; i++)
		{
			o_index[i] = idxPatches[distancePair[i].second];
		}
	}	
	else
	{
		// When the diameter of the bin is small enough, we just select uniformly a subset of kNN patches that will be used instead of the full hyperball (which can be costly) 
		std::vector<int> permu(currentNode->right - currentNode->left + 1);
		for(unsigned i = currentNode->left, j = 0; i <= currentNode->right; ++i,++j)
			permu[j] = i;
		std::random_shuffle(permu.begin(), permu.end(), rand_gen);
		for(int i = 0; i < kNN; ++i)
		{
			o_index[i] = idxPatches[permu[i]];
		}
	}

	pm->loadGroupStep1(o_group3d, o_index, kNN, pidx);

	return kNN;
}

int VPTree::estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank)
{
	Node* currentNode = root; 
	// Find the leaf in which the patch resides
	while(currentNode->child1 != NULL && currentNode->child2 != NULL)
	{
		if(pm->distance(pidx, currentNode->vantagePoint) > currentNode->splitDistance)
			currentNode = currentNode->child2;
		else
			currentNode = currentNode->child1;
	}

	if(1 == currentNode->stopCondition)
	{
		// < 2*k element, therefore we keep the best ones
		// Do a bruteforce search inside this bin
		std::vector<std::pair<float, unsigned> > distancePair(currentNode->right - currentNode->left + 1);
		for(unsigned i = currentNode->left; i <= currentNode->right; ++i)
		{
			float dist = 0.f;
			if(rerank && pm->getType() != 0)
				dist = pm->realDistance(pidx,idxPatches[i]);
			else
				dist = pm->distance(pidx,idxPatches[i]);
			distancePair[i - currentNode->left] = std::make_pair(dist, i);
		}
		std::partial_sort(distancePair.begin(), distancePair.begin() + kNN, distancePair.end(), comparaisonFirst);
		for (int i = 0; i < kNN; i++)
		{
			o_index[i] = idxPatches[distancePair[i].second];
		}
	}	
	else
	{
		// When the diameter of the bin is small enough, we just select uniformly a subset of kNN patches that will be used instead of the full hyperball (which can be costly) 
		std::vector<int> permu(currentNode->right - currentNode->left + 1);
		for(unsigned i = currentNode->left, j = 0; i <= currentNode->right; ++i,++j)
			permu[j] = i;
		std::random_shuffle(permu.begin(), permu.end(), rand_gen);
		for(int i = 0; i < kNN; ++i)
		{
			o_index[i] = idxPatches[permu[i]];
		}
	}
	pm->loadGroupStep2(o_group3dBasic, o_index, i_imNoisy, o_group3dNoisy, kNN, pidx);
	return kNN;
}

unsigned VPTree::partition(unsigned left, unsigned right, unsigned pivot, std::vector<float>& distances, unsigned n, unsigned l)
{
	float valuePivot = distances[pivot - l];

	unsigned temp = idxPatches[pivot];
	idxPatches[pivot] = idxPatches[right];
	idxPatches[right] = temp;
	float tempF = distances[pivot - l];
	distances[pivot - l] = distances[right - l];
	distances[right - l] = tempF;


	unsigned store = left;
	int allEqual = 0;

	// move all elements between $left$ and $right$ so they are at the correct position according to the pivot
	for(unsigned i = left; i <= right; ++i)
	{
		float candidate = distances[i - l];

		// trick in case all elements are equal to speed up the computation
		if(std::abs(candidate - valuePivot) < 0.000001)
			allEqual++;

		if(candidate < valuePivot)  
		{
			temp = idxPatches[store];
			idxPatches[store] = idxPatches[i];
			idxPatches[i] = temp;
			tempF = distances[store - l];
			distances[store - l] = distances[i - l];
			distances[i - l] = tempF;
			++store;
		}
	}

	temp = idxPatches[store];
	idxPatches[store] = idxPatches[right];
	idxPatches[right] = temp;
	tempF = distances[store - l];
	distances[store - l] = distances[right - l];
	distances[right - l] = tempF;

	if(allEqual >= (right - left) / 2)
		return n;

	// return the index where the pivot is
	return store;
}

// Fast computation of the median along a specific dimension for a set of element delimited by left and right 
float VPTree::quickselect(unsigned l, unsigned r, std::vector<float>& distances)
{
	unsigned right = r;
	unsigned left = l;
	if(l == r)
	{
		return distances[0];
	}	

	unsigned n = l + (r - l) / 2;
	int compteur = 0;
	while(1==1)
	{
		compteur++;
		unsigned pivot = left + (right - left) / 2;
		pivot = partition(left, right, pivot, distances, n, l);

		if(n  == pivot)
		{
			return distances[n - l];
		}
		else if(n < pivot)
		{
			right = pivot - 1;
		}
		else
		{
			left = pivot + 1;
		}
	}
}

VPTree::Node* VPTree::splitTree(unsigned left, unsigned right, float diameter, unsigned& binId, unsigned& type2)
{
	Node* tempNode = new Node();

	tempNode->diameter = diameter;

	// Test if we are creating a leaf or not
	if( (right - left + 1) <= leaf_max_size || diameter < leaf_max_diameter)
	{
		// It is a leaf, we set all informations regarding this leaf and finish
		tempNode->child1 = NULL;
		tempNode->child2 = NULL;

		if(diameter < leaf_max_diameter)
		{
			tempNode->stopCondition = 2;
			++type2;
		}
		else
		{
			tempNode->stopCondition = 1;
		}

		tempNode->left = left;
		tempNode->right = right;
		tempNode->vantagePoint = 0;
		tempNode->splitDistance = 0.;

		fastAccessBins.push_back(tempNode);
		tempNode->id = binId;
		++binId;
	}
	else
	{
		// It is not a leaf, create the split and the two children nodes
		std::vector<float> distances(right - left + 1);
		unsigned vantage = computeVP(left, right);

		float maxDist = 0.;
		for(unsigned i = 0, j = left; j <= right; ++i,++j)
		{
			distances[i] = pm->distanceLocal(vantage, idxPatches[j]);
			if(distances[i] > maxDist)
				maxDist = distances[i];
		}
		tempNode->left = left;
		tempNode->right = right;

		tempNode->vantagePoint = vantage;

		tempNode->splitDistance = quickselect(left, right, distances);
		// Create the nodes for the children 
		tempNode->child1 = splitTree(left, left + (right - left) / 2, tempNode->splitDistance, binId, type2);
		tempNode->child2 = splitTree(left + (right - left) / 2 + 1, right, maxDist, binId, type2);
	}
	
	// Return the pointer to the constructed node
	return tempNode;
}


unsigned VPTree::computeVP(unsigned left, unsigned right)
{
	std::random_shuffle(idxPatches.begin() + left, idxPatches.begin() + right + 1, rand_gen);
	std::vector<unsigned> elementToConsider(nbSelectionVP);
	std::vector<unsigned> subSample(std::min(right-left+1, (unsigned)subSampling));

	std::vector<float> distancesToSubs(std::min(right - left + 1, (unsigned)subSampling), 0.);

	for(int i = 0; i < nbSelectionVP; ++i)
		elementToConsider[i] = idxPatches[left + (unsigned)i];
	std::random_shuffle(idxPatches.begin() + left, idxPatches.begin() + right + 1, rand_gen);
	for(int i = 0; i < subSample.size(); ++i)
		subSample[i] = idxPatches[left + (unsigned)i];

	float maxVar = 0.;
	int interestingPoint = 0;
	if(nbSelectionVP == 1)
	{
		// Construct a VP tree for a forest, in this case no other computation need to be done
		return elementToConsider[0];
	}
	else
	{
		// Constructing a normal VP tree
		for(int i = 0; i < nbSelectionVP; ++i)
		{
			//Compute the distances to the point
			for(unsigned j = 0; j < subSample.size(); ++j)
			{
				distancesToSubs[j] = pm->distanceLocal(elementToConsider[i], subSample[j]); 
			}

			// compute the median
			float medianTemp = quickselect(left, left + distancesToSubs.size() - 1, distancesToSubs);	

			// compute the variance 
			float var = 0.;
			for(int j = 0; j < distancesToSubs.size(); ++j)
				var += (distancesToSubs[j] - medianTemp) * (distancesToSubs[j] - medianTemp);
			if(var > maxVar)
			{
				maxVar = var;
				interestingPoint = i;
			}
		}
	}	
	// Return the best VP candidate
	return elementToConsider[interestingPoint];
}

int VPTree::estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool rerank)
{
	Node* currentNode = this->root; 
	// Find the leaf in which the patch resides
	float tmp;
	while(currentNode->child1 != NULL && currentNode->child2 != NULL)
	{
		if((tmp = pm->distance(pidx, currentNode->vantagePoint)) > currentNode->splitDistance)
			currentNode = currentNode->child2;
		else
			currentNode = currentNode->child1;
	}


	const int sPx = param.sizePatch;
	const int sPt = param.sizePatchTime;

	if(1 == currentNode->stopCondition)
	{
		// < 2*k element, therefore we keep the best ones

		// Do a bruteforce search inside this bin
		std::vector<std::pair<float, unsigned> > distance(currentNode->right - currentNode->left + 1);
		for(int i = currentNode->left; i <= currentNode->right; ++i)
		{
			if(rerank && pm->getType() != 0)
				distance[i - currentNode->left] = std::make_pair(pm->realDistance(pidx, idxPatches[i]),i);
			else
				distance[i - currentNode->left] = std::make_pair(pm->distance(pidx, idxPatches[i]),i);
		}
		std::partial_sort(distance.begin(), distance.begin() + kNN, distance.end(), comparaisonFirst);
		for (int i = 0; i < kNN; i++)
		{
			if(rerank || pm->getType() == 0)
				output[i] = distance[i].first;
			else
				output[i] = pm->realDistance(pidx, idxPatches[distance[i].second]);
			index[i] = idxPatches[distance[i].second];
		}
	}	
	else
	{
		// TODO
	}

	return kNN;
}

int VPTree::backpropagation(unsigned pidx, std::vector<unsigned>& nearestNeigh, std::vector<float>& distancesTo)
{
	int nbDistanceComputation = 0;

	std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs> file;

	Node* currentNode = this->root; 
	// Find the leaf in which the patch resides, as well as the path to get there
	std::vector<Node*> backprop;
	while(currentNode->child1 != NULL && currentNode->child2 != NULL)
	{
		backprop.push_back(currentNode);
		if(pm->distance(pidx, currentNode->vantagePoint) > currentNode->splitDistance)
			currentNode = currentNode->child2;
		else
			currentNode = currentNode->child1;
	}
	backprop.push_back(currentNode);
	// Consider all the elements in the current bin ("the nearest neighbors are most likely these ones")
	for(unsigned i = currentNode->left; i <= currentNode->right;++i)
	{
		file.push(std::make_pair(pm->distance(pidx, idxPatches[i]), idxPatches[i]));
		if(file.size() > kNN)
			file.pop();
	}
	nbDistanceComputation += (currentNode->right - currentNode->left + 1);

	// Backtrack in the tree using the path computed previously to consider other interesting branches
	for(int it = backprop.size() - 2; it >= 0; --it)
	{	
		nbDistanceComputation += investigate(pidx, file, backprop[it], backprop[it + 1]);
	}

	
	for(int it = kNN-1; it >= 0; --it)
	{
		std::pair<float, unsigned> temp = file.top();
		nearestNeigh[it] = temp.second;
		distancesTo[it] = temp.first;
		file.pop();
	}
	return nbDistanceComputation; 
}

int VPTree::investigate(unsigned pidx, std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs>& file, Node* here, Node* avoid)
{
	int nbDistanceComputation = 0;
	Node* nextNode;
	if(here->child1 != avoid)
	{
		nextNode = here->child1;
		// If the node we are considering is a leaf 
		if(nextNode->child1 == NULL)
		{
			// Consider all the elements in the current bin
			for(unsigned it = nextNode->left; it <= nextNode->right; ++it)
			{
				file.push(std::make_pair(pm->distance(pidx, idxPatches[it]), idxPatches[it]));
				if(file.size() > kNN)
					file.pop();
			}
			nbDistanceComputation += (nextNode->right - nextNode->left + 1);
		}
		// If the ball is close enough to be relevant
		else if(pm->distance(pidx, nextNode->vantagePoint) - nextNode->diameter < file.top().first || file.size() < kNN)
		{
			nbDistanceComputation += investigate(pidx,file,nextNode,NULL) + 1;
		}
	}	
	if(here->child2 != avoid)
	{
		nextNode = here->child2;
		// If the node we are considering is a leaf 
		if(nextNode->child1 == NULL)
		{
			// Consider all the elements in the current bin
			for(unsigned it = nextNode->left; it <= nextNode->right; ++it)
			{
				file.push(std::make_pair(pm->distance(pidx, idxPatches[it]), idxPatches[it]));
				if(file.size() > kNN)
					file.pop();
			}
			nbDistanceComputation += (nextNode->right - nextNode->left + 1);
		}
		// If the ball is close enough to be relevant
		else if(pm->distance(pidx, nextNode->vantagePoint) - nextNode->diameter < file.top().first || file.size() < kNN)
		{
			nbDistanceComputation += investigate(pidx,file,nextNode,NULL) + 1;
		}
	}	
	return nbDistanceComputation;
}

unsigned VPTree::whichBin(unsigned pidx)
{
	Node* currentNode = this->root; 
	// Find the leaf in which the patch resides
	while(currentNode->child1 != NULL && currentNode->child2 != NULL)
	{
		if(pm->distance(pidx, currentNode->vantagePoint) > currentNode->splitDistance)
			currentNode = currentNode->child2;
		else
			currentNode = currentNode->child1;
	}
	return currentNode->id;
}


int VPTree::retrieveFromTree(std::vector<std::pair<float, unsigned> >& localResult, const unsigned pidx, bool rerank)
{
	Node* currentNode = this->root; 
	// Find the leaf in which the patch resides
	while(currentNode->child1 != NULL && currentNode->child2 != NULL)
	{
		if(pm->distance(pidx, currentNode->vantagePoint) > currentNode->splitDistance)
			currentNode = currentNode->child2;
		else
			currentNode = currentNode->child1;
	}

	const int sPx = param.sizePatch;
	const int sPt = param.sizePatchTime;


	if(1 == currentNode->stopCondition)
	{
		for(unsigned i = currentNode->left, j = 0; i <= currentNode->right; ++i,++j)
		{
			float dist;
			if(rerank && pm->getType() != 0)
				dist = pm->realDistance(pidx,idxPatches[i]);
			else
				dist = pm->distance(pidx,idxPatches[i]);
			localResult.push_back(std::make_pair(dist, idxPatches[i]));
		}
		return 1;
	}	
	else
	{
		// When the diameter of the bin is small enough, we just select uniformly a subset of kNN patches that will be used instead of the full hyperball (which can be costly) 
		std::vector<int> permu(currentNode->right - currentNode->left + 1);
		for(unsigned i = currentNode->left, j = 0; i <= currentNode->right; ++i,++j)
			permu[j] = i;
		std::random_shuffle(permu.begin(), permu.end(), rand_gen);
		for(int i = 0; i < kNN; ++i)
		{
			// The distance that we return in this case is 0 because we consider that the patches are the same modulo the noise
			localResult.push_back(std::make_pair(0.f,idxPatches[permu[i]]));
		}
		return 2;
	}
}

int VPTree::estimateClassicFromBins(std::vector<float> &output, std::vector<unsigned> &index, std::vector<unsigned> bins, const unsigned pidx, bool rerank)
{
	std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs> que;
	std::unordered_map<unsigned, int> alreadySeen;
	for(int i = 0; i < bins.size(); ++i)
	{
		int seen = (alreadySeen[bins[i]]++);
		// This bin has already been checked, no need to take a look at it again
		if(seen > 0)
			continue;
		
		Node* currentNode = fastAccessBins[bins[i]];
		if(1 == currentNode->stopCondition)
		{
			for(int j = currentNode->left; j <= currentNode->right; ++j)
			{
				float dist;
				if(rerank && pm->getType() != 0)
					que.push(std::make_pair(pm->realDistance(pidx, idxPatches[j]), idxPatches[j]));
				else
					que.push(std::make_pair(pm->distance(pidx, idxPatches[j]), idxPatches[j]));
				if(que.size() > kNN)
					que.pop();
			}
			for(int i = 0; i < kNN; ++i)
			{
				output[kNN-1-i] = que.top().first;
				index[kNN-1-i] = que.top().second;
				que.pop();
			}
		}	
		else
		{
			std::vector<int> permu(currentNode->right - currentNode->left + 1);
			for(unsigned i = currentNode->left, j = 0; i <= currentNode->right; ++i,++j)
				permu[j] = i;
			std::random_shuffle(permu.begin(), permu.end(), rand_gen);
			for(int i = 0; i < kNN; ++i)
			{
				// The distance that we return in this case is 0 because we consider that the patches are the same modulo the noise
				output[i] = 0.f;
				index[i] = idxPatches[permu[i]];
			}
		}
	}

	return kNN;	
}

void VPTree::retrieveNode(std::vector<unsigned>::iterator& beg, std::vector<unsigned>::iterator& end, unsigned bin)
{
	Node* current = fastAccessBins[bin];
	beg = idxPatches.begin() + current->left;
	end = idxPatches.begin() + current->right + 1;
}

int VPTree::exportTree(char* path)
{
	FILE *f;
	f = fopen(path, "w");

	// First line correspond to the type of the tree
	fprintf(f, "%d\n", 1);

	// FIXME Export all the parameters at some point?

	// export the index
	fprintf(f, "%d\n", idxPatches.size());
	for(int i = 0; i < idxPatches.size(); ++i)
		fprintf(f, "%u ", idxPatches[i]);
	fprintf(f, "\n");
	
	// export the tree in the following lines: one line for each node from up to bottom, left to right
	std::stack<Node*> lifo;
	lifo.push(root);

	while(!lifo.empty())
	{
		Node* current = lifo.top();
		lifo.pop();

		int info = 0;
		if(current->child1 == NULL && current->child2 == NULL)
			info = 1;

		fprintf(f, "%u %u %u %u %f %f %d %d\n", current->id, current->left, current->right, current->vantagePoint, current->splitDistance, current->diameter, current->stopCondition, info);
		if(info == 0)
		{
			lifo.push(current->child2);
			lifo.push(current->child1);
		}
	}

	fclose(f);
	return 0;
}

VPTree::VPTree(float epsilon, PatchManager &pm_, const nlbParams &p_param, int nbVP, int subS, char* path) : nbSelectionVP(nbVP), subSampling(subS)
{
	int k = p_param.nSimilarPatches; 
	// Initialize the random number generator
	std::srand( unsigned(std::time(0))); 

	kNN = k;
	// We assure that we have at least k elements in the bins with a median split
	leaf_max_size = 2*k+1;

	leaf_max_diameter = epsilon;
	pm = &pm_;

	FILE *f;
	f = fopen(path, "r");

	// Check if the type of the tree at 'path' is a VP-tree
	int tp = -1;
	if(f != NULL)
		fscanf(f, "%d\n", &tp);

	if(tp != 1)
	{
		printf("Wrong type of tree, the tree is rebuilt !\n");

		int nbPatches = pm->getNbPatches();
		idxPatches = std::vector<unsigned>(nbPatches);

		pm->getAllPatches(idxPatches);

		unsigned binId = 0;
		unsigned type2 = 0;
		root = splitTree(0, idxPatches.size()-1, FLT_MAX, binId, type2); 
	}
	else
	{
		load(f);
	}

	if(f != NULL)
		fclose(f);
}

VPTree::VPTree(PatchManager& pm_, PrmTree& paramTree, char* path) : nbSelectionVP(paramTree.nbVP), subSampling(paramTree.subS)
{
	float epsilon = paramTree.epsilon;
	nlbParams p_param = paramTree.prms;
	int k = p_param.nSimilarPatches; 
	// Initialize the random number generator
	std::srand( unsigned(std::time(0))); 

	kNN = k;
	// We assure that we have at least k elements in the bins with a median split
	leaf_max_size = 2*k+1;

	leaf_max_diameter = epsilon;
	pm = &pm_;

	param = p_param;

	FILE *f;
	f = fopen(path, "r");

	// Check if the type of the tree at 'path' is a VP-tree
	int tp = -1;
	if(f != NULL)
		fscanf(f, "%d\n", &tp);

	if(tp != 1)
	{
		printf("Wrong type of tree, the tree is rebuilt !\n");

		int nbPatches = pm->getNbPatches();
		idxPatches = std::vector<unsigned>(nbPatches);

		pm->getAllPatches(idxPatches);

		unsigned binId = 0;
		unsigned type2 = 0;
		root = splitTree(0, idxPatches.size()-1, FLT_MAX, binId, type2); 
	}
	else
	{
		load(f);
	}

	if(f != NULL)
		fclose(f);
}

VPTree::VPTree(float epsilon, PatchManager& pm_, const nlbParams &p_param, int nbVP, int subS, int sizeLeaves, char* path) : nbSelectionVP(nbVP), subSampling(subS)
{
	int k = p_param.nSimilarPatches; 
	// Initialize the random number generator
	std::srand( unsigned(std::time(0))); 

	kNN = k;
	leaf_max_size = sizeLeaves;

	leaf_max_diameter = epsilon;
	pm = &pm_;

	param = p_param;

	FILE *f;
	f = fopen(path, "r");

	// Check if the type of the tree at 'path' is a VP-tree
	int tp = -1;
	if(f != NULL)
		fscanf(f, "%d\n", &tp);

	if(tp != 1)
	{
		printf("Wrong type of tree, the tree is rebuilt !\n");

		int nbPatches = pm->getNbPatches();
		idxPatches = std::vector<unsigned>(nbPatches);

		pm->getAllPatches(idxPatches);

		unsigned binId = 0;
		unsigned type2 = 0;
		root = splitTree(0, idxPatches.size()-1, FLT_MAX, binId, type2); 
	}
	else
	{
		load(f);
	}

	if(f != NULL)
		fclose(f);
}

int VPTree::load(FILE* f)
{
	int nbPatches;
	fscanf(f, "%d\n", &nbPatches);

	idxPatches = std::vector<unsigned>(nbPatches);

	for(int i = 0; i < nbPatches; ++i)
	{
		fscanf(f, "%u ", &idxPatches[i]);	
	}

	std::stack<Node*> lifo;

	unsigned id;
	unsigned left, right;

	unsigned vantagePoint;
	float splitDistance;
	float diameter;
	int stopCondition;
	int info;
	fscanf(f, "%u %u %u %u %f %f %d %d\n", &id, &left, &right, &vantagePoint, &splitDistance, &diameter, &stopCondition, &info);
	root = new Node();
	root->id = id;
	root->left = left;
	root->right = right;
	root->vantagePoint = vantagePoint;
	root->splitDistance = splitDistance;
	root->diameter = diameter;
	root->stopCondition = stopCondition;
	root->child1 = NULL;
	root->child2 = NULL;

	lifo.push(root);


	while(fscanf(f, "%u %u %u %u %f %f %d %d\n", &id, &left, &right, &vantagePoint, &splitDistance, &diameter, &stopCondition, &info) > 0)
	{
		Node* previous = lifo.top();

		Node* current = new Node();
		current->id = id;
		current->left = left;
		current->right = right;
		current->vantagePoint = vantagePoint;
		current->splitDistance = splitDistance;
		current->diameter = diameter;
		current->stopCondition = stopCondition;
		current->child1 = NULL;
		current->child2 = NULL;

		if(previous->child1 != NULL)
		{
			// Node is full, remove it from the stack
			lifo.pop();
			previous->child2 = current;
		}
		else
		{
			previous->child1 = current;
		}

		// if the node is a leaf, construct the fast access array and not load it in the stack
		if(info > 0)
		{
			fastAccessBins.push_back(current);	
		}
		else
		{
			lifo.push(current);
		}
	}
}

void VPTree::typeBallToVideo(Video<float>& output)
{
	for(int i = 0; i < fastAccessBins.size(); ++i)
	{
		if(2 == fastAccessBins[i]->stopCondition)
		{
			unsigned px, py, pt, pc;
			for(unsigned j = fastAccessBins[i]->left; j <= fastAccessBins[i]->right; ++j)
			{
				output.sz.coords(idxPatches[j], px, py, pt, pc);
				
				output(px, py, pt, 0) = 255.f;
				output(px, py, pt, 1) = 0.f;
				output(px, py, pt, 2) = 0.f;

			}
		}
	}	
}

void VPTree::groupingType2Balls(std::vector<std::vector<unsigned> >& indexes)
{
	for(int i = 0; i < fastAccessBins.size(); ++i)
	{
		if(2 == fastAccessBins[i]->stopCondition)
		{

			indexes.push_back(std::vector<unsigned>(fastAccessBins[i]->right - fastAccessBins[i]->left + 1));
			int pos = indexes.size() - 1;
			for(unsigned j = fastAccessBins[i]->left, k = 0; j <= fastAccessBins[i]->right; ++j, ++k)
				indexes[pos][k] = idxPatches[j];
		}
	}	
}

void VPTree::recExtractBallsType2(std::vector<std::vector<unsigned> >& listPatches, float epsilon, Node* no)
{
	if(no->diameter < epsilon)
	{
		std::vector<unsigned> tempList(no->right - no->left + 1);	
		for(unsigned i = no->left, j = 0; i <= no->right; ++i,++j)
			tempList[j] = idxPatches[i];
		listPatches.push_back(tempList);
	}
	else if(no->child1 != NULL && no->child2 != NULL)
	{
		recExtractBallsType2(listPatches, epsilon, no->child1);
		recExtractBallsType2(listPatches, epsilon, no->child2);
	}
}

void VPTree::extractBallsType2(std::vector<std::vector<unsigned> >& listPatches, float epsilon)
{
	recExtractBallsType2(listPatches, epsilon, this->root);
}
