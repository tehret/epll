/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef PARTITIONTREE_HEADER_H
#define PARTITIONTREE_HEADER_H

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <queue>
#include <unordered_map>
#include "../Utilities.h"
#include "../PatchManager/patchManager.h"
#include "../LibVideoT.hpp"
#include "../comparators.h"

/**
 *
 * Basis for different partition trees
 *
 */

class PartitionTree
{
	public:
		/**
		 * @brief Compute the nearest neighbors and return them in the form used during the first of the denoising algorithm
		 *
		 * @param o_group3d: Used to return the patches corresponding to the nearest neighbors
		 * @param o_index: Used to return the position of the patches corresponding to the nearest neighbors
		 * @param pidx: The query patch
		 * @param reranking: If using a dimensionality reduction, rerank the results using the true distances
		 *
		 * @return the number of nearest neighbors computed, (currently it is always kNN if there wasn't any problem)
		 **/
		virtual int estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool reranking) = 0;

		/**
		 * @brief Compute the nearest neighbors and return them in the form used during the second of the denoising algorithm
		 *
		 * @param i_imNoisy: the noisy dataset
		 * @param o_group3dNoisy: Used to return the noisy patches corresponding to the nearest neighbors
		 * @param o_group3dBasic: Used to return the patches from step 1 corresponding to the nearest neighbors
		 * @param o_index: Used to return the position of the patches corresponding to the nearest neighbors
		 * @param pidx: The query patch
		 * @param reranking: If using a dimensionality reduction, rerank the results using the true distances
		 *
		 * @return the number of nearest neighbors computed, (currently it is always kNN if there wasn't any problem)
		 **/
		virtual int estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool reranking) = 0;

		/**
		 * @brief Compute the nearest neighbors and return their position and the distance to each of them
		 *
		 * @param output: Used to return the distance to the patches corresponding to the nearest neighbors
		 * @param index: Used to return the position of the patches corresponding to the nearest neighbors
		 * @param pidx: The query patch
		 * @param reranking: If using a dimensionality reduction, rerank the results using the true distances
		 *
		 * @return the number of nearest neighbors computed, (currently it is always kNN if there wasn't any problem)
		 **/
		virtual int estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool reranking) = 0;

		/**
		 * @brief Compute the nearest neighbors and return their position and the distance to each of them
		 *
		 * @param localResult: Used to return the position of the patches corresponding to the nearest neighbors alongside their distance to the query patch
		 * @param pidx: The query patch
		 * @param reranking: If using a dimensionality reduction, rerank the results using the true distances
		 *
		 * @return the stop condition of the bin in which pidx is
		 **/
		virtual int retrieveFromTree(std::vector<std::pair<float, unsigned> >& localResult, const unsigned pidx, bool reranking) = 0;

		/**
		 * @brief Return the bin containing the query patch
		 *
		 * @param pidx: The query patch
		 *
		 * @return index of the bin
		 **/
		virtual unsigned whichBin(unsigned pidx) = 0;

		/**
		 * @brief Compute the nearest neighbors using only specific bins and return their position and the distance to each of them
		 *
		 * @param output: Used to return the distance to the patches corresponding to the nearest neighbors
		 * @param index: Used to return the position of the patches corresponding to the nearest neighbors
		 * @param bins: List of the bins where the candidates are picked from
		 * @param pidx: The query patch
		 * @param rerank: If using a dimensionality reduction, rerank the results using the true distances
		 *
		 * @return the number of nearest neighbors computed, (currently it is always kNN if there wasn't any problem)
		 **/
		virtual int estimateClassicFromBins(std::vector<float> &output, std::vector<unsigned> &index, std::vector<unsigned> bins, const unsigned pidx, bool rerank) = 0;

		/**
		 * @brief Retrieve the content of a given bin
		 *
		 * @param beg: Used to return the iterator corresponding at the beginning of the bin
		 * @param end: Used to return the iterator corresponding at the end of the bin
		 * @param bin: The query bin
		 **/
		virtual void retrieveNode(std::vector<unsigned>::iterator& beg, std::vector<unsigned>::iterator& end, unsigned bin) = 0;

		/**
		 * @brief Export the tree to file to be loaded for later purposes
		 *
		 * @param path: The path where to save the tree
		 *
		 * @return 0 if the export was successful
		 **/
		virtual int exportTree(char* path) = 0;

		/**
		 * @brief Return the bin containing the query patch
		 *
		 * @param pm: The new patch manager
		 **/
		virtual void updatePM(PatchManager& pm) = 0;

		/**
		 * @brief Create a carbon copy of the tree
		 *
		 * @return a pointer to the new partition tree
		 **/
		virtual PartitionTree* copy() = 0;

		/**
		 *
		 * Not used in the current framework
		 *
		 **/
		virtual void groupingType2Balls(std::vector<std::vector<unsigned> >& indexes) = 0;
		virtual void extractBallsType2(std::vector<std::vector<unsigned> >& listPatches, float epsilon) = 0;
		virtual int backpropagation(unsigned pidx, std::vector<unsigned>& nearestNeigh, std::vector<float>& distancesTo) = 0;
};



#endif
