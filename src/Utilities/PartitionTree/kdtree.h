/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KDTREE_HEADER_H
#define KDTREE_HEADER_H

#include "partitionTree.h"
#include "../../NlBayes/nlbParams.h"
#include "prmTree.h"
#include <stack>

using namespace VideoNLB;


/**
 *
 * KD-tree
 *
 */

class KDTree : public PartitionTree
{
	public:
		/**
		 * Constructors
		 * @param epsilon: Threshold to which the computation of a tree is stop (set to 0 for best use for now, not every functions done otherwise)
		 * @param p_param: Parameters from the denoising algorithms
		 * @param rand: Number of dimension to use for the randomization 
		 * @{
		 */
		KDTree(float epsilon, PatchManager& _pm, const nlbParams &p_param, int rand = 1);
		KDTree(PatchManager& _pm, PrmTree& paramTree);
		/**
		 * @}
		 */

		/**
		 * 
		 * @brief Same as the previous constructors without computation, the tree is loaded from the data pointed by the path
		 * @{
		 */
		KDTree(float epsilon, PatchManager& _pm, const nlbParams &p_param, int rand, char* path);
		KDTree(PatchManager& _pm, PrmTree& paramTree, char* path);
		/**
		 * @}
		 */


		/**
		 *
		 * Check partitionTree.h for informations on the following functions
		 * @{
		 */

		int estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank);
		int estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank);
		int estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool rerank);
		int retrieveFromTree(std::vector<std::pair<float, unsigned> >& localResult, const unsigned pidx, bool rerank);
		int backpropagation(unsigned pidx, std::vector<unsigned>& nearestNeigh, std::vector<float>& distancesTo);

		unsigned whichBin(unsigned pidx);
		int estimateClassicFromBins(std::vector<float> &output, std::vector<unsigned> &index, std::vector<unsigned> bins, const unsigned pidx, bool rerank);

		void retrieveNode(std::vector<unsigned>::iterator& beg, std::vector<unsigned>::iterator& end, unsigned bin);

		int exportTree(char* path);

		void groupingType2Balls(std::vector<std::vector<unsigned> >& indexes) {};
	  	void extractBallsType2(std::vector<std::vector<unsigned> >& listPatches, float epsilon) {};
		void updatePM(PatchManager& pm_) {pm = &pm_;};
		KDTree* copy() {return new KDTree(*this);};

		/**
		 * @}
		 */

		~KDTree()
		{
			root->~Node();
		}
	private:
		/**
		 * @brief Structures of the node defining a KD-tree
		 *
		 * @param id: Each node of a tree has a unique id
		 * @param left,right: Beginning and end of the bin (if the node is a leaf) in the list of patches
		 * @param dimension: Dimension of the split
		 * @param splitSection: Position of the split in the specific dimension
		 * @param child1: Pointer to the left child of this node in the tree
		 * @param child2: Pointer to the right child of this node in the tree
		 * @param stopCondition: Is 1 if the stop condition is not enough elements left to construct a new Node, 2 if the diameter is small enough (require setting an epsilon)
		 */
		struct Node
		{
			unsigned id;
			int left, right;

			unsigned dimension;
			float splitSection;

			struct Node* child1;
			struct Node* child2;	

			int stopCondition;

			//BoundingBox bbox;
			
			~Node()
			{
				if (child1) child1->~Node();
				if (child2) child2->~Node();
			}
		};
		typedef std::vector<std::pair<float,float> > BoundingBox;

		/// For fast access of the bins of the tree
		std::vector<Node*> fastAccessBins;

		/// Number of elements to be returned for each search
		int kNN;
		/// Max nb of elem in a leaf
		int leaf_max_size;
		/// Max diameter of a leaf
		float leaf_max_diameter;
		/// Reranking the elements might be useful
		bool reranking;

		/// Number of channels to be used during the computation (channel used will be 0 to nbChannelsToUse)
		unsigned nbChannelsToUse;

		/// Root of the KDTree 
		Node* root;

		/// PatchManager
		PatchManager* pm; 

		/// index of the elements on which the tree is based
		std::vector<unsigned> idxPatches;

		/// Parameter for the search
		nlbParams param;

		/// Compute a bounding
		BoundingBox computeBoundingBox();

		/// Compute the dimension of the maximum span
		unsigned dimMaxSpan(BoundingBox &bbox, int rand);

		/// Move the elements according to their position relatively to the pivot
		int partition(unsigned left, unsigned right, unsigned pivot, unsigned dimension, unsigned n);

		/// Fast computation of the median along a specific dimension for a set of element delimited by left and right 
		float quickselect(unsigned left, unsigned right, unsigned dimension);

		/// Split the elements recursively and return a node each time, main function used to construct the tree 
		Node* splitTree(unsigned left, unsigned right, BoundingBox &bbox, int rand, unsigned& binId);

		/// Used to do back tracking if needed, not useful for the patch search studied 
		int investigate(unsigned pidx, std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs>& file, Node* here, Node* avoid);

		/// Load data from the given file to create a tree
		int load(FILE* f);
};
#endif
