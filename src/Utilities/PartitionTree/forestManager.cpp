/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file forestManager.cpp
 * @brief Partition tree forest managing functions
 * @author Thibaud Ehret <ehret.thibaud@gmail.com>
 **/

#include "forestManager.h"

ForestManager::ForestManager(PatchManager& pm_, std::vector<PartitionTree*>& forest_, int kNN_, nlbParams param)
{
	pm = &pm_;
	forest = forest_;
	kNN = kNN_;
	p_params = param;
}

ForestManager::ForestManager(ForestManager& toCopy)
{
	kNN = toCopy.getkNN();
	p_params = toCopy.getp_params();
	pm = toCopy.getpm();

	std::vector<PartitionTree*> temp = toCopy.getforest();
	forest.resize(temp.size());	
	for(int i = 0; i < temp.size(); ++i)
	{
		forest[i] = temp[i]->copy();
	}
	
}

int ForestManager::estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool improveLocal)
{
	std::vector<std::vector<std::pair<float, unsigned> > > localResults(forest.size());
	std::unordered_map<unsigned, int> alreadySeen;
	
	// Search all trees in the forest and extract the best candidates
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic) \
	shared(localResults)
#endif
	for(int tree = 0; tree < forest.size(); tree++)
	{
		forest[tree]->retrieveFromTree(localResults[tree], pidx, rerank);
	}

	// Combine the candidates found at the previous step to find the best candidates overall
	int totalDiffPatches = 0;
	std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs> file;
	for(int i = 0; i < localResults.size(); ++i)
	{
		for(int j = 0; j < localResults[i].size(); ++j)
		{
			int seen = (alreadySeen[localResults[i][j].second]++);
			if(seen == 0)
			{
				++totalDiffPatches;
				file.push(localResults[i][j]);
				// Keep the priority queue small (at size kNN so that we have enough elements) to increase the performances
				if(file.size() > kNN)
					file.pop();
			}
		}
	}
	
	if(improveLocal)
	{

		int sWx   = p_params.sizeSearchWindow;
		int sWy   = p_params.sizeSearchWindow;
		const int sWt_f = p_params.sizeSearchTimeRangeFwd;
		const int sWt_b = p_params.sizeSearchTimeRangeBwd;
		const int sPx   = p_params.sizePatch;
		const int sPt   = p_params.sizePatchTime;

		//! Coordinates of center of search box
		unsigned px, py, pt, pc;
		pm->infoVid()->coords(pidx, px, py, pt, pc);

		unsigned rangex[2];
		unsigned rangey[2];
		unsigned ranget[2];

#ifdef CENTRED_SEARCH
		rangex[0] = std::max(0, (int)px - (sWx-1)/2);
		rangey[0] = std::max(0, (int)py - (sWy-1)/2);
		ranget[0] = std::max(0, (int)pt -  sWt_b   );

		rangex[1] = std::min((int)pm->infoVid()->width  - sPx, (int)px + (sWx-1)/2);
		rangey[1] = std::min((int)pm->infoVid()->height - sPx, (int)py + (sWy-1)/2);
		ranget[1] = std::min((int)pm->infoVid()->frames - sPt, (int)pt +  sWt_f   );
#else
		int shift_x = std::min(0, (int)px - (sWx-1)/2); 
		int shift_y = std::min(0, (int)py - (sWy-1)/2); 
		int shift_t = std::min(0, (int)pt -  sWt_b   ); 

		shift_x += std::max(0, (int)px + (sWx-1)/2 - (int)pm->infoVid()->width  + sPx); 
		shift_y += std::max(0, (int)py + (sWy-1)/2 - (int)pm->infoVid()->height + sPx); 
		shift_t += std::max(0, (int)pt +  sWt_f    - (int)pm->infoVid()->frames + sPt); 

		rangex[0] = std::max(0, (int)px - (sWx-1)/2 - shift_x);
		rangey[0] = std::max(0, (int)py - (sWy-1)/2 - shift_y);
		ranget[0] = std::max(0, (int)pt -  sWt_b    - shift_t);

		rangex[1] = std::min((int)pm->infoVid()->width  - sPx, (int)px + (sWx-1)/2 - shift_x);
		rangey[1] = std::min((int)pm->infoVid()->height - sPx, (int)py + (sWy-1)/2 - shift_y);
		ranget[1] = std::min((int)pm->infoVid()->frames - sPt, (int)pt +  sWt_f    - shift_t);
#endif

		//! Redefine size of search range
		sWx = rangex[1] - rangex[0] + 1;
		sWy = rangey[1] - rangey[0] + 1;
		int sWt = ranget[1] - ranget[0] + 1;

		std::vector<std::pair<float, unsigned> > distance(sWx * sWy * sWt);

		//! Compute distance between patches in search range
		for (unsigned qt = ranget[0], dt = 0; qt <= ranget[1]; qt++, dt++)
			for (unsigned qy = rangey[0], dy = 0; qy <= rangey[1]; qy++, dy++)
				for (unsigned qx = rangex[0], dx = 0; qx <= rangex[1]; qx++, dx++)
				{
					unsigned currentPatch = pm->infoVid()->index(qx, qy, qt, 0);
					int seen = (alreadySeen[currentPatch]++);
					if(seen == 0)
					{
						file.push(std::make_pair(pm->distanceOrigPos(pidx, currentPatch), currentPatch));
						// Keep the priority queue small (at size kNN so that we have enough elements) to increase the performances
						if(file.size() > kNN)
							file.pop();
					}
				}
	}
	for(int i = 0; i < kNN; ++i)
	{
		o_index[kNN - 1 - i] = file.top().second;
		file.pop();
	}
	pm->loadGroupStep1(o_group3d, o_index, kNN, pidx);
	//return kNN;
	return totalDiffPatches;
}

int ForestManager::estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool improveLocal)
{
	std::vector<std::vector<std::pair<float, unsigned> > > localResults(forest.size());
	std::unordered_map<unsigned, int> alreadySeen;

	// Search all trees in the forest and extract the best candidates
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic) \
	shared(localResults)
#endif
	for(int tree = 0; tree < forest.size(); tree++)
	{
		forest[tree]->retrieveFromTree(localResults[tree], pidx, rerank);
	}

	// Combine the candidates found at the previous step to find the best candidates overall
	int totalDiffPatches = 0;
	std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs> file;
	for(int i = 0; i < localResults.size(); ++i)
	{
		for(int j = 0; j < localResults[i].size(); ++j)
		{
			int seen = (alreadySeen[localResults[i][j].second]++);
			if(seen == 0)
			{
				++totalDiffPatches;
				file.push(localResults[i][j]);
				// Keep the priority queue small (at size kNN so that we have enough elements) to increase the performances
				if(file.size() > kNN)
					file.pop();
			}
		}
	}

	if(improveLocal)
	{
		// Improve the results by adding the local patches as well
		int sWx   = p_params.sizeSearchWindow;
		int sWy   = p_params.sizeSearchWindow;
		const int sWt_f = p_params.sizeSearchTimeRangeFwd;
		const int sWt_b = p_params.sizeSearchTimeRangeBwd;
		const int sPx   = p_params.sizePatch;
		const int sPt   = p_params.sizePatchTime;

		//! Coordinates of center of search box
		unsigned px, py, pt, pc;
		pm->infoVid()->coords(pidx, px, py, pt, pc);

		unsigned rangex[2];
		unsigned rangey[2];
		unsigned ranget[2];

#ifdef CENTRED_SEARCH
		rangex[0] = std::max(0, (int)px - (sWx-1)/2);
		rangey[0] = std::max(0, (int)py - (sWy-1)/2);
		ranget[0] = std::max(0, (int)pt -  sWt_b   );

		rangex[1] = std::min((int)i_imNoisy.sz.width  - sPx, (int)px + (sWx-1)/2);
		rangey[1] = std::min((int)i_imNoisy.sz.height - sPx, (int)py + (sWy-1)/2);
		ranget[1] = std::min((int)i_imNoisy.sz.frames - sPt, (int)pt +  sWt_f   );
#else
		int shift_x = std::min(0, (int)px - (sWx-1)/2); 
		int shift_y = std::min(0, (int)py - (sWy-1)/2); 
		int shift_t = std::min(0, (int)pt -  sWt_b   ); 

		shift_x += std::max(0, (int)px + (sWx-1)/2 - (int)i_imNoisy.sz.width  + sPx); 
		shift_y += std::max(0, (int)py + (sWy-1)/2 - (int)i_imNoisy.sz.height + sPx); 
		shift_t += std::max(0, (int)pt +  sWt_f    - (int)i_imNoisy.sz.frames + sPt); 

		rangex[0] = std::max(0, (int)px - (sWx-1)/2 - shift_x);
		rangey[0] = std::max(0, (int)py - (sWy-1)/2 - shift_y);
		ranget[0] = std::max(0, (int)pt -  sWt_b    - shift_t);

		rangex[1] = std::min((int)i_imNoisy.sz.width  - sPx, (int)px + (sWx-1)/2 - shift_x);
		rangey[1] = std::min((int)i_imNoisy.sz.height - sPx, (int)py + (sWy-1)/2 - shift_y);
		ranget[1] = std::min((int)i_imNoisy.sz.frames - sPt, (int)pt +  sWt_f    - shift_t);
#endif

		//! Redefine size of search range
		sWx = rangex[1] - rangex[0] + 1;
		sWy = rangey[1] - rangey[0] + 1;
		int sWt = ranget[1] - ranget[0] + 1;

		std::vector<std::pair<float, unsigned> > distance(sWx * sWy * sWt);

		//! Compute distance between patches in search range
		const int chnls = i_imNoisy.sz.channels;
		for (unsigned qt = ranget[0], dt = 0; qt <= ranget[1]; qt++, dt++)
			for (unsigned qy = rangey[0], dy = 0; qy <= rangey[1]; qy++, dy++)
				for (unsigned qx = rangex[0], dx = 0; qx <= rangex[1]; qx++, dx++)
				{
					unsigned currentPatch = pm->infoVid()->index(qx, qy, qt, 0);
					//! Save distance and corresponding patch index
					int seen = (alreadySeen[currentPatch]++);
					if(seen == 0)
					{
						file.push(std::make_pair(pm->distanceOrigPos(pidx, currentPatch), currentPatch));
						// Keep the priority queue small (at size kNN so that we have enough elements) to increase the performances
						if(file.size() > kNN)
							file.pop();
					}
				}
	}
	
	for(int i = 0; i < kNN; ++i)
	{
		o_index[kNN -1 - i] = file.top().second;
		file.pop();
	}
	pm->loadGroupStep2(o_group3dBasic, o_index, i_imNoisy, o_group3dNoisy, kNN, pidx);
//	return kNN;
	return totalDiffPatches;
}


int ForestManager::estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool rerank, bool improveLocal)
{
	std::vector<std::vector<std::pair<float, unsigned> > > localResults(forest.size());
	std::unordered_map<unsigned, int> alreadySeen;
	
	// Search all trees in the forest and extract the best candidates
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic) \
	shared(localResults)
#endif
	for(int tree = 0; tree < forest.size(); tree++)
	{
		forest[tree]->retrieveFromTree(localResults[tree], pidx, rerank);
	}

	// Combine the candidates found at the previous step to find the best candidates overall
	std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs> file;
	for(int i = 0; i < localResults.size(); ++i)
	{
		for(int j = 0; j < localResults[i].size(); ++j)
		{
			int seen = (alreadySeen[localResults[i][j].second]++);
			if(seen == 0)
			{
				file.push(localResults[i][j]);
				// Keep the priority queue small (at size kNN so that we have enough elements) to increase the performances
				if(file.size() > kNN)
					file.pop();
			}
		}
	}

	if(improveLocal)
	{

		int sWx   = p_params.sizeSearchWindow;
		int sWy   = p_params.sizeSearchWindow;
		const int sWt_f = p_params.sizeSearchTimeRangeFwd;
		const int sWt_b = p_params.sizeSearchTimeRangeBwd;
		const int sPx   = p_params.sizePatch;
		const int sPt   = p_params.sizePatchTime;

		//! Coordinates of center of search box
		unsigned px, py, pt, pc;
		pm->infoVid()->coords(pidx, px, py, pt, pc);

		unsigned rangex[2];
		unsigned rangey[2];
		unsigned ranget[2];

#ifdef CENTRED_SEARCH
		rangex[0] = std::max(0, (int)px - (sWx-1)/2);
		rangey[0] = std::max(0, (int)py - (sWy-1)/2);
		ranget[0] = std::max(0, (int)pt -  sWt_b   );

		rangex[1] = std::min((int)pm->infoVid()->width  - sPx, (int)px + (sWx-1)/2);
		rangey[1] = std::min((int)pm->infoVid()->height - sPx, (int)py + (sWy-1)/2);
		ranget[1] = std::min((int)pm->infoVid()->frames - sPt, (int)pt +  sWt_f   );
#else
		int shift_x = std::min(0, (int)px - (sWx-1)/2); 
		int shift_y = std::min(0, (int)py - (sWy-1)/2); 
		int shift_t = std::min(0, (int)pt -  sWt_b   ); 

		shift_x += std::max(0, (int)px + (sWx-1)/2 - (int)pm->infoVid()->width  + sPx); 
		shift_y += std::max(0, (int)py + (sWy-1)/2 - (int)pm->infoVid()->height + sPx); 
		shift_t += std::max(0, (int)pt +  sWt_f    - (int)pm->infoVid()->frames + sPt); 

		rangex[0] = std::max(0, (int)px - (sWx-1)/2 - shift_x);
		rangey[0] = std::max(0, (int)py - (sWy-1)/2 - shift_y);
		ranget[0] = std::max(0, (int)pt -  sWt_b    - shift_t);

		rangex[1] = std::min((int)pm->infoVid()->width  - sPx, (int)px + (sWx-1)/2 - shift_x);
		rangey[1] = std::min((int)pm->infoVid()->height - sPx, (int)py + (sWy-1)/2 - shift_y);
		ranget[1] = std::min((int)pm->infoVid()->frames - sPt, (int)pt +  sWt_f    - shift_t);
#endif

		//! Redefine size of search range
		sWx = rangex[1] - rangex[0] + 1;
		sWy = rangey[1] - rangey[0] + 1;
		int sWt = ranget[1] - ranget[0] + 1;

		std::vector<std::pair<float, unsigned> > distance(sWx * sWy * sWt);

		//! Compute distance between patches in search range
		for (unsigned qt = ranget[0], dt = 0; qt <= ranget[1]; qt++, dt++)
			for (unsigned qy = rangey[0], dy = 0; qy <= rangey[1]; qy++, dy++)
				for (unsigned qx = rangex[0], dx = 0; qx <= rangex[1]; qx++, dx++)
				{
					unsigned currentPatch = pm->infoVid()->index(qx, qy, qt, 0);
					int seen = (alreadySeen[currentPatch]++);
					if(seen == 0)
					{
						file.push(std::make_pair(pm->distanceOrigPos(pidx, currentPatch), currentPatch));
						// Keep the priority queue small (at size kNN so that we have enough elements) to increase the performances
						if(file.size() > kNN)
							file.pop();
					}
				}
	}
	
	for(int i = 0; i < kNN; ++i)
	{
		output[kNN-1-i] = file.top().first;
		index[kNN-1-i] = file.top().second;
		file.pop();
	}
}

int ForestManager::estimateClassicFromBins(std::vector<float> &output, std::vector<unsigned> &index, std::vector<std::vector<unsigned> > bins, const unsigned pidx, bool rerank, bool verbose)
{
	std::unordered_map<unsigned, int> alreadySeen;
	std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs> que;
	std::vector<std::vector<float> > localResultOutput(forest.size(), std::vector<float>(kNN, 0.f));
	std::vector<std::vector<unsigned> > localResultIdx(forest.size(), std::vector<unsigned>(kNN, 0.f));

	int finalValue = 0;
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic) \
	shared(localResultOutput, localResultIdx)
#endif
	for(int i = 0; i < forest.size(); ++i)
	{
		forest[i]->estimateClassicFromBins(localResultOutput[i], localResultIdx[i], bins[i], pidx, rerank);
	}
	for(int i = 0; i < forest.size(); ++i)
	{
		int newElmts = 0;
		for(int j = 0; j < kNN; ++j)
		{
			int seen = (alreadySeen[localResultIdx[i][j]]++);
			if(seen == 0)
			{
				finalValue++;
				newElmts++;
				que.push(std::make_pair(localResultOutput[i][j], localResultIdx[i][j]));
				if(que.size() > kNN)
					que.pop();
			}
		}
		if(verbose)
			printf("Adding %d new possibilities\n", newElmts);
	}

	if(verbose)
		printf("-------------------------\n");

	for(int i = 0; i < kNN; ++i)
	{
		output[kNN-1-i] = que.top().first;
		index[kNN-1-i] = que.top().second;
		que.pop();
	}
	
	return finalValue;
}

void ForestManager::getBins(std::vector<unsigned>& bins, unsigned pidx)
{
	for(int i = 0; i < forest.size(); i++)
	{
		bins[i] = forest[i]->whichBin(pidx);
	}
}

void ForestManager::count(std::vector<unsigned>& listElements, std::unordered_map<unsigned, int>& mapping, const unsigned pidx, bool verbose)
{
	std::vector<unsigned>::iterator beg;
	std::vector<unsigned>::iterator end;

	int newElmts = 0;
	for(int i = 0; i < forest.size(); ++i)
	{
		unsigned node = forest[i]->whichBin(pidx);
		forest[i]->retrieveNode(beg, end, node);
		while(beg != end)
		{
			int seen = mapping[*beg]++;
			if(seen == 0)
			{
				listElements.push_back(*beg);
				newElmts++;
			}
			beg++;
		}
	}
	if(verbose)
		printf("Adding %d new possibilities\n", newElmts);
}

void ForestManager::countNeighbors(std::vector<unsigned>& listElements, std::unordered_map<unsigned, int>& mapping, int type, const unsigned pidx, bool verbose)
{
	std::vector<unsigned>::iterator beg;
	std::vector<unsigned>::iterator end;

	int newElmts = 0;
	for(int i = 0; i < forest.size(); ++i)
	{
		unsigned node = forest[i]->whichBin(pidx);
		forest[i]->retrieveNode(beg, end, node);
		while(beg != end)
		{
			unsigned newId = pm->getNeighboringPatch(*beg, type, p_params.sizePatch / 2, p_params.sizePatchTime / 2);
			int seen = mapping[newId]++;
			if(seen == 0)
			{
				listElements.push_back(newId);
				newElmts++;
			}
			beg++;
		}
	}
	if(verbose)
		printf("Adding %d new possibilities\n", newElmts);
}

int ForestManager::retrieveFromForest(std::vector<float>& dists, std::vector<unsigned>& indexes, unsigned pidx, bool rerank, bool verbose)
{
	std::vector<std::vector<std::pair<float, unsigned> > > localResults(forest.size());
	std::unordered_map<unsigned, int> alreadySeen;

	int typeResult[forest.size()];
	
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic) \
	shared(localResults)
#endif
	for(int tree = 0; tree < forest.size(); tree++)
	{
		typeResult[tree] = forest[tree]->retrieveFromTree(localResults[tree], pidx, rerank);
	}
	int totalDiffPatches = 0;


	for(int i = 0; i < localResults.size(); ++i)
	{
		if(typeResult[i] == 2)
		{
			// These ones are very good candidates, lets keep these ones
			for(int j = 0; j < kNN; ++j)
			{
				indexes[kNN - 1 - j] = localResults[i][j].second;
				dists[kNN - 1 - j] = localResults[i][j].first;
			}
			return kNN;
		}
	}

	std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs> file;
	for(int i = 0; i < localResults.size(); ++i)
	{
		for(int j = 0; j < localResults[i].size(); ++j)
		{
			int seen = (alreadySeen[localResults[i][j].second]++);
			if(seen == 0)
			{
				++totalDiffPatches;
				file.push(localResults[i][j]);
				// Keep the priority queue small (at size kNN so that we have enough elements) to increase the performances
				if(file.size() > kNN)
					file.pop();
			}
		}
	}
	for(int i = 0; i < kNN; ++i)
	{
		indexes[kNN - 1 - i] = file.top().second;
		dists[kNN - 1 - i] = file.top().first;
		file.pop();
	}
	pm->getRealPatches(indexes);
	return kNN;
}

void ForestManager::groupingType2Balls(std::vector<std::vector<unsigned> >& indexes)
{
	for(int i = 0; i < forest.size(); ++i)
	{
		forest[i]->groupingType2Balls(indexes);
	}
}

void ForestManager::extractBallsType2(std::vector<std::vector<unsigned> >& listPatches, float epsilon)
{
	for(int i = 0; i < forest.size(); ++i)
	{
		forest[i]->extractBallsType2(listPatches, epsilon);
	}
}

void ForestManager::updatePM(PatchManager& pm_)
{
	pm = &pm_;
	for(int i = 0; i < forest.size(); ++i)
		forest[i]->updatePM(pm_);
}
