/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file kdtree.cpp
 * @brief Specific type of partition tree: KD-tree
 *
 * @author Thibaud Ehret <ehret.thibaud@gmail.com>
 **/

#include "kdtree.h"

// TODO Searches can be speed up by using a reverse index directly to the bin

KDTree::KDTree(float epsilon, PatchManager& _pm, const nlbParams &p_param, int rand)
{
	int k = p_param.nSimilarPatches; 
	std::srand( unsigned(std::time(0))); 

	kNN = k;
	// We assure that we have at least k elements in the bins with a median split
	leaf_max_size = 2*k+1;

	leaf_max_diameter = epsilon;
	pm = &_pm;

	int nbPatches = pm->getNbPatches();

	param = p_param;

	idxPatches = std::vector<unsigned>(nbPatches);
	pm->getAllPatches(idxPatches);

	unsigned binId = 0;
	BoundingBox bbox = computeBoundingBox(); 
	root = splitTree(0, idxPatches.size()-1, bbox, rand, binId); 
}

KDTree::KDTree(PatchManager& _pm, PrmTree& paramTree)
{
	float epsilon = paramTree.epsilon;
	nlbParams p_param = paramTree.prms;
	int rand = paramTree.rand;

	int k = p_param.nSimilarPatches; 
	std::srand( unsigned(std::time(0))); 

	kNN = k;
	// We assure that we have at least k elements in the bins with a median split
	leaf_max_size = 2*k+1;

	leaf_max_diameter = epsilon;
	pm = &_pm;

	int nbPatches = pm->getNbPatches();

	param = p_param;

	idxPatches = std::vector<unsigned>(nbPatches);
	pm->getAllPatches(idxPatches);

	unsigned binId = 0;
	BoundingBox bbox = computeBoundingBox(); 
	root = splitTree(0, idxPatches.size()-1, bbox, rand, binId); 
}

static int rand_gen(int i)
{
	return std::rand() % i;
}

int KDTree::estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank)
{
	Node* currentNode = this->root; 
	// Find the leaf in which the patch resides
	while(currentNode->child1 != NULL && currentNode->child2 != NULL)
	{
		if(pm->getValueAtDim(pidx, currentNode->dimension) > currentNode->splitSection)
			currentNode = currentNode->child2;
		else
			currentNode = currentNode->child1;
	}

	const int sPx = param.sizePatch;
	const int sPt = param.sizePatchTime;

	if(1 == currentNode->stopCondition)
	{
		// < 2*k element, therefore we keep the best ones

		// Do a bruteforce search inside this bin
		std::vector<std::pair<float, unsigned> > distance(currentNode->right - currentNode->left + 1);
		for(int i = currentNode->left; i <= currentNode->right; ++i)
		{
			if(rerank && pm->getType() != 0)
				distance[i - currentNode->left] = std::make_pair(pm->realDistance(pidx, idxPatches[i]), i);
			else
				distance[i - currentNode->left] = std::make_pair(pm->distance(pidx, idxPatches[i]), i);
		}
		std::partial_sort(distance.begin(), distance.begin() + kNN, distance.end(), comparaisonFirst);
		for (int i = 0; i < kNN; i++)
		{
			o_index[i] = idxPatches[distance[i].second];
		}
	}	
	else
	{
		// When the diameter of the bin is small enough we just draw the kNN patches that will be used 
		// FIXME
		std::vector<int> permu(currentNode->right - currentNode->left + 1);
		for(int i = currentNode->left, j = 0; i <= currentNode->right; ++i,++j)
			permu[j] = i;

		std::random_shuffle(permu.begin(), permu.end(), rand_gen);
		for(int i = 0; i < kNN; ++i)
		{
			o_index[i] = idxPatches[permu[i]];
		}
	}

	pm->loadGroupStep1(o_group3d, o_index, kNN, pidx);

	return kNN;
}

int KDTree::estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank)
{
	Node* currentNode = this->root; 
	// Find the leaf in which the patch resides
	while(currentNode->child1 != NULL && currentNode->child2 != NULL)
	{
		if(pm->getValueAtDim(pidx, currentNode->dimension) > currentNode->splitSection)
			currentNode = currentNode->child2;
		else
			currentNode = currentNode->child1;
	}

	const int sPx = param.sizePatch;
	const int sPt = param.sizePatchTime;

	if(1 == currentNode->stopCondition)
	{
		// < 2*k element, therefore we keep the best ones

		// Do a bruteforce search inside this bin
		std::vector<std::pair<float, unsigned> > distance(currentNode->right - currentNode->left + 1);
		for(int i = currentNode->left; i <= currentNode->right; ++i)
		{
			if(rerank && pm->getType() != 0)
				distance[i - currentNode->left] = std::make_pair(pm->realDistance(pidx, idxPatches[i]), i);
			else
				distance[i - currentNode->left] = std::make_pair(pm->distance(pidx, idxPatches[i]), i);
		}
		std::partial_sort(distance.begin(), distance.begin() + kNN, distance.end(), comparaisonFirst);
		for (int i = 0; i < kNN; i++)
		{
			o_index[i] = idxPatches[distance[i].second];
		}
	}	
	else
	{
		// When the diameter of the bin is small enough we just draw the kNN patches that will be used 
		std::vector<int> permu(currentNode->right - currentNode->left + 1);
		for(int i = currentNode->left, j = 0; i <= currentNode->right; ++i,++j)
			permu[j] = i;
		std::random_shuffle(permu.begin(), permu.end(), rand_gen);
		for(int i = 0; i < kNN; ++i)
		{
			o_index[i] = idxPatches[permu[i]];
		}
	}

	pm->loadGroupStep2(o_group3dBasic, o_index, i_imNoisy, o_group3dNoisy, kNN, pidx);
	return kNN;
}

KDTree::BoundingBox KDTree::computeBoundingBox()
{
	int nbDim = pm->getDim();
	BoundingBox bbox(nbDim, std::pair<float,float>(0.,0.));	
	// Initialisation of the bounding box around a single point
	for(unsigned d = 0; d < nbDim; ++d)
	{
		bbox[d].first = pm->getValueAtDimLocal(idxPatches[0], d);
		bbox[d].second = pm->getValueAtDimLocal(idxPatches[0], d);
	}

	// Compute the bounding for the initial patches. Increase the size of the bounding box until it contains each patch
	for(int i = 1; i < idxPatches.size(); ++i)
	{
		for(unsigned d = 0; d < nbDim; ++d)
		{
			float value = pm->getValueAtDimLocal(idxPatches[i], d);
			if(bbox[d].first > value)
				bbox[d].first = value;
			if(bbox[d].second < value)
				bbox[d].second = value;
		}
	}

	return bbox;
}

unsigned KDTree::dimMaxSpan(BoundingBox &bbox, int rand)
{
	if(rand == 1)
	{
		// If we don't create a randomized tree, find the dimension in which the bounding box is the largest
		unsigned dim = 0;
		float maxSpan = 0.;
		for(unsigned d = 0; d < bbox.size(); ++d)
		{
			if((bbox[d].second - bbox[d].first) > maxSpan)
			{
				maxSpan = (bbox[d].second - bbox[d].first);
				dim = d;
			}
		}
		return dim;
	}	
	else
	{
		// If we create a randomized tree, find the $rand$ dimensions in which the bounding box is the largest, return one of them at random
		std::vector<std::pair<float, int> > span(bbox.size());
		for(unsigned d = 0; d < bbox.size(); ++d)
		{
			span[d] = std::make_pair(bbox[d].first - bbox[d].second, d);
		}
		std::partial_sort(span.begin(), span.begin() + rand, span.end(), comparaisonFirst);
		int random_dim = std::rand() % rand;
		return span[random_dim].second;
	}
}

int KDTree::partition(unsigned left, unsigned right, unsigned pivot, unsigned dim, unsigned n)
{
	float valuePivot = pm->getValueAtDimLocal(idxPatches[pivot], dim);

	unsigned temp = idxPatches[pivot];
	idxPatches[pivot] = idxPatches[right];
	idxPatches[right] = temp;

	unsigned store = left;
	int allEqual = 0;

	// move all elements between $left$ and $right$ so they are at the correct position according to the pivot
	for(unsigned i = left; i <= right; ++i)
	{
		float candidate = pm->getValueAtDimLocal(idxPatches[i], dim);
		// trick in case all elements are equal to speed up the computation
		if(std::abs(candidate - valuePivot) < 0.000001)
			allEqual++;

		if(candidate < valuePivot)  
		{
			temp = idxPatches[store];
			idxPatches[store] = idxPatches[i];
			idxPatches[i] = temp;
			++store;
		}
	}

	temp = idxPatches[store];
	idxPatches[store] = idxPatches[right];
	idxPatches[right] = temp;

	if(allEqual >= (right - left) / 2)
	{
		return n;
	}

	// return the index where the pivot is
	return store;
}

// Fast computation of the median along a specific dimension for a set of element delimited by left and right 
float KDTree::quickselect(unsigned l, unsigned r, unsigned dim)
{
	unsigned left = l;
	unsigned right = r;

	// Position of the median element
	unsigned n = left + (right - left) / 2;

	if(left == right)
	{
		return pm->getValueAtDimLocal(idxPatches[left], dim);
	}	
	unsigned pivot;
	while(1==1)
	{
		pivot = left + (right - left) / 2;
		pivot = partition(left, right, pivot, dim, n);

		if(n  == pivot)
		{
			return pm->getValueAtDimLocal(idxPatches[n], dim);
		}
		else if(n < pivot)
		{
			right = pivot - 1;
		}
		else
		{
			left = pivot + 1;
		}
	}
}

KDTree::Node* KDTree::splitTree(unsigned left, unsigned right, BoundingBox &bbox, int rand, unsigned& binId)
{
	Node* tempNode = new Node();

	unsigned dim = dimMaxSpan(bbox, rand);
	float leafDiameter = bbox[dim].second - bbox[dim].first;
	
	// Test if we are creating a leaf or not
	if( (right - left + 1) <= leaf_max_size || leafDiameter < leaf_max_diameter)
	{
		// It is a leaf, we set all informations regarding this leaf and finish
		tempNode->child1 = NULL;
		tempNode->child2 = NULL;

		tempNode->dimension =  0u;
		tempNode-> splitSection = 0.;

		if(leafDiameter <= leaf_max_diameter)
		{
			tempNode->stopCondition = 2;
		}
		else
		{
			tempNode->stopCondition = 1;
		}

		tempNode->left = left;
		tempNode->right = right;

		fastAccessBins.push_back(tempNode);
		tempNode->id = binId;
		++binId;
	}
	else
	{
		// It is not a leaf, create the split and the two children nodes
		tempNode->left = left;
		tempNode->right = right;

		tempNode->dimension = dim;

		tempNode->splitSection = quickselect(left, right, tempNode->dimension);

		// Compute the bounding boxes for the two space restriction
		BoundingBox leftSplit(bbox);
		leftSplit[tempNode->dimension].second = tempNode->splitSection;

		BoundingBox rightSplit(bbox);
		rightSplit[tempNode->dimension].first = tempNode->splitSection;


		// Create the nodes for the children 
		tempNode->child1 = splitTree(left, left + (right - left) / 2, leftSplit, rand, binId);
		tempNode->child2 = splitTree(left + (right - left) / 2 + 1, right, rightSplit, rand, binId);
	}

	// Return a pointer to the constructed node
	return tempNode;
}

int KDTree::estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool rerank)
{
	Node* currentNode = this->root; 
	// Find the leaf in which the patch resides
	while(currentNode->child1 != NULL && currentNode->child2 != NULL)
	{
		if(pm->getValueAtDim(pidx, currentNode->dimension) > currentNode->splitSection)
			currentNode = currentNode->child2;
		else
			currentNode = currentNode->child1;
	}

	const int sPx = param.sizePatch;
	const int sPt = param.sizePatchTime;

	if(1 == currentNode->stopCondition)
	{
		// < 2*k element, therefore we keep the best ones

		// Do a bruteforce search inside this bin
		std::vector<std::pair<float, unsigned> > distance(currentNode->right - currentNode->left + 1);
		for(int i = currentNode->left; i <= currentNode->right; ++i)
		{
			if(rerank && pm->getType() != 0)
				distance[i - currentNode->left] = std::make_pair(pm->realDistance(pidx, idxPatches[i]), i);
			else
				distance[i - currentNode->left] = std::make_pair(pm->distance(pidx, idxPatches[i]), i);
		}
		std::partial_sort(distance.begin(), distance.begin() + kNN, distance.end(), comparaisonFirst);
		for (int i = 0; i < kNN; i++)
		{
			if(rerank || pm->getType() == 0)
				output[i] = distance[i].first;
			else
				output[i] = pm->realDistance(pidx, idxPatches[distance[i].second]);

			index[i] = idxPatches[distance[i].second];
		}
	}	
	else
	{
		// TODO
	}

	pm->getRealPatches(index);

	return kNN;
}

int KDTree::retrieveFromTree(std::vector<std::pair<float, unsigned> >& localResult, const unsigned pidx, bool rerank)
{
	Node* currentNode = this->root; 
	// Find the leaf in which the patch resides
	while(currentNode->child1 != NULL && currentNode->child2 != NULL)
	{
		if(pm->getValueAtDim(pidx, currentNode->dimension) > currentNode->splitSection)
			currentNode = currentNode->child2;
		else
			currentNode = currentNode->child1;
	}

	const int sPx = param.sizePatch;
	const int sPt = param.sizePatchTime;

	if(1 == currentNode->stopCondition)
	{
		for(unsigned i = currentNode->left, j = 0; i <= currentNode->right; ++i,++j)
		{
			float dist;
			if(rerank && pm->getType() != 0)
				dist = pm->realDistance(pidx,idxPatches[i]);
			else
				dist = pm->distance(pidx,idxPatches[i]);
			localResult.push_back(std::make_pair(dist, idxPatches[i]));
		}
		return 1;
	}	
	else
	{
		//TODO if it used at some points do something here !!!!!!!!!
		//TODO 
		//TODO 
		//TODO 
		//TODO 
		//TODO 
		//TODO 
		//TODO 
		//TODO 
		//TODO 
		//TODO 
		return 2;
	}
}

int KDTree::estimateClassicFromBins(std::vector<float> &output, std::vector<unsigned> &index, std::vector<unsigned> bins, const unsigned pidx, bool rerank)
{
	std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs> que;
	for(int i = 0; i < bins.size(); ++i)
	{
		Node* currentNode = fastAccessBins[bins[i]];
		if(1 == currentNode->stopCondition)
		{
			for(int i = currentNode->left; i <= currentNode->right; ++i)
			{
				float dist;
				if(rerank && pm->getType() != 0)
					que.push(std::make_pair(pm->realDistance(pidx, idxPatches[i]), i));
				else
					que.push(std::make_pair(pm->distance(pidx, idxPatches[i]), i));
				if(que.size() > kNN)
					que.pop();
			}
		}	
		else
		{
			// TODO
		}
	}
	for(int i = 0; i < kNN; ++i)
	{
		output[kNN-1-i] = que.top().first;
		index[kNN-1-i] = idxPatches[que.top().second];
		que.pop();
	}

	pm->getRealPatches(index);

	return kNN;	
}

void KDTree::retrieveNode(std::vector<unsigned>::iterator& beg, std::vector<unsigned>::iterator& end, unsigned bin)
{
	Node* current = fastAccessBins[bin];
	beg = idxPatches.begin() + current->left;
	end = idxPatches.end() + current->right + 1;
}

unsigned KDTree::whichBin(unsigned pidx)
{
	Node* currentNode = this->root; 
	// Find the leaf in which the patch resides
	while(currentNode->child1 != NULL && currentNode->child2 != NULL)
	{
		if(pm->getValueAtDim(pidx, currentNode->dimension) > currentNode->splitSection)
			currentNode = currentNode->child2;
		else
			currentNode = currentNode->child1;
	}
	return currentNode->id;
}

int KDTree::backpropagation(unsigned pidx, std::vector<unsigned>& nearestNeigh, std::vector<float>& distancesTo)
{
	int nbDistanceComputation = 0;

	std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs> file;

	Node* currentNode = this->root; 
	// Find the leaf in which the patch resides, as well as the path to get there
	std::vector<Node*> backprop;
	while(currentNode->child1 != NULL && currentNode->child2 != NULL)
	{
		backprop.push_back(currentNode);
		if(pm->getValueAtDim(pidx, currentNode->dimension) > currentNode->splitSection)
			currentNode = currentNode->child2;
		else
			currentNode = currentNode->child1;
	}
	backprop.push_back(currentNode);
	// Consider all the elements in the current bin ("the nearest neighbors are most likely these ones")
	for(unsigned i = currentNode->left; i <= currentNode->right;++i)
	{
		file.push(std::make_pair(pm->distance(pidx, idxPatches[i]), idxPatches[i]));
		if(file.size() > kNN)
			file.pop();
	}
	nbDistanceComputation += (currentNode->right - currentNode->left + 1);

	// Backtrack in the tree using the path computed previously to consider other interesting branches
	for(int it = backprop.size() - 2; it >= 0; --it)
	{	
		nbDistanceComputation += investigate(pidx, file, backprop[it], backprop[it + 1]);
	}

	
	for(int it = kNN-1; it >= 0; --it)
	{
		std::pair<float, unsigned> temp = file.top();
		nearestNeigh[it] = temp.second;
		distancesTo[it] = temp.first;
		file.pop();
	}
	return nbDistanceComputation; 
}

int KDTree::investigate(unsigned pidx, std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs>& file, Node* here, Node* avoid)
{
	int nbDistanceComputation = 0;
	Node* nextNode;
	if(here->child1 != avoid)
	{
		nextNode = here->child1;
		// If the node we are considering is a leaf 
		if(nextNode->child1 == NULL)
		{
			// Consider all the elements in the current bin
			for(unsigned it = nextNode->left; it <= nextNode->right; ++it)
			{
				file.push(std::make_pair(pm->distance(pidx, idxPatches[it]), idxPatches[it]));
				if(file.size() > kNN)
					file.pop();
			}
			nbDistanceComputation += (nextNode->right - nextNode->left + 1);
		}
		// If the ball is close enough to be relevant
		else if(pm->getValueAtDim(pidx, nextNode->dimension) - file.top().first < nextNode->splitSection || file.size() < kNN)
		{
			nbDistanceComputation += investigate(pidx,file,nextNode,NULL) + 1;
		}
	}	
	if(here->child2 != avoid)
	{
		nextNode = here->child2;
		// If the node we are considering is a leaf 
		if(nextNode->child1 == NULL)
		{
			// Consider all the elements in the current bin
			for(unsigned it = nextNode->left; it <= nextNode->right; ++it)
			{
				file.push(std::make_pair(pm->distance(pidx, idxPatches[it]), idxPatches[it]));
				if(file.size() > kNN)
					file.pop();
			}
			nbDistanceComputation += (nextNode->right - nextNode->left + 1);
		}
		// If the ball is close enough to be relevant
		else if(pm->getValueAtDim(pidx, nextNode->dimension) + file.top().first > nextNode->splitSection || file.size() < kNN)
		{
			nbDistanceComputation += investigate(pidx,file,nextNode,NULL) + 1;
		}
	}	
	return nbDistanceComputation;
}

int KDTree::exportTree(char* path)
{
	FILE *f;
	f = fopen(path, "w");

	// First line correspond to the type of the tree
	fprintf(f, "2\n");

	// FIXME Export all the parameters ?

	// export the index
	for(int i = 0; i < idxPatches.size(); ++i)
		fprintf(f, "%u ", idxPatches[i]);
	fprintf(f, "\n");
	
	// export the tree in the following lines: one line for each node from up to bottom, left to right
	std::stack<Node*> lifo;
	lifo.push(root);	

	while(!lifo.empty())
	{
		Node* current = lifo.top();
		lifo.pop();

		int info = 0;
		if(current->child1 == NULL && current->child2 == NULL)
			info = 1;
		fprintf(f, "%u %u %u %u %f %d %d\n", current->id, current->left, current->right, current->dimension, current->splitSection, current->stopCondition, info);
		lifo.push(current->child2);
		lifo.push(current->child1);
	}

	fclose(f);
	return 0;
}



KDTree::KDTree(float epsilon, PatchManager& _pm, const nlbParams &p_param, int rand, char* path)
{
	int k = p_param.nSimilarPatches; 
	// Initialize the random number generator
	std::srand( unsigned(std::time(0))); 

	kNN = k;
	// We assure that we have at least k elements in the bins with a median split
	leaf_max_size = 2*k+1;

	leaf_max_diameter = epsilon;
	pm = &_pm;

	param = p_param;

	FILE *f;
	f = fopen(path, "r");

	// Check if the type of the tree at 'path' is a VP-tree
	int tp = -1;
	if(f != NULL)
		fscanf(f, "%d\n", &tp);

	if(tp != 2)
	{
		printf("Wrong type of tree, the tree is rebuilt !\n");

		int nbPatches = pm->getNbPatches();
		idxPatches = std::vector<unsigned>(nbPatches);

		pm->getAllPatches(idxPatches);

		unsigned binId = 0;
		BoundingBox bbox = computeBoundingBox(); 
		root = splitTree(0, idxPatches.size()-1, bbox, rand, binId); 
	}
	else
	{
		load(f);
	}

	if(f != NULL)
		fclose(f);

}

KDTree::KDTree(PatchManager& _pm, PrmTree& paramTree, char* path)
{
	float epsilon = paramTree.epsilon;
	nlbParams p_param = paramTree.prms;
	int rand = paramTree.rand;

	int k = p_param.nSimilarPatches; 
	// Initialize the random number generator
	std::srand( unsigned(std::time(0))); 

	kNN = k;
	// We assure that we have at least k elements in the bins with a median split
	leaf_max_size = 2*k+1;

	leaf_max_diameter = epsilon;
	pm = &_pm;

	param = p_param;

	FILE *f;
	f = fopen(path, "r");

	// Check if the type of the tree at 'path' is a VP-tree
	int tp = -1;
	if(f != NULL)
		fscanf(f, "%d\n", &tp);

	if(tp != 2)
	{
		printf("Wrong type of tree, the tree is rebuilt !\n");

		int nbPatches = pm->getNbPatches();
		idxPatches = std::vector<unsigned>(nbPatches);

		pm->getAllPatches(idxPatches);

		unsigned binId = 0;
		BoundingBox bbox = computeBoundingBox(); 
		root = splitTree(0, idxPatches.size()-1, bbox, rand, binId); 
	}
	else
	{
		load(f);
	}

	if(f != NULL)
		fclose(f);
}

int KDTree::load(FILE* f)
{
	int nbPatches;
	fscanf(f, "%d\n", &nbPatches);

	idxPatches = std::vector<unsigned>(nbPatches);

	for(int i = 0; i < nbPatches; ++i)
	{
		fscanf(f, "%u ", &idxPatches[i]);	
	}

	std::stack<Node*> lifo;

	unsigned id;
	unsigned left, right;

	unsigned dimension;
	float splitSection;
	int stopCondition;
	int info;
	fscanf(f, "%u %u %u %u %f %d %d\n", &id, &left, &right, &dimension, &splitSection, &stopCondition, &info);
	root = new Node();
	root->id = id;
	root->left = left;
	root->right = right;
	root->dimension = dimension;
	root->splitSection = splitSection;
	root->stopCondition = stopCondition;
	root->child1 = NULL;
	root->child2 = NULL;

	lifo.push(root);


	while(fscanf(f, "%u %u %u %u %f %d %d\n", &id, &left, &right, &dimension, &splitSection, &stopCondition, &info) > 0)
	{
		Node* previous = lifo.top();

		Node* current = new Node();
		current->id = id;
		current->left = left;
		current->right = right;
		current->dimension = dimension;
		current->splitSection = splitSection;
		current->stopCondition = stopCondition;
		current->child1 = NULL;
		current->child2 = NULL;

		if(previous->child1 != NULL)
		{
			// Node is full, remove it from the stack
			lifo.pop();
			previous->child2 = current;
		}
		else
		{
			previous->child1 = current;
		}

		// if the node is a leaf, construct the fast access array and not load it in the stack
		if(info > 0)
		{
			fastAccessBins.push_back(current);	
		}
		else
		{
			lifo.push(current);
		}
	}

	return 0;
}

