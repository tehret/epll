//
// TOP
//

#ifndef VST_HEADER_H
#define VST_HEADER_H

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "LibVideoT.hpp"

#define SCALE 1


class VST {
	public:
		static float dirVst(Video<float>& input, float a, float b)
		{
			float mini = 0;
			for(unsigned x = 0; x < input.sz.width; ++x)
			for(unsigned y = 0; y < input.sz.height; ++y)
			for(unsigned t = 0; t < input.sz.frames; ++t)
			for(unsigned c = 0; c < input.sz.channels; ++c)
				if(mini > input(x,y,t,c))
					mini = input(x,y,t,c);

			for(unsigned x = 0; x < input.sz.width; ++x)
			for(unsigned y = 0; y < input.sz.height; ++y)
			for(unsigned t = 0; t < input.sz.frames; ++t)
			for(unsigned c = 0; c < input.sz.channels; ++c)
				//input(x,y,t,c) = SCALE * 2 * (std::sqrt(a * (input(x,y,t,c) + b) - mini) - std::sqrt(b)) / a;
				input(x,y,t,c) = SCALE * 2 * (std::sqrt(a * (input(x,y,t,c) + b) - mini)) / a;

			return mini;
		};

		static void invVst(Video<float>& input, float a, float b, float mini)
		{
			for(unsigned x = 0; x < input.sz.width; ++x)
			for(unsigned y = 0; y < input.sz.height; ++y)
			for(unsigned t = 0; t < input.sz.frames; ++t)
			for(unsigned c = 0; c < input.sz.channels; ++c)
				input(x,y,t,c) = ((a * input(x,y,t,c) / (2 * SCALE) + std::sqrt(b)) * (a * input(x,y,t,c) / (2 * SCALE) + std::sqrt(b)) - b) / a + mini;
		};
};
#endif
