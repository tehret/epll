// Created by Nicola Pierazzo on 20/10/15.
//
//

#include <multiscaler.hpp>

namespace Multiscaler {

	void dct_vid(Video<float>& vid, std::vector<float>& result, int initFrame, int finalFrame) {
		int n[] = {vid.sz.width, vid.sz.height, finalFrame - initFrame + 1};
		//int n[] = {vid.sz.width, vid.sz.height, vid.sz.frames};
		fftwf_r2r_kind dct3[] = {FFTW_REDFT10, FFTW_REDFT10, FFTW_REDFT10};
		std::vector<float> data = vid.dct_shuffle_video(initFrame, finalFrame);
		fftwf_plan plan = fftwf_plan_many_r2r(3,
				n,
				vid.sz.channels,
				data.data(),
				NULL,
				vid.sz.channels,
				1,
				result.data(),
				NULL,
				vid.sz.channels,
				1,
				dct3,
				FFTW_ESTIMATE);
		fftwf_execute(plan);
		fftwf_destroy_plan(plan);

		// Normalization
		for (int i = 0; i < vid.sz.height * vid.sz.width * (finalFrame - initFrame + 1) * vid.sz.channels; ++i) {
			result[i] /= 8 * vid.sz.height * vid.sz.width * (finalFrame - initFrame + 1);
		}
		//for (int i = 0; i < vid.sz.height * vid.sz.width * vid.sz.frames * vid.sz.channels; ++i) {
		//		 result[i] /= 8 * vid.sz.height * vid.sz.width * vid.sz.frames;
		//}
	}

	void idct_vid(Video<float>& vid, std::vector<float>& input, int initFrame, int lastFrame) {
		std::vector<float> temp(input.size());
		int n[] = {vid.sz.width, vid.sz.height, lastFrame - initFrame + 1};
		//int n[] = {vid.sz.width, vid.sz.height, vid.sz.frames};
		fftwf_r2r_kind idct3[] = {FFTW_REDFT01, FFTW_REDFT01, FFTW_REDFT01};
		fftwf_plan plan = fftwf_plan_many_r2r(3,
				n,
				vid.sz.channels,
				input.data(),
				NULL,
				vid.sz.channels,
				1,
				temp.data(),
				NULL,
				vid.sz.channels,
				1,
				idct3,
				FFTW_ESTIMATE);
		fftwf_execute(plan);
		fftwf_destroy_plan(plan);

		unsigned offsetChannels = vid.sz.channels * vid.sz.height * (lastFrame - initFrame + 1);
		//unsigned offsetChannels = vid.sz.channels * vid.sz.height * vid.sz.frames;
		for(unsigned w = 0; w < vid.sz.width; ++w)
			for(unsigned h = 0; h < vid.sz.height; ++h)
				for(unsigned f = initFrame; f < lastFrame + 1; ++f)
					//for(unsigned f = 0; f < vid.sz.frames; ++f)
					for(unsigned c = 0; c < vid.sz.channels; ++c)
					{
						//vid(w,h,f,c) = temp[c + f * vid.sz.channels + h * vid.sz.channels * vid.sz.frames + w * offsetChannels];
						vid(w,h,f,c) = temp[c + (f - initFrame) * vid.sz.channels + h * vid.sz.channels * (lastFrame - initFrame + 1) + w * offsetChannels];
					}					

	}

	void fine_to_coarse_vid(std::vector<float>& fine, std::vector<float>& coarse, float ratio, const VideoSize& sz, int initFrame, int lastFrameFine, int lastFrameCoarse)
	{
		unsigned offsetChannels = sz.channels * sz.height * (lastFrameFine - initFrame + 1);
		for(unsigned w = 0, i = 0; w < std::max((int)(sz.width * ratio), 1); ++w)
			for(unsigned h = 0; h < std::max((int)(sz.height * ratio), 1); ++h)
				for(unsigned f = initFrame; f < lastFrameCoarse + 1; ++f)
					for(unsigned c = 0; c < sz.channels; ++c, ++i)
					{
						coarse[i] = fine[c + (f - initFrame) * sz.channels + h * sz.channels * (lastFrameFine - initFrame + 1) + offsetChannels * w];
					}
	}

	void coarse_to_fine_vid(std::vector<float>& fine, std::vector<float>& coarse, const VideoSize& fine_sz, const VideoSize& coarse_sz, int initFrame, int lastFrameFine, int lastFrameCoarse, float factor)
	{
		unsigned fine_offsetChannels = fine_sz.channels * fine_sz.height * (lastFrameFine - initFrame + 1);
		unsigned coarse_offsetChannels = coarse_sz.channels * coarse_sz.height * (lastFrameCoarse - initFrame + 1);

		for(unsigned w = 0, j = 0; w < std::max(coarse_sz.width * factor, 1.f); ++w)
			for(unsigned h = 0; h < std::max(coarse_sz.height * factor, 1.f); ++h)
				for(unsigned f = initFrame; f < std::max((lastFrameCoarse + 1) * factor, initFrame + 1.f); ++f)
					for(unsigned c = 0; c < coarse_sz.channels; ++c, ++j)
					{
						fine[c + (f - initFrame) * fine_sz.channels + h*fine_sz.channels*(lastFrameFine - initFrame + 1) + w * fine_offsetChannels] = coarse[c + (f - initFrame) * coarse_sz.channels + h*coarse_sz.channels*(lastFrameCoarse - initFrame + 1) + w * coarse_offsetChannels];
					}
	}

	const char *pick_option(int *c, char **v, const char *o, const char *d) {
		int id = d ? 1 : 0;
		for (int i = 0; i < *c - id; i++) {
			if (v[i][0] == '-' && 0 == strcmp(v[i] + 1, o)) {
				char *r = v[i + id] + 1 - id;
				for (int j = i; j < *c - id; j++)
					v[j] = v[j + id + 1];
				*c -= id + 1;
				return r;
			}
		}
		return d;
	}
}

