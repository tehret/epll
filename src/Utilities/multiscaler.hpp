//
// Created by Nicola Pierazzo on 20/10/15.
//

#ifndef MULTISCALER_MULTISCALER_H
#define MULTISCALER_MULTISCALER_H

#include "LibVideoT.hpp"
#include <vector>
#include <cstring>
#include <fftw3.h>

namespace Multiscaler {
	void dct_vid(Video<float>& vid, std::vector<float>& result, int initFrame, int lastFrame);
	void idct_vid(Video<float>& vid, std::vector<float>& result, int initFrame, int lastFrame);

	void fine_to_coarse_vid(std::vector<float>& fine, std::vector<float>& coarse, float ratio, const VideoSize& fine_vid, int initFrame, int lastFrameFine, int lastFrameCoarse);
	void coarse_to_fine_vid(std::vector<float>& fine, std::vector<float>& coarse, const VideoSize& fine_sz, const VideoSize& coarse_sz, int initFrame, int lastFrameFine, int lastFrameCoarse, float factor);
	const char *pick_option(int *c, char **v, const char *o, const char *d); 

}

#endif //MULTISCALER_MULTISCALER_H
