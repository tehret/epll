#include "matchPropagationv2.h"
//int MatchPropagation::propAssInspired(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx)
//{
//	std::vector<std::pair<unsigned,int> > listNeighbors;
//	pm->getNeighbors(listNeighbors, pidx);
//
//	// Maybe parallelise here, compute the best match of the neighbor (other than himself), translate it and put it
//	for(int j = 0; j < listNeighbors.size(); ++j)
//	{
//		fm->retrieveFromForest(output, index, listNeighbors[j].first, false);
//		// index[1] because index[0] will always be the patch itself in our case
//		listNeighbors[j].first = pm->getInverseNeighboringPatch(index[1], listNeighbors[j].second, 2, 2);
//	}
//
//
//	int nbTrees = fm->getNbTrees();
//
//	std::vector<unsigned> cbin(nbTrees);
//	std::vector<std::vector<unsigned> > bins(nbTrees, std::vector<unsigned>(listNeighbors.size() + 1));
//	fm->getBins(cbin, pidx);
//	for(int j = 0; j < nbTrees; ++j)
//	{
//		bins[j][0] = cbin[j];
//	}
//	for(int i = 0; i < listNeighbors.size(); ++i)
//	{
//		fm->getBins(cbin, listNeighbors[i].first);
//		for(int j = 0; j < nbTrees; ++j)
//		{
//			bins[j][i + 1] = cbin[j];
//		}
//	}
//
//	int finalValue = fm->estimateClassicFromBins(output, index, bins, pidx, false, false);
//	
//	return finalValue;
//}
//
//int MatchPropagation::estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose)
//{
//	std::vector<float> output(kNN);
//	int finalValue = propAssInspired(output, o_index, pidx);
//
//	pm->loadGroupStep1(o_group3d, o_index, kNN, pidx);
//	return finalValue;
//}
//
//int MatchPropagation::estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose)
//{
//	std::vector<float> output(kNN);
//	int finalValue = propAssInspired(output, o_index, pidx);
//
//	pm->loadGroupStep2(o_group3dBasic, o_index, i_imNoisy, o_group3dNoisy, kNN, pidx);
//	return finalValue;
//}
//
//int MatchPropagation::estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool rerank, bool verbose)
//{
//	int finalValue = propAssInspired(output, index, pidx);
//	return finalValue;
//}
