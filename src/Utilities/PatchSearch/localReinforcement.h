//
// TOP
//

#ifndef LR_HEADER_H
#define LR_HEADER_H

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "../Utilities.h"
#include "../LibVideoT.hpp"
#include "../PatchManager/patchManager.h"
#include "../PartitionTree/forestManager.h"
#include "localPatchPropagation.h"

/**
 *
 * PTLR search
 *
 */

class LocalReinforcement : public LocalPatchPropagation {
	public:
		/**
		 * @brief basic constructor
		 *
		 * @param pm_: Current patch manager
		 * @param fm_: Forest to be used during the computation
		 * @param kNN_: Number of nearest neighbors to be computed
		 * @param prms: Denoising parameters
		 */
		LocalReinforcement(PatchManager& pm_, ForestManager& fm_, int kNN_, nlbParams prms) {pm = &pm_; fm = &fm_; kNN = kNN_; p_params = prms;};

		/**
		 * Check localPatchPropagation.h for more informations on these functions
		 * @{
		 */
		int estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose = false);
		int estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose = false);
		int estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool rerank, bool verbose = false);
		/**
		 * @}
		 */
	private:
		/// Number of nearest neighbors to be computed
		ForestManager* fm;
		/// Current patch manager
		PatchManager* pm;
		/// Number of nearest neighbors to be computed
		int kNN;
		/// Denoising parameters
		nlbParams p_params;
};
#endif
