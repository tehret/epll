#include "rosanna.h"

Rosanna::Rosanna(PatchManager& pm_, int nbTables_, int nbTop_, int nbBins_, int kNN_, nlbParams param) 
{ 
	pm = &pm_; 
	nbTables = nbTables_; 
	nbTop = nbTop_; 
	nbBins = nbBins_;
	p_params = param;
	kNN = kNN_;

	int dim = pm->getDim();

	std::vector<unsigned> allPatches(pm->getNbPatches());
	pm->getAllPatches(allPatches);
	
	printf("Starting constructing the rotation matrices\n");
	rot = std::vector<std::vector<std::vector<float> > >(nbTables, std::vector<std::vector<float> >(dim, std::vector<float>(dim, 0)));
	for(int i = 0; i < nbTables - 1; ++i)
	{
		generate_rot(rot[i]);
		printf("Finished table %d\n", i); 
	}

	printf("Let's hash now\n");
	std::vector<std::vector<float> > useless;
	std::vector<std::pair<int,int> > alsoUseless;
	for(int i = 0; i < allPatches.size(); ++i)
	{
		int secondBin;
		int firstBin = pm->hashPatch(allPatches[i], false, useless, nbTop, secondBin, alsoUseless);
		hash[allPatches[i]].resize(nbTables);
		hash[allPatches[i]][0] = std::make_pair(firstBin, secondBin); 

		bin[firstBin].resize(nbTables);
		bin[firstBin][0].resize(32);
		if(bin[firstBin][0][secondBin] == NULL)
			bin[firstBin][0][secondBin] = new std::vector<unsigned>();
		bin[firstBin][0][secondBin]->push_back(allPatches[i]);

		for(int j = 1; j < nbTables; ++j)
		{
			firstBin = pm->hashPatch(allPatches[i], true, rot[j-1], nbTop, secondBin, alsoUseless); 
			hash[allPatches[i]][j] = std::make_pair(firstBin, secondBin); 
			bin[firstBin].resize(nbTables);
			bin[firstBin][j].resize(32);
			if(bin[firstBin][j][secondBin] == NULL)
				bin[firstBin][j][secondBin] = new std::vector<unsigned>();
			bin[firstBin][j][secondBin]->push_back(allPatches[i]);
		}
	}
	printf("Hashing finished\n");
}

void Rosanna::candidates(unsigned pidx, std::vector<unsigned>& output, int types)
{
	unsigned transformId = pm->transformId(pidx);

	std::unordered_map<unsigned,int> alreadySeen;	
	for(int i = 0; i < nbTables; ++i)
	{
		std::pair<int,int> positionBins = hash[transformId][i];
		std::vector<unsigned>* listOfPossibilities = bin[positionBins.first][i][positionBins.second];
		for(int j = 0; j < listOfPossibilities->size(); ++j)
		{
			int seen = alreadySeen[(*listOfPossibilities)[j]]++;
			if(seen == 0)
				output.push_back((*listOfPossibilities)[j]);
		}
	}
}

int Rosanna::estimateDistanceSimilarPatchesStep1(std::vector<float>& dists, std::vector<unsigned>& indexes, unsigned pidx, int kNN, bool rerank, bool verbose, bool propTranslated)
{
	unsigned transformId = pm->transformId(pidx);

	std::unordered_map<unsigned,int> alreadySeen;	
	std::vector<std::pair<float, unsigned> > output;

	int firstBin;
	int secondBin;

	std::vector<std::vector<float> > useless;


	int typeSearch = 0;
	int nbElmtSeen = 0;
	// Check the first bin
	for(int i = 0; i < nbTables; ++i)
	{
		std::pair<int,int> positionBins = hash[transformId][i];
		std::vector<unsigned>* listOfPossibilities = bin[positionBins.first][i][positionBins.second];
		for(int j = 0; j < listOfPossibilities->size(); ++j)
		{
			int seen = alreadySeen[(*listOfPossibilities)[j]]++;
			if(seen == 0)
			{
				nbElmtSeen++;
				output.push_back(std::make_pair(pm->realDistance(pidx, (*listOfPossibilities)[j]), (*listOfPossibilities)[j]));
			}
		}
	}

	// Propagate elements around
	if(nbElmtSeen < 2 * kNN)
	{
		typeSearch = 1;

		std::vector<std::pair<unsigned,int> > listNeighbors;
		pm->getNeighbors(listNeighbors, pidx);

		for(int nei = 0; nei < listNeighbors.size(); ++nei)
		{
			for(int i = 0; i < nbTables; ++i)
			{
				std::pair<int,int> positionBins = hash[listNeighbors[nei].first][i];
				std::vector<unsigned>* listOfPossibilities = bin[positionBins.first][i][positionBins.second];
				for(int j = 0; j < listOfPossibilities->size(); ++j)
				{
					if(propTranslated)
					{
						unsigned newId = pm->getNeighboringPatch((*listOfPossibilities)[j], listNeighbors[nei].second, p_params.sizePatch / 2, p_params.sizePatchTime / 2);
						int seen = alreadySeen[newId]++;
						if(seen == 0)
						{
							nbElmtSeen++;
							output.push_back(std::make_pair(pm->realDistance(pidx, newId), newId));
						}
					}
					else
					{
						int seen = alreadySeen[(*listOfPossibilities)[j]]++;
						if(seen == 0)
						{
							nbElmtSeen++;
							output.push_back(std::make_pair(pm->realDistance(pidx, (*listOfPossibilities)[j]), (*listOfPossibilities)[j]));
						}
					}
				}
			}
		}
	}

	// Check the other bins around
	if(nbElmtSeen < 2 * kNN)
	{
		typeSearch = 2;
		for(int i = 0; i < nbTables; ++i)
		{
			std::vector<std::pair<int,int> > listPositionBins(0);

			if(i == 0)
				firstBin = pm->hashPatch(transformId, false, useless, nbTop, secondBin, listPositionBins, true);
			else
				firstBin = pm->hashPatch(transformId, true, rot[i-1], nbTop, secondBin, listPositionBins, true);

			for(int idxBin = 0; idxBin < listPositionBins.size(); ++idxBin)
			{
				if(bin[listPositionBins[idxBin].first].size() == 0)
				{
					continue;
				}
				if(bin[listPositionBins[idxBin].first][i].size() == 0)
				{
					continue;
				}
				if(bin[listPositionBins[idxBin].first][i][listPositionBins[idxBin].second] == NULL)
				{
					continue;
				}
				std::vector<unsigned>* listOfPossibilities = bin[listPositionBins[idxBin].first][i][listPositionBins[idxBin].second];
				for(int j = 0; j < listOfPossibilities->size(); ++j)
				{
					int seen = alreadySeen[(*listOfPossibilities)[j]]++;
					if(seen == 0)
					{
						nbElmtSeen++;
						output.push_back(std::make_pair(pm->realDistance(pidx, (*listOfPossibilities)[j]), (*listOfPossibilities)[j]));
					}
				}
			}
		}
	}
	if(nbElmtSeen < 2* kNN)
	{
		typeSearch = 3;
		printf("We need so many elements biblethump");
	}
	std::partial_sort(output.begin(), output.begin() + kNN, output.end(), comparaisonFirst);
	if(verbose)
	{
		printf("%d elements have been compared to find these ones, using type %d\n", nbElmtSeen, typeSearch);
	}

	for(int i = 0; i < ((kNN < output.size()) ? kNN : output.size()); ++i)
	{
		dists[i] = output[i].first; 
		indexes[i] = output[i].second;
	}
	return nbElmtSeen;
}

int Rosanna::estimateDistanceSimilarPatchesStep1OnlyOneBin(std::vector<float>& dists, std::vector<unsigned>& indexes, unsigned pidx, int kNN, bool rerank, bool verbose)
{
	unsigned transformId = pm->transformId(pidx);

	std::unordered_map<unsigned,int> alreadySeen;	
	std::vector<std::pair<float, unsigned> > output;
	int nbElmtSeen = 0;
	for(int i = 0; i < nbTables; ++i)
	{
		std::pair<int,int> positionBins = hash[transformId][i];
		std::vector<unsigned>* listOfPossibilities = bin[positionBins.first][i][positionBins.second];
		for(int j = 0; j < listOfPossibilities->size(); ++j)
		{
			int seen = alreadySeen[(*listOfPossibilities)[j]]++;
			if(seen == 0)
			{
				nbElmtSeen++;
				output.push_back(std::make_pair(pm->distance(pidx, (*listOfPossibilities)[j]), (*listOfPossibilities)[j]));
			}
		}
	}
	if(output.size() > kNN)
		std::partial_sort(output.begin(), output.begin() + kNN, output.end(), comparaisonFirst);
	if(verbose)
	{
		printf("%d elements have been compared to find these ones\n", nbElmtSeen);


		printf("Statistics about the hashing scheme:\n");
		int count[16];
		for(int i = 0; i < 16; ++i)
			count[i] = 0;
		for(int i = 0; i < output.size(); ++i)
			count[alreadySeen[output[i].second] - 1]++;

		for(int i = 0; i < 16; ++i)
			printf("%d -> %d\n", i, count[i]);
	}

	


	for(int i = 0; i < ((kNN < output.size()) ? kNN : output.size()); ++i)
	{
		dists[i] = output[i].first; 
		indexes[i] = output[i].second;
	}
	return nbElmtSeen;
}

int Rosanna::estimateDistanceSimilarPatchesStep1WithMorePatches(std::vector<float>& dists, std::vector<unsigned>& indexes, unsigned pidx, int kNN, bool rerank, bool verbose)
{
	unsigned transformId = pm->transformId(pidx);

	std::unordered_map<unsigned,int> alreadySeen;	
	std::vector<std::pair<float, unsigned> > output;
	int nbElmtSeen = 0;

	int firstBin;
	int secondBin;

	std::vector<std::vector<float> > useless;
	for(int i = 0; i < nbTables; ++i)
	{
		std::vector<std::pair<int,int> > listPositionBins(0);

		if(i == 0)
			firstBin = pm->hashPatch(transformId, false, useless, nbTop, secondBin, listPositionBins, true);
		else
			firstBin = pm->hashPatch(transformId, true, rot[i-1], nbTop, secondBin, listPositionBins, true);

		for(int idxBin = 0; idxBin < listPositionBins.size(); ++idxBin)
		{
			if(bin[listPositionBins[idxBin].first].size() == 0)
			{
				continue;
			}
			if(bin[listPositionBins[idxBin].first][i].size() == 0)
			{
				continue;
			}
			if(bin[listPositionBins[idxBin].first][i][listPositionBins[idxBin].second] == NULL)
			{
				continue;
			}
			std::vector<unsigned>* listOfPossibilities = bin[listPositionBins[idxBin].first][i][listPositionBins[idxBin].second];
			for(int j = 0; j < listOfPossibilities->size(); ++j)
			{
				int seen = alreadySeen[(*listOfPossibilities)[j]]++;
				if(seen == 0)
				{
					nbElmtSeen++;
					output.push_back(std::make_pair(pm->distance(pidx, (*listOfPossibilities)[j]), (*listOfPossibilities)[j]));
				}
			}
		}
	}
	if(output.size() > kNN)
		std::partial_sort(output.begin(), output.begin() + kNN, output.end(), comparaisonFirst);
	if(verbose)
	{
		printf("%d elements have been compared to find these ones\n", nbElmtSeen);


		//printf("Statistics about the hashing scheme:\n");
		//int count[16];
		//for(int i = 0; i < 16; ++i)
		//	count[i] = 0;
		//for(int i = 0; i < output.size(); ++i)
		//	count[alreadySeen[output[i].second] - 1]++;

		//for(int i = 0; i < 16; ++i)
		//	printf("%d -> %d\n", i, count[i]);
	}

	for(int i = 0; i < ((kNN < output.size()) ? kNN : output.size()); ++i)
	{
		dists[i] = output[i].first; 
		indexes[i] = output[i].second;
	}
	return nbElmtSeen;
}

inline void Rosanna::matrix_transpose(std::vector<std::vector<float> >& matrix)
{	
	for(int i = 0; i < matrix.size(); ++i)
		for(int j = 0; j < i; ++j)
		{
			float temp = matrix[i][j];
			matrix[i][j] = matrix[j][i];
			matrix[j][i] = temp; 
		}
}

inline void Rosanna::matrix_mult(std::vector<std::vector<float> >& output, std::vector<std::vector<float> >& m1, std::vector<std::vector<float> >& m2)
{
	for(int i = 0; i < output.size(); i++)
		for(int j = 0; j < output.size(); j++)
			output[i][j] = 0;

	for(int i = 0; i < m1.size(); i++)
		for(int j = 0; j < m1.size(); j++)
			for(int k = 0; k < m1.size(); k++)
				output[i][j] += m1[i][k] * m2[k][j];
}

inline float Rosanna::vector_norm(std::vector<float>& v)
{
	float norm = 0.;
	for(int i = 0; i < v.size(); ++i)
		norm += (v[i]*v[i]);
	return std::sqrt(norm);

}

inline void Rosanna::extract_minor(std::vector<std::vector<float> >& output, std::vector<std::vector<float> >& x, int d)
{
	for(int i = 0; i < x.size(); i++)
		for(int j = 0; j < x.size(); j++)
			output[i][j] = 0;

	for(int i = 0; i < d; i++)
		output[i][i] = 1;

	for(int i = d; i < x.size(); i++)
		for(int j = d; j < x.size(); j++)
			output[i][j] = x[i][j];
}

inline void Rosanna::extract_column(std::vector<std::vector<float> >& m, std::vector<float>& v, int k)
{
	for(int i = 0; i < m.size(); ++i)
		v[i] = m[i][k];
}

inline void Rosanna::lin_comb_vectors(std::vector<float>& output, std::vector<float>& v1, std::vector<float>& v2, float constMult)
{
	for(int i = 0; i < v1.size(); i++)
		output[i] = v1[i] + constMult * v2[i];
}
 
inline void Rosanna::idMinusvTv(std::vector<std::vector<float> >& output, std::vector<float>& v)
{
	for(int i = 0; i < v.size(); ++i)
		for(int j = 0; j < v.size(); j++)
			output[i][j] = -2 *  v[i] * v[j];
	for(int i = 0; i < v.size(); ++i)
		output[i][i] += 1;
}

inline void Rosanna::vector_normalize(std::vector<float>& v)
{
	float norm = vector_norm(v);
	for(int i = 0; i < v.size(); ++i)
		v[i] /= norm;
}

// FIXME Verify that it works
void Rosanna::generate_rot(std::vector<std::vector<float> >& rotation)
{
	// TODO add a random seed to this generator
	std::default_random_engine generator;
	std::normal_distribution<float> normal_distribution(0.,1.);

	std::vector<std::vector<float> > matrix_rand_init(rotation);
	for(int i = 0; i < matrix_rand_init.size(); ++i)
		for(int j = 0; j < matrix_rand_init.size(); ++j)
			matrix_rand_init[i][j] = normal_distribution(generator);

	std::vector<std::vector<float> > matrix_temp(rotation);
	std::vector<std::vector<float> > z(matrix_rand_init);
        std::vector<std::vector<float> > z1(matrix_rand_init);

	for(int k = 0; k < rotation.size() - 1; k++)
	{
		std::vector<float> e(rotation.size());
		std::vector<float> v(rotation.size());
	        float n;

		extract_minor(z1, z, k);
		z = z1;
 
		extract_column(z, v, k);
		n = vector_norm(v);
		if (matrix_rand_init[k][k] > 0) 
			n = -n;
 
		for (int i = 0; i < rotation.size(); i++)
			e[i] = (i == k) ? 1 : 0;
 
		lin_comb_vectors(e, v, e, n);
		vector_normalize(e);
		idMinusvTv(matrix_temp, e);

		if(k == 0)
			rotation = matrix_temp;
		else
		{
			std::vector<std::vector<float> > temp(rotation);
			matrix_mult(rotation, temp, matrix_temp);
		}

		matrix_mult(z1, matrix_temp, z);
		z = z1;
	}
	matrix_transpose(rotation);
}
