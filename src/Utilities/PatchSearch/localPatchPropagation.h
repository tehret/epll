/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOCALPATCHPROP_HEADER_H
#define LOCALPATCHPROP_HEADER_H

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "../Utilities.h"
#include "../LibVideoT.hpp"
#include "../PatchManager/patchManager.h"
#include "../PartitionTree/forestManager.h"

/**
 *
 * Basis for different patch search
 *
 */

class LocalPatchPropagation {
	public:
		/**
		 * @brief Compute the nearest neighbors and return them in the form used during the first of the denoising algorithm
		 *
		 * @param o_group3d: Used to return the patches corresponding to the nearest neighbors
		 * @param o_index: Used to return the position of the patches corresponding to the nearest neighbors
		 * @param pidx: The query patch
		 * @param rerank: If using a dimensionality reduction, rerank the results using the true distances
		 * @param verbose: Print temporal computation that can be useful
		 *
		 * @return the number of nearest neighbors computed, (currently it is always kNN if there wasn't any problem)
		 **/
		virtual int estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose = false) = 0;

		/**
		 * @brief Compute the nearest neighbors and return them in the form used during the second of the denoising algorithm
		 *
		 * @param i_imNoisy: the noisy dataset
		 * @param o_group3dNoisy: Used to return the noisy patches corresponding to the nearest neighbors
		 * @param o_group3dBasic: Used to return the patches from step 1 corresponding to the nearest neighbors
		 * @param o_index: Used to return the position of the patches corresponding to the nearest neighbors
		 * @param pidx: The query patch
		 * @param rerank: If using a dimensionality reduction, rerank the results using the true distances
		 * @param verbose: Print temporal computation that can be useful
		 *
		 * @return the number of nearest neighbors computed, (currently it is always kNN if there wasn't any problem)
		 **/
		virtual int estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose = false) = 0;

		/**
		 * @brief Compute the nearest neighbors and return their position and the distance to each of them
		 *
		 * @param output: Used to return the distance to the patches corresponding to the nearest neighbors
		 * @param index: Used to return the position of the patches corresponding to the nearest neighbors
		 * @param pidx: The query patch
		 * @param rerank: If using a dimensionality reduction, rerank the results using the true distances
		 * @param verbose: Print temporal computation that can be useful
		 *
		 * @return the number of nearest neighbors computed, (currently it is always kNN if there wasn't any problem)
		 **/
		virtual int estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool rerank, bool verbose) = 0;
};
#endif
