#include "localPatchPropagationCount.h"

int LocalPatchPropagationCount::estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose)
{
	std::unordered_map<unsigned, int> compteur;
	std::vector<unsigned> listElements;

	count(listElements, compteur, pidx, verbose);

	std::vector<std::pair<int, unsigned> > distancePair(listElements.size());
	for(int i = 0; i < listElements.size(); ++i)
	{
		distancePair[i] = std::make_pair(compteur[listElements[i]], listElements[i]);
	}

	std::partial_sort(distancePair.begin(), distancePair.begin() + kNN, distancePair.end(), comparaisonInverseFirst);
	for (int i = 0; i < kNN; i++)
	{
		o_index[i] = distancePair[i].second;
	}

	pm->loadGroupStep1(o_group3d, o_index, kNN, pidx);

	return listElements.size();
}

int LocalPatchPropagationCount::estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose)
{
	std::unordered_map<unsigned, int> compteur;
	std::vector<unsigned> listElements;

	count(listElements, compteur, pidx, verbose);

	std::vector<std::pair<int, unsigned> > distancePair(listElements.size());
	for(int i = 0; i < listElements.size(); ++i)
	{
		distancePair[i] = std::make_pair(compteur[listElements[i]], listElements[i]);
	}

	std::partial_sort(distancePair.begin(), distancePair.begin() + kNN, distancePair.end(), comparaisonInverseFirst);
	for (int i = 0; i < kNN; i++)
	{
		o_index[i] = distancePair[i].second;
	}

	pm->loadGroupStep2(o_group3dBasic, o_index, i_imNoisy, o_group3dNoisy, kNN, pidx);
	return listElements.size();
}

void LocalPatchPropagationCount::count(std::vector<unsigned>& listElements, std::unordered_map<unsigned, int>& compteur, const unsigned pidx, bool verbose)
{
	if(verbose)
		printf("Adding elements from patch %u\n", pidx);
	fm->count(listElements, compteur, pidx, verbose);
	compteur[pidx]++;

	std::vector<std::pair<unsigned,int> > listNeighbors;
	pm->getNeighbors(listNeighbors, pidx);

	// Propagate the bin
	for(int i = 0; i < listNeighbors.size(); ++i)
	{
		if(verbose)
			printf("Adding elements from patch %u\n", pidx);
		fm->count(listElements, compteur, listNeighbors[i].first, verbose);
		compteur[listNeighbors[i].first]++;
	}

	// Propagate the elements from the bins 
	for(int i = 0; i < listNeighbors.size(); ++i)
	{
		if(verbose)
			printf("Adding elements from patch %u\n", pidx);
		fm->countNeighbors(listElements, compteur, listNeighbors[i].second, listNeighbors[i].first, verbose);
	}
	if(verbose)
		printf("-------------------------\n");
}

int LocalPatchPropagationCount::estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool rerank, bool verbose)
{
	std::unordered_map<unsigned, int> compteur;
	std::vector<unsigned> listElements;

	count(listElements, compteur, pidx, verbose);

	std::vector<std::pair<int, unsigned> > distancePair(listElements.size());
	for(int i = 0; i < listElements.size(); ++i)
	{
		distancePair[i] = std::make_pair(compteur[listElements[i]], listElements[i]);
	}

	std::partial_sort(distancePair.begin(), distancePair.begin() + kNN, distancePair.end(), comparaisonInverseFirst);
	for (int i = 0; i < kNN; i++)
	{
		index[i] = distancePair[i].second;
		output[i] = pm->distance(pidx, index[i]);
	}
	std::sort(output.begin(), output.end());

	return listElements.size();
}
