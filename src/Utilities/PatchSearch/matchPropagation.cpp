/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file matchPropagation.cpp
 * @brief Used to compute the PTMP for given partition tree forest
 *
 * @author Thibaud Ehret <ehret.thibaud@gmail.com>
 **/

#include "matchPropagation.h"

int MatchPropagation::estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose)
{
	std::vector<std::pair<unsigned,int> > listNeighbors;
	pm->getNeighbors(listNeighbors, pidx);
	int nbTrees = fm->getNbTrees();

	std::vector<unsigned> cbin(nbTrees);
	std::vector<std::vector<unsigned> > bins(nbTrees, std::vector<unsigned>(listNeighbors.size() + 1));
	// Get the current bins
	fm->getBins(cbin, pidx);
	for(int j = 0; j < nbTrees; ++j)
	{
		bins[j][0] = cbin[j];
	}

	// Propagate the elements in the neighboring bins
	for(int i = 0; i < listNeighbors.size(); ++i)
	{
		fm->getBins(cbin, listNeighbors[i].first);
		for(int j = 0; j < nbTrees; ++j)
		{
			bins[j][i + 1] = cbin[j];
		}
	}

	std::vector<float> outputDistances(kNN);
	int finalValue = fm->estimateClassicFromBins(outputDistances, o_index, bins, pidx, false, verbose);

	pm->loadGroupStep1(o_group3d, o_index, kNN, pidx);
	return finalValue;
}

int MatchPropagation::estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose)
{
	std::vector<std::pair<unsigned,int> > listNeighbors;
	pm->getNeighbors(listNeighbors, pidx);
	int nbTrees = fm->getNbTrees();

	std::vector<unsigned> cbin(nbTrees);
	std::vector<std::vector<unsigned> > bins(nbTrees, std::vector<unsigned>(listNeighbors.size() + 1));
	// Get the current bins
	fm->getBins(cbin, pidx);
	for(int j = 0; j < nbTrees; ++j)
	{
		bins[j][0] = cbin[j];
	}

	// Propagate the elements in the neighboring bins
	for(int i = 0; i < listNeighbors.size(); ++i)
	{
		fm->getBins(cbin, listNeighbors[i].first);
		for(int j = 0; j < nbTrees; ++j)
		{
			bins[j][i + 1] = cbin[j];
		}
	}

	std::vector<float> outputDistances(kNN);
	int finalValue = fm->estimateClassicFromBins(outputDistances, o_index, bins, pidx, false, verbose);

	pm->loadGroupStep2(o_group3dBasic, o_index, i_imNoisy, o_group3dNoisy, kNN, pidx);
	return finalValue;
}

int MatchPropagation::estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool rerank, bool verbose)
{
	std::vector<std::pair<unsigned,int> > listNeighbors;
	pm->getNeighbors(listNeighbors, pidx);
	int nbTrees = fm->getNbTrees();

	std::vector<unsigned> cbin(nbTrees);
	std::vector<std::vector<unsigned> > bins(nbTrees, std::vector<unsigned>(listNeighbors.size() + 1));
	// Get the current bins
	fm->getBins(cbin, pidx);
	for(int j = 0; j < nbTrees; ++j)
	{
		bins[j][0] = cbin[j];
	}

	// Propagate the elements in the neighboring bins
	for(int i = 0; i < listNeighbors.size(); ++i)
	{
		fm->getBins(cbin, listNeighbors[i].first);
		for(int j = 0; j < nbTrees; ++j)
		{
			bins[j][i + 1] = cbin[j];
		}
	}

	int finalValue = fm->estimateClassicFromBins(output, index, bins, pidx, false, verbose);
	
	return finalValue;
}

