/*
 * Original work Copyright (c) 2016, Thibaud Ehret <ehret.thibaud@gmail.com>
 * All rights reserved.
 *
 * This program is free software: you can use, modify and/or
 * redistribute it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later
 * version. You should have received a copy of this license along
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MATCHPROP_HEADER_H
#define MATCHPROP_HEADER_H

#include "localPatchPropagation.h"


/**
 *
 * PTMP search
 *
 */

class MatchPropagation : public LocalPatchPropagation {
	public:
		/**
		 * @brief basic constructor
		 *
		 * @param fm_: Forest to be used during the computation
		 * @param pm_: Current patch manager
		 * @param kNN_: Number of nearest neighbors to be computed
		 */
		MatchPropagation(ForestManager& fm_, PatchManager& pm_, int kNN_) {fm = &fm_; pm = &pm_; kNN = kNN_;};

		/**
		 * Check localPatchPropagation.h for more informations on these functions
		 * @{
		 */
		int estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose = false);
		int estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose = false);
		int estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool rerank, bool verbose);
		/**
		 * @}
		 */
	private:
		/// Current patch manager
		PatchManager* pm;
		/// Pointer toward the forest to be used
		ForestManager* fm;
		/// Number of nearest neighbors to be computed
		int kNN;
};
#endif
