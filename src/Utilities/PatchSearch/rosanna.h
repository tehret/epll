//
// TOP
//

#ifndef ROSANNA_HEADER_H
#define ROSANNA_HEADER_H

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <random>
#include <cmath>
#include "../PatchManager/patchManager.h"
#include <unordered_map>
#include "../../NlBayes/VideoNLBayes.h"

/* 
 * Implementation of a LSH scheme which seems adapted to image application
 * /!\ It is hard coded for vector of length 64 while considering only the 4 top values
 * TODO generalize this to any size vector and more than the 4 top values
 */

class Rosanna {
	public:
		Rosanna(PatchManager& pm_, int nbTables_, int nbTop_, int nbBins_, int kNN_, nlbParams param);
		void candidates(unsigned pidx, std::vector<unsigned>& output, int type);
		int estimateDistanceSimilarPatchesStep1(std::vector<float>& dists, std::vector<unsigned>& indexes, unsigned pidx, int kNN, bool rerank = false, bool verbose = false, bool propTranslated = false);
		int estimateDistanceSimilarPatchesStep1OnlyOneBin(std::vector<float>& dists, std::vector<unsigned>& indexes, unsigned pidx, int kNN, bool rerank = false, bool verbose = false);
		int estimateDistanceSimilarPatchesStep1WithMorePatches(std::vector<float>& dists, std::vector<unsigned>& indexes, unsigned pidx, int kNN, bool rerank = false, bool verbose = false);

	private:
		inline void matrix_transpose(std::vector<std::vector<float> >& matrix);
		inline void matrix_mult(std::vector<std::vector<float> >& output, std::vector<std::vector<float> >& m1, std::vector<std::vector<float> >& m2);
		inline float vector_norm(std::vector<float>& v);
		inline void extract_minor(std::vector<std::vector<float> >& output, std::vector<std::vector<float> >& x, int d);
		inline void extract_column(std::vector<std::vector<float> >& m, std::vector<float>& v, int k);
		inline void lin_comb_vectors(std::vector<float>& output, std::vector<float>& v1, std::vector<float>& v2, float constMult);
		inline void idMinusvTv(std::vector<std::vector<float> >& output, std::vector<float>& v);
		inline void vector_normalize(std::vector<float>& v);
		void generate_rot(std::vector<std::vector<float> >& rotation);

		PatchManager* pm;
		int nbTables;
		int nbTop;
		int nbBins;
		int kNN;
		nlbParams p_params;

		std::unordered_map<unsigned, std::vector<std::pair<int, int> > > hash;
		std::unordered_map<int, std::vector<std::vector<std::vector<unsigned>* > > > bin;
		std::vector<std::vector<std::vector<float> > > rot;
		
};
#endif
