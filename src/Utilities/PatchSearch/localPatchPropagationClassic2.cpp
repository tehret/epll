#include "localPatchPropagationClassic2.h"

int LocalPatchPropagationClassic2::estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose)
{
	std::vector<std::pair<unsigned,int> > listNeighbors;
	pm->getNeighbors(listNeighbors, pidx);
	std::vector<unsigned> listElements;
       	std::unordered_map<unsigned, int> compteur;

	if(verbose)
		printf("Adding elements from patch %u\n", pidx);
	fm->count(listElements, compteur, pidx, verbose);
	for(int i = 0; i < listNeighbors.size(); ++i)
	{
		if(verbose)
			printf("Adding elements from neighbor %u (type %d neighbor)\n", listNeighbors[i].first, listNeighbors[i].second);
		fm->countNeighbors(listElements, compteur, listNeighbors[i].second, listNeighbors[i].first, verbose);
	}

	std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs> file;
	for(int i = 0; i < listElements.size(); ++i)
	{
		file.push(std::make_pair(pm->distance(pidx, listElements[i]), listElements[i]));
		if(file.size() > kNN)
			file.pop();
	}

	for(int i = 0; i < kNN; ++i)
	{
		o_index[kNN -1 - i] = file.top().second;
		file.pop();
	}

	pm->loadGroupStep1(o_group3d, o_index, kNN, pidx);
	return listElements.size();
}

int LocalPatchPropagationClassic2::estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose)
{
	std::vector<std::pair<unsigned,int> > listNeighbors;
	pm->getNeighbors(listNeighbors, pidx);
	std::vector<unsigned> listElements;
       	std::unordered_map<unsigned, int> compteur;

	if(verbose)
		printf("Adding elements from patch %u\n", pidx);
	fm->count(listElements, compteur, pidx, verbose);
	for(int i = 0; i < listNeighbors.size(); ++i)
	{
		if(verbose)
			printf("Adding elements from neighbor %u (type %d neighbor)\n", listNeighbors[i].first, listNeighbors[i].second);
		fm->countNeighbors(listElements, compteur, listNeighbors[i].second, listNeighbors[i].first, verbose);
	}
	if(verbose)
		printf("----------------------------\n");

	std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs> file;
	for(int i = 0; i < listElements.size(); ++i)
	{
		file.push(std::make_pair(pm->distance(pidx, listElements[i]), listElements[i]));
		if(file.size() > kNN)
			file.pop();
	}

	for(int i = 0; i < kNN; ++i)
	{
		o_index[kNN -1 - i] = file.top().second;
		file.pop();
	}

	pm->loadGroupStep2(o_group3dBasic, o_index, i_imNoisy, o_group3dNoisy, kNN, pidx);
	return listElements.size();
}

int LocalPatchPropagationClassic2::estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool rerank, bool verbose)
{
	std::vector<std::pair<unsigned,int> > listNeighbors;
	pm->getNeighbors(listNeighbors, pidx);
	std::vector<unsigned> listElements;
       	std::unordered_map<unsigned, int> compteur;

	if(verbose)
		printf("Adding elements from patch %u, %d neighbors to consider\n", pidx, listNeighbors.size());
	fm->count(listElements, compteur, pidx, verbose);
	for(int i = 0; i < listNeighbors.size(); ++i)
	{
		if(verbose)
			printf("Adding elements from neighbor %u (type %d neighbor)\n", listNeighbors[i].first, listNeighbors[i].second);
		fm->countNeighbors(listElements, compteur, listNeighbors[i].second, listNeighbors[i].first, verbose);
	}
	if(verbose)
		printf("----------------------------\n");

	std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs> file;
	for(int i = 0; i < listElements.size(); ++i)
	{
		file.push(std::make_pair(pm->distance(pidx, listElements[i]), listElements[i]));
		if(file.size() > kNN)
			file.pop();
	}

	for(int i = 0; i < kNN; ++i)
	{
		index[kNN -1 - i] = file.top().second;
		output[kNN -1 - i] = file.top().first;
		file.pop();
	}
	return listElements.size();
}
