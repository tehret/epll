#include "localReinforcement.h"

#define LR_X 10
#define LR_Y 10
#define LR_F 1
#define LR_B 1

int LocalReinforcement::estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose)
{

	// Get the best patches from the forestmanager
	std::vector<float> output(kNN, 0.f);
	int finalValue = fm->retrieveFromForest(output, o_index, pidx, false, verbose);

	std::unordered_map<unsigned, int> alreadySeen;
	std::vector<std::pair<float, unsigned> > listMatches(0);
	for(int i = 0; i < kNN; ++i)
	{
		++alreadySeen[o_index[i]];
		listMatches.push_back(std::make_pair(output[i],o_index[i]));	
	}

	int sWx = LR_X;
	int sWy = LR_Y;
	const int sWt_f = LR_F;
	const int sWt_b = LR_B;
	const int sPx   = p_params.sizePatch;
	const int sPt   = p_params.sizePatchTime;

	const VideoSize* i_im_sz = pm->infoVid();

	// Update the list using some local search centered around each patches (LR step)
	for(int i = 0; i < kNN; ++i)
	{
		// Define the small search region around the current match
		unsigned px, py, pt, pc;
		i_im_sz->coords(o_index[i], px, py, pt, pc);

		unsigned rangex[2];
		unsigned rangey[2];
		unsigned ranget[2];

#ifdef CENTRED_SEARCH
		rangex[0] = std::max(0, (int)px - (sWx-1)/2);
		rangey[0] = std::max(0, (int)py - (sWy-1)/2);
		ranget[0] = std::max(0, (int)pt -  sWt_b   );

		rangex[1] = std::min((int)i_im_sz->width  - sPx, (int)px + (sWx-1)/2);
		rangey[1] = std::min((int)i_im_sz->height - sPx, (int)py + (sWy-1)/2);
		ranget[1] = std::min((int)i_im_sz->frames - sPt, (int)pt +  sWt_f   );
#else
		int shift_x = std::min(0, (int)px - (sWx-1)/2); 
		int shift_y = std::min(0, (int)py - (sWy-1)/2); 
		int shift_t = std::min(0, (int)pt -  sWt_b   ); 

		shift_x += std::max(0, (int)px + (sWx-1)/2 - (int)i_im_sz->width  + sPx); 
		shift_y += std::max(0, (int)py + (sWy-1)/2 - (int)i_im_sz->height + sPx); 
		shift_t += std::max(0, (int)pt +  sWt_f    - (int)i_im_sz->frames + sPt); 

		rangex[0] = std::max(0, (int)px - (sWx-1)/2 - shift_x);
		rangey[0] = std::max(0, (int)py - (sWy-1)/2 - shift_y);
		ranget[0] = std::max(0, (int)pt -  sWt_b    - shift_t);

		rangex[1] = std::min((int)i_im_sz->width  - sPx, (int)px + (sWx-1)/2 - shift_x);
		rangey[1] = std::min((int)i_im_sz->height - sPx, (int)py + (sWy-1)/2 - shift_y);
		ranget[1] = std::min((int)i_im_sz->frames - sPt, (int)pt +  sWt_f    - shift_t);
#endif

		//! Redefine size of search range
		sWx = rangex[1] - rangex[0] + 1;
		sWy = rangey[1] - rangey[0] + 1;
		int sWt = ranget[1] - ranget[0] + 1;

		for (unsigned qt = ranget[0], dt = 0; qt <= ranget[1]; qt++, dt++)
			for (unsigned qy = rangey[0], dy = 0; qy <= rangey[1]; qy++, dy++)
				for (unsigned qx = rangex[0], dx = 0; qx <= rangex[1]; qx++, dx++)
				{
					unsigned currentPatch = i_im_sz->index(qx, qy, qt, 0);
					int seen = alreadySeen[currentPatch]++;
					if(seen == 0)
					{
						listMatches.push_back(std::make_pair(pm->distanceOrigPos(pidx, currentPatch), currentPatch));
					}
				}

	}
	std::partial_sort(listMatches.begin(), listMatches.begin() + kNN, listMatches.end(), comparaisonFirst);
	for (unsigned n = 0; n < kNN; n++)
	{
		o_index[n] = listMatches[n].second;
	}
	pm->loadGroupStep1(o_group3d, o_index, kNN, pidx);

	return finalValue;
}

int LocalReinforcement::estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose)
{
	// Get the best patches from the forestmanager
	std::vector<float> output(kNN, 0.f);
	int finalValue = fm->retrieveFromForest(output, o_index, pidx, false, verbose);

	//LocalPatchPropagationClassic lpp = LocalPatchPropagationClassic(*fm, *pm, kNN);
	//int finalValue = lpp->estimateClassicFromBins(output, o_index, bins, pidx, false, verbose);

	std::unordered_map<unsigned, int> alreadySeen;
	std::vector<std::pair<float, unsigned> > listMatches(0);
	for(int i = 0; i < kNN; ++i)
	{
		++alreadySeen[o_index[i]];
		listMatches.push_back(std::make_pair(output[i], o_index[i]));	
	}

	int sWx = LR_X;
	int sWy = LR_Y;
	const int sWt_f = LR_F;
	const int sWt_b = LR_B;
	const int sPx   = p_params.sizePatch;
	const int sPt   = p_params.sizePatchTime;

	const VideoSize* i_im_sz = pm->infoVid(); 

	// Update the list using some local search centered around each patches (LR step)
	for(int i = 0; i < kNN; ++i)
	{
		// Define the small search region around the current match
		unsigned px, py, pt, pc;
		i_im_sz->coords(o_index[i], px, py, pt, pc);

		unsigned rangex[2];
		unsigned rangey[2];
		unsigned ranget[2];

#ifdef CENTRED_SEARCH
		rangex[0] = std::max(0, (int)px - (sWx-1)/2);
		rangey[0] = std::max(0, (int)py - (sWy-1)/2);
		ranget[0] = std::max(0, (int)pt -  sWt_b   );

		rangex[1] = std::min((int)i_im_sz->width  - sPx, (int)px + (sWx-1)/2);
		rangey[1] = std::min((int)i_im_sz->height - sPx, (int)py + (sWy-1)/2);
		ranget[1] = std::min((int)i_im_sz->frames - sPt, (int)pt +  sWt_f   );
#else
		int shift_x = std::min(0, (int)px - (sWx-1)/2); 
		int shift_y = std::min(0, (int)py - (sWy-1)/2); 
		int shift_t = std::min(0, (int)pt -  sWt_b   ); 

		shift_x += std::max(0, (int)px + (sWx-1)/2 - (int)i_im_sz->width  + sPx); 
		shift_y += std::max(0, (int)py + (sWy-1)/2 - (int)i_im_sz->height + sPx); 
		shift_t += std::max(0, (int)pt +  sWt_f    - (int)i_im_sz->frames + sPt); 

		rangex[0] = std::max(0, (int)px - (sWx-1)/2 - shift_x);
		rangey[0] = std::max(0, (int)py - (sWy-1)/2 - shift_y);
		ranget[0] = std::max(0, (int)pt -  sWt_b    - shift_t);

		rangex[1] = std::min((int)i_im_sz->width  - sPx, (int)px + (sWx-1)/2 - shift_x);
		rangey[1] = std::min((int)i_im_sz->height - sPx, (int)py + (sWy-1)/2 - shift_y);
		ranget[1] = std::min((int)i_im_sz->frames - sPt, (int)pt +  sWt_f    - shift_t);
#endif

		//! Redefine size of search range
		sWx = rangex[1] - rangex[0] + 1;
		sWy = rangey[1] - rangey[0] + 1;
		int sWt = ranget[1] - ranget[0] + 1;

		for (unsigned qt = ranget[0], dt = 0; qt <= ranget[1]; qt++, dt++)
			for (unsigned qy = rangey[0], dy = 0; qy <= rangey[1]; qy++, dy++)
				for (unsigned qx = rangex[0], dx = 0; qx <= rangex[1]; qx++, dx++)
				{
					unsigned currentPatch = i_im_sz->index(qx, qy, qt, 0);
					int seen = alreadySeen[currentPatch]++;
					if(seen == 0)
					{
						listMatches.push_back(std::make_pair(pm->distanceOrigPos(pidx, currentPatch), currentPatch));
					}
				}

	}
	std::partial_sort(listMatches.begin(), listMatches.begin() + kNN, listMatches.end(), comparaisonFirst);
	for (unsigned n = 0; n < kNN; n++)
	{
		o_index[n] = listMatches[n].second;
	}
	pm->loadGroupStep2(o_group3dBasic, o_index, i_imNoisy, o_group3dNoisy, kNN, pidx);

	return finalValue;
}

int LocalReinforcement::estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool rerank, bool verbose)
{
	// Get the best patches from the forestmanager
	fm->estimateDistanceSimilarPatchesStep1(output, index, pidx, rerank, verbose);	

	std::unordered_map<unsigned, int> alreadySeen;
	std::vector<std::pair<float, unsigned> > listMatches(0);
	for(int i = 0; i < kNN; ++i)
	{
		++alreadySeen[index[i]];
		listMatches.push_back(std::make_pair(output[i],index[i]));	
	}

	int sWx = LR_X;
	int sWy = LR_Y;
	const int sWt_f = LR_F;
	const int sWt_b = LR_B;
	const int sPx   = p_params.sizePatch;
	const int sPt   = p_params.sizePatchTime;


	const VideoSize* i_im_sz = pm->infoVid();

	// Update the list using some local search centered around each patches (LR step)
	for(int i = 0; i < kNN; ++i)
	{
		// Define the small search region around the current match
		unsigned px, py, pt, pc;
		i_im_sz->coords(index[i], px, py, pt, pc);

		unsigned rangex[2];
		unsigned rangey[2];
		unsigned ranget[2];

#ifdef CENTRED_SEARCH
		rangex[0] = std::max(0, (int)px - (sWx-1)/2);
		rangey[0] = std::max(0, (int)py - (sWy-1)/2);
		ranget[0] = std::max(0, (int)pt -  sWt_b   );

		rangex[1] = std::min((int)i_im_sz->width  - sPx, (int)px + (sWx-1)/2);
		rangey[1] = std::min((int)i_im_sz->height - sPx, (int)py + (sWy-1)/2);
		ranget[1] = std::min((int)i_im_sz->frames - sPt, (int)pt +  sWt_f   );
#else
		int shift_x = std::min(0, (int)px - (sWx-1)/2); 
		int shift_y = std::min(0, (int)py - (sWy-1)/2); 
		int shift_t = std::min(0, (int)pt -  sWt_b   ); 

		shift_x += std::max(0, (int)px + (sWx-1)/2 - (int)i_im_sz->width  + sPx); 
		shift_y += std::max(0, (int)py + (sWy-1)/2 - (int)i_im_sz->height + sPx); 
		shift_t += std::max(0, (int)pt +  sWt_f    - (int)i_im_sz->frames + sPt); 

		rangex[0] = std::max(0, (int)px - (sWx-1)/2 - shift_x);
		rangey[0] = std::max(0, (int)py - (sWy-1)/2 - shift_y);
		ranget[0] = std::max(0, (int)pt -  sWt_b    - shift_t);

		rangex[1] = std::min((int)i_im_sz->width  - sPx, (int)px + (sWx-1)/2 - shift_x);
		rangey[1] = std::min((int)i_im_sz->height - sPx, (int)py + (sWy-1)/2 - shift_y);
		ranget[1] = std::min((int)i_im_sz->frames - sPt, (int)pt +  sWt_f    - shift_t);
#endif

		//! Redefine size of search range
		sWx = rangex[1] - rangex[0] + 1;
		sWy = rangey[1] - rangey[0] + 1;
		int sWt = ranget[1] - ranget[0] + 1;

		for (unsigned qt = ranget[0], dt = 0; qt <= ranget[1]; qt++, dt++)
			for (unsigned qy = rangey[0], dy = 0; qy <= rangey[1]; qy++, dy++)
				for (unsigned qx = rangex[0], dx = 0; qx <= rangex[1]; qx++, dx++)
				{
					unsigned currentPatch = i_im_sz->index(qx, qy, qt, 0);
					int seen = alreadySeen[currentPatch]++;
					if(seen == 0)
					{
						listMatches.push_back(std::make_pair(pm->distanceOrigPos(pidx, currentPatch), currentPatch));
					}
				}

	}
	std::partial_sort(listMatches.begin(), listMatches.begin() + kNN, listMatches.end(), comparaisonFirst);
	for (unsigned n = 0; n < kNN; n++)
	{
		output[n] = listMatches[n].first;
		index[n] = listMatches[n].second;
	}
	return kNN;
}
