//
// TOP
//

#ifndef LOCALPATCHPROPCOUNT_HEADER_H
#define LOCALPATCHPROPCOUNT_HEADER_H

#include "localPatchPropagation.h"

class LocalPatchPropagationCount : public LocalPatchPropagation {
	public:
		LocalPatchPropagationCount(ForestManager& fm_, PatchManager& pm_, int kNN_) {fm = &fm_; pm = &pm_; kNN = kNN_;};

		int estimateSimilarPatchesStep1(std::vector<std::vector<float> > &o_group3d, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose = false);
		int estimateSimilarPatchesStep2(Video<float> const& i_imNoisy, std::vector<float> &o_group3dNoisy, std::vector<float> &o_group3dBasic, std::vector<unsigned> &o_index, const unsigned pidx, bool rerank, bool verbose = false);
		int estimateDistanceSimilarPatchesStep1(std::vector<float> &output, std::vector<unsigned> &index, const unsigned pidx, bool rerank, bool verbose);

	private:
		void count(std::vector<unsigned>& listElements, std::unordered_map<unsigned, int>& compteur, const unsigned pidx, bool verbose = false);

		PatchManager* pm;
		ForestManager* fm;
		int kNN;

};
#endif
