//
// TOP
//

#ifndef PATCHMATCH_HEADER_H
#define PATCHMATCH_HEADER_H

#include "../PartitionTree/forestManager.h"
#include "../PartitionTree/prmTree.h"
#include "LibVideoT.hpp"
#include "Utilities.h"
#include <vector>
#include <unordered_map>
#include <algorithm> 
#include "../PartitionTree/vptree.h"
#include "../PartitionTree/kdtree.h"
#include "../comparators.h"
#ifdef _OPENMP
#include <omp.h>
#endif


class Patchmatch
{
	public:
		Patchmatch(Video<float>& v, PatchManager& pm_, int kNN_, int sp_, int st_, unsigned psx_, unsigned pst_);

		// Different types of initialization for patchmatch
		// 0 -> no problem, -1 failure
		//
		// Initialization random like in the original article
		int initializeRandom();
		// Initialization using a Forest 
		int initializeForest(int nbTrees, bool rerank, int type, PrmTree& prm, PatchManager& pm_);

		// Do the patchmatch propagation step
		//
		// 1 step
		void propagate();
		void backpropagate();
		// N step alternating between propagation and backpropagation
		void propagateNtimes(int N);

		// Get the result after using patchmatch 
		void extract(std::vector<unsigned>& output, std::vector<float>& distances, unsigned pidx);

		int swSearch(std::vector<unsigned>& output, std::vector<float>& distances, unsigned pidx, int nbNN, float epsilon = 0.);
		void randomSearch();
	private:
		void propToId(unsigned currentId, unsigned nextId, int type);

		Video<float>* vid;
		//std::unordered_map<unsigned, std::vector<float> > mappingDistances;
		//std::unordered_map<unsigned, std::vector<unsigned> > mappingId;
		Video<std::vector<float> > mappingDistances;
		Video<std::vector<unsigned> > mappingId;
		PatchManager* pm;
		int kNN;
		
		// patchmatch shift
		int st;
		int sp;

		unsigned psx;
		unsigned pst;

		ForestManager forest;
		bool usingTree;

};
#endif
