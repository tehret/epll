//
// TOP
//

#ifndef PATCHMATCHBIN_HEADER_H 
#define PATCHMATCHBIN_HEADER_H 

#include "../PartitionTree/forestManager.h"
#include "../PartitionTree/prmTree.h"
#include "LibVideoT.hpp"
#include "Utilities.h"
#include <vector>
#include <unordered_map>
#include <algorithm> 
#include "../PartitionTree/vptree.h"
#include "../PartitionTree/kdtree.h"
#include "../comparators.h"
#ifdef _OPENMP
#include <omp.h>
#endif

class PatchmatchBin
{
	public:
		PatchmatchBin(Video<float>& v, PatchManager& pm_, int kNN_, int sp, int st, unsigned a, unsigned b, unsigned c, unsigned d, unsigned e, unsigned f);

		// Different types of initialization for patchmatch
		// 0 -> no problem, -1 failure
		//
		// Initialization using a Forest 
		int initializeForest(int nbTrees, bool rerank, int type, PrmTree& prm);

		// Do the patchmatch propagation step
		//
		// 1 step
		void propagate();
		void backpropagate();
		// N step alternating between propagation and backpropagation
		void propagateNtimes(int N);

		// Get the result after using patchmatch 
		void extract(std::vector<unsigned>& output, std::vector<float>& distances, unsigned pidx);
	private:
		void propToId(unsigned currentId, unsigned nextId, int type);

		Video<float>* vid;
		std::unordered_map<unsigned, std::vector<float> > mappingDistances;
		std::unordered_map<unsigned, std::vector<unsigned> > mappingId;
		PatchManager* pm;
		int kNN;

		ForestManager* fm;
		
		// patchmatch shift
		int st;
		int sp;

		unsigned startX;
		unsigned startY;
		unsigned startT;
		unsigned width;
		unsigned height;
		unsigned frames;

};
#endif
