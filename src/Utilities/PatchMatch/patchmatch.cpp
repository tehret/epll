#include "patchmatch.h"

static int rand_gen(int i)
{
	return std::rand() % i;
}

Patchmatch::Patchmatch(Video<float>& v, PatchManager& pm_, int kNN_, int sp_, int st_, unsigned psx_, unsigned pst_)
{
	vid = &v;
	pm = &pm_;
	kNN = kNN_;

	sp = sp_;
	st = st_;

	psx = psx_;
	pst = pst_;
}

int Patchmatch::initializeRandom()
{
	//int nbPatches = pm->getNbPatches();
	//std::vector<unsigned> allPatches(nbPatches);
	//pm->getAllPatches(allPatches);

	//std::vector<unsigned> guessId(kNN);
	//std::vector<float> guessDistances(kNN);
	
	usingTree = false;

	mappingId.resize(vid->sz.width, vid->sz.height, vid->sz.frames, 1);
	mappingDistances.resize(vid->sz.width, vid->sz.height, vid->sz.frames, 1);

	std::default_random_engine generator;

	std::uniform_int_distribution<unsigned> x_distribution(0,vid->sz.width-psx);
	std::uniform_int_distribution<unsigned> y_distribution(0,vid->sz.height-psx);
	std::uniform_int_distribution<unsigned> t_distribution(0,vid->sz.frames-pst);

	// TODO < ou <= ici ?????
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic) num_threads(8)
#endif
	for (unsigned pt = 0; pt <= vid->sz.frames-pst; pt+=st)
	{
		for (unsigned py = 0; py <= vid->sz.height-psx; py+=sp)
			for (unsigned px = 0; px <= vid->sz.width-psx; px+=sp)
			{
				//unsigned pidx = vid->sz.index(px,py,pt,0);
				//std::random_shuffle(allPatches.begin(), allPatches.end(), rand_gen);
				//for(int k = 0; k < kNN; ++k)
				//{
				//	guessId[k] = allPatches[k];
				//	guessDistances[k] = pm->distance(pidx, allPatches[k]);
				//}
				//mappingId(pidx) = guessId;
				//mappingDistances(pidx) = guessDistances;


				// TODO check if we can draw kNN elements at the same time (eviter de faire un shuffling sur tous les elements ce qui est extremement long !!!!!)
				unsigned pidx_vid = vid->sz.index(px,py,pt,0);
				unsigned pidx_mapping = mappingId.sz.index(px,py,pt,0);
				mappingId(pidx_mapping).resize(kNN);
				mappingDistances(pidx_mapping).resize(kNN);

				std::unordered_map<unsigned, int> mark;

				for(int temp = 0; temp < kNN; ++temp)
				{
					int newId;
					//int x_patch;
					//int y_patch;
					//int t_patch;

					do {
						int x_patch = x_distribution(generator);
						int y_patch = y_distribution(generator);
						int t_patch = t_distribution(generator);

						newId = vid->sz.index(x_patch, y_patch, t_patch, 0);
					} while(mark[newId] == 1);
					mappingId(pidx_mapping)[temp] = newId;
					mappingDistances(pidx_mapping)[temp] = pm->distance(pidx_vid, newId);
					mark[newId] = 1;
				}
			}
	}

	return 0;
}

// type = 0 -> kdtree
// type = 1 -> vptree
int Patchmatch::initializeForest(int nbTrees, bool rerank, int type, PrmTree& prm, PatchManager& pm2)
{
	usingTree = true;

	mappingId.resize(vid->sz.width, vid->sz.height, vid->sz.frames);
	mappingDistances.resize(vid->sz.width, vid->sz.height, vid->sz.frames);

//	std::vector<PartitionTree*> trees; 
//	if(type == 0)
//	{
//#ifdef _OPENMP
//#pragma omp parallel for schedule(dynamic) \
//			shared(trees) 
//#endif
//		for(int i = 0; i < nbTrees; ++i)
//		{
//			trees.push_back(new KDTree(pm2, prm));	
//		}
//	}
//	else if(type == 1)
//	{
//#ifdef _OPENMP
//#pragma omp parallel for schedule(dynamic) \
//			shared(trees) 
//#endif
//		for(int i = 0; i < nbTrees; ++i)
//		{
//			trees.push_back(new VPTree(pm2, prm));	
//		}
//	}
//	else
//	{
//		return -1;
//	}
//
//	forest = ForestManager(pm2, trees, kNN, prm.prms);
			forest = new KDTree(pm2, prm);	

#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic) num_threads(8)
#endif
	for (unsigned pt = 0; pt <= vid->sz.frames-pst; pt+=st)
		for (unsigned py = 0; py <= vid->sz.height-psx; py+=sp)
			for (unsigned px = 0; px <= vid->sz.width-psx; px+=sp)
			{
				unsigned pidx_vid = vid->sz.index(px,py,pt,0);
				unsigned pidx_mapping = mappingId.sz.index(px,py,pt,0);
				mappingId(pidx_mapping).resize(kNN);
				mappingDistances(pidx_mapping).resize(kNN);
				//forest.retrieveFromForest(mappingDistances(pidx_mapping), mappingId(pidx_mapping), pidx_vid, rerank);
				forest->estimateDistanceSimilarPatchesStep1(mappingDistances(pidx_mapping), mappingId(pidx_mapping), pidx_vid, true);
			}
	return 0;
}

void Patchmatch::propagate()
{
	for (unsigned pt = 0; pt <= vid->sz.frames-pst; pt+=st)
		for (unsigned py = 0; py <= vid->sz.height-psx; py+=sp)
			for (unsigned px = 0; px <= vid->sz.width-psx; px+=sp)
			{
				unsigned pidx = vid->sz.index(px,py,pt,0);
				// Propagate to the right
				if(px + sp < vid->sz.width - psx)
				{
					unsigned next = vid->sz.index(px+sp,py,pt,0);
					propToId(pidx, next, 0);
				}

				// Propagate to the bottom
				if(py + sp < vid->sz.height - psx)
				{
					unsigned next = vid->sz.index(px,py+sp,pt,0);
					propToId(pidx, next, 1);
				}

				// Propagate to the temporally next
				if(pt + st < vid->sz.frames - pst) 
				{
					unsigned next = vid->sz.index(px,py,pt+st,0);
					propToId(pidx, next, 2);
				}
			}
}

void Patchmatch::backpropagate()
{
	for (unsigned pt = ((unsigned)((vid->sz.frames-pst) / st)) * st; pt >= st; pt-=st)
		for (unsigned py = ((unsigned)((vid->sz.height-psx) / sp)) * sp; py >= sp; py-=sp)
			for (unsigned px = ((unsigned)((vid->sz.height-psx) / sp)) * sp; px >= sp; px-=sp)
			{
				unsigned pidx = vid->sz.index(px,py,pt,0);
				// Propagate to the left
				if(px - sp >= 0)
				{
					unsigned next = vid->sz.index(px-sp,py,pt,0);
					propToId(pidx, next, 3);
				}

				// Propagate to the top
				if(py - sp >= 0)
				{
					unsigned next = vid->sz.index(px,py-sp,pt,0);
					propToId(pidx, next, 4);
				}

				// Propagate to the temporally previous
				if(pt - st >= 0) 
				{
					unsigned next = vid->sz.index(px,py,pt-st,0);
					propToId(pidx, next, 5);
				}
			}
}

void Patchmatch::propagateNtimes(int N)
{
	for(int i = 0; i < N; ++i)
	{
		if(i % 2 == 0)
			propagate();
		else
			backpropagate();

		if(!usingTree)
			randomSearch();
	}	
}

void Patchmatch::extract(std::vector<unsigned>& output, std::vector<float>& distances, unsigned pidx)
{
	unsigned cx,cy,ct,cc;

	vid->sz.coords(pidx, cx,cy,ct,cc);

	unsigned pidx_mapping = mappingId.sz.index(cx,cy,ct,0);
	std::vector<unsigned>* temp = &mappingId(pidx_mapping);
	std::vector<float>* temp2 = &mappingDistances(pidx_mapping);
	for(int i = 0; i < kNN; ++i)
	{
		output[i] = (*temp)[i];
		distances[i] = (*temp2)[i];
	}
}

void Patchmatch::propToId(unsigned currentId, unsigned nextId, int type)
{
	std::vector<std::pair<float,unsigned> > joust;
	std::unordered_map<unsigned, int> mark;
	
	unsigned cx,cy,ct,cc;
	unsigned nx,ny,nt,nc;

	vid->sz.coords(currentId, cx,cy,ct,cc);
	vid->sz.coords(nextId, nx,ny,nt,nc);

	unsigned currentId_mapping = mappingId.sz.index(cx,cy,ct,0);
	unsigned nextId_mapping = mappingId.sz.index(nx,ny,nt,0);

	std::vector<unsigned>* tempId = &mappingId(nextId_mapping);
	std::vector<float>* tempDist = &mappingDistances(nextId_mapping);

	// load the previous elements
	for(int i = 0; i < kNN; ++i)
	{
		joust.push_back(std::make_pair((*tempDist)[i], (*tempId)[i]));	
		mark[(*tempId)[i]] = 1;
	}

	tempId = &mappingId(currentId_mapping);
	tempDist = &mappingDistances(currentId_mapping);
	// load the new elements
	for(int i = 0; i < kNN; ++i)
	{
		unsigned newId = pm->getNeighboringPatch((*tempId)[i], type, sp, st);
		if(mark[newId] == 0)
		{
			joust.push_back(std::make_pair(pm->distance(nextId, newId), newId));	
			mark[newId] = 1;
		}
	}

	// propagate a bin as well just as in prop-ass tree
	if(usingTree)
	{
		unsigned usefulId;
		if((*tempId)[0] == currentId)
		{
			usefulId = (*tempId)[1];
		}
		else
			usefulId = (*tempId)[0];

		usefulId = pm->getNeighboringPatch(usefulId, type, sp, st);

		std::vector<unsigned> index(kNN);
		std::vector<float> output(kNN);
		
		//int nbTrees = forest.getNbTrees();
		//std::vector<unsigned> cbin(nbTrees);
		//std::vector<std::vector<unsigned> > bins(nbTrees, std::vector<unsigned>(1));
		//forest.getBins(cbin, usefulId);
		//for(int j = 0; j < nbTrees; ++j)
		//{
		//	bins[j][0] = cbin[j];
		//}

		//int finalValue = forest.estimateClassicFromBins(output, index, bins, nextId, true, false);
		int finalValue = forest->estimateClassicFromBins(output, index, std::vector<unsigned>(1, forest->whichBin(usefulId)), nextId, true);

		for(int it = 0; it < kNN; ++it)
		{
			joust.push_back(std::make_pair(output[it], index[it]));	
		}

	}

	tempId = &mappingId(nextId_mapping);
	tempDist = &mappingDistances(nextId_mapping);
	// Keep the kNN of the elements
	std::partial_sort(joust.begin(), joust.begin() + kNN, joust.end(), comparaisonFirst);
	for(int i = 0; i < kNN; ++i)
	{
		(*tempId)[i] = joust[i].second;
		(*tempDist)[i] = joust[i].first;	
	}
}

// Do a small world graph search based on the patchmatch initialization
int Patchmatch::swSearch(std::vector<unsigned>& output, std::vector<float>& distances, unsigned idx, int nbNN, float epsilon)
{
	// Here we use the property that if 
	std::unordered_map<unsigned, int> visited; 
	visited[idx] = 1;
	std::priority_queue<std::pair<float, unsigned>, std::vector<std::pair<float, unsigned> >, ComparePairsInv> currentNodes;
	currentNodes.push(std::make_pair(0.f, idx));
	std::priority_queue<std::pair<float, unsigned>, std::vector<std::pair<float, unsigned> >, ComparePairs> result;
	result.push(std::make_pair(0.f, idx));
	volatile int totalNodeVisited = 0;

	while(!currentNodes.empty() && //Stop criterion TODO
			(result.size() < nbNN ||
			//totalNodeVisited < 2 * nbNN
			result.top().first > currentNodes.top().first ||
			result.top().first < epsilon)//Trying some new stop criterion
 		)
	{
		unsigned currentNode = currentNodes.top().second;
		currentNodes.pop();

		unsigned px,py,pt,pc;
		vid->sz.coords(currentNode, px,py,pt,pc);
		unsigned mapping_currentNode = mappingId.sz.index(px,py,pt,0);

		std::vector<unsigned>* list_edges = &mappingId(mapping_currentNode);
		for(int i = 0; i < list_edges->size(); ++i)
		{
			if(visited[(*list_edges)[i]] == 0)
			{
				visited[(*list_edges)[i]] = 1;
				float dist = pm->distance(idx, (*list_edges)[i]); 
				currentNodes.push(std::make_pair(dist, (*list_edges)[i]));
				result.push(std::make_pair(dist, (*list_edges)[i]));
				if(result.size() > nbNN && result.top().first >= epsilon)
					result.pop();

				totalNodeVisited++;
			}
		}
	}

	//nbNN = std::min(nbNN, (int) result.size());
	nbNN = result.size();
	if(output.size() < nbNN)
	{
		output.resize(nbNN);
	}
	if(distances.size() < nbNN)
	{
		distances.resize(nbNN);
	}
	for(int i = 0; i < nbNN; ++i)
	{
		std::pair<float, unsigned> cur = result.top();
		result.pop();
		output[nbNN - i - 1] = cur.second;
		distances[nbNN - i - 1] = cur.first;
	}

	return nbNN;
}

void Patchmatch::randomSearch()
{
	int nbPatches = pm->getNbPatches();
	std::vector<unsigned> allPatches(nbPatches);
	pm->getAllPatches(allPatches);

//	std::vector<unsigned> guessId(kNN);
//	std::vector<float> guessDistances(kNN);

	std::default_random_engine generator;

#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic) num_threads(8)
#endif
	for (unsigned pt = 0; pt <= vid->sz.frames-pst; pt+=st)
		for (unsigned py = 0; py <= vid->sz.height-psx; py+=sp)
			for (unsigned px = 0; px <= vid->sz.width-psx; px+=sp)
			{
				int init_size_search_region = std::max(std::max(vid->sz.width, vid->sz.height), vid->sz.frames); 

				unsigned pidx_vid = vid->sz.index(px,py,pt,0);
				unsigned pidx_mapping = mappingId.sz.index(px,py,pt,0);

				std::vector<std::pair<float,unsigned> > joust;
				std::unordered_map<unsigned, int> mark;

				std::vector<unsigned>* tempId = &mappingId(pidx_mapping);
				std::vector<float>* tempDist = &mappingDistances(pidx_mapping);

				for(int i = 0; i < kNN; ++i)
				{
					joust.push_back(std::make_pair((*tempDist)[i], (*tempId)[i]));	
					mark[(*tempId)[i]] = 1;
				}

				for (int sz_sr = init_size_search_region; sz_sr >= 1; sz_sr /= 2) 
				{
					/* Sampling window */
					volatile unsigned x_min = std::max((int)px-sz_sr, 0), x_max = std::min(px+sz_sr+1,vid->sz.width - psx - 1);
					volatile unsigned y_min = std::max((int)py-sz_sr, 0), y_max = std::min(py+sz_sr+1,vid->sz.height - psx - 1);
					volatile unsigned t_min = std::max((int)pt-sz_sr, 0), t_max = std::min(pt+sz_sr+1,vid->sz.frames - pst - 1);
					std::uniform_int_distribution<unsigned> x_distribution(x_min,x_max);
					std::uniform_int_distribution<unsigned> y_distribution(y_min,y_max);
					std::uniform_int_distribution<unsigned> t_distribution(t_min,t_max);

					for(int temp = 0; temp < kNN; ++temp)
					{
						unsigned x_patch = x_distribution(generator);
						unsigned y_patch = y_distribution(generator);
						unsigned t_patch = t_distribution(generator);

						unsigned newId = vid->sz.index(x_patch, y_patch, t_patch, 0);
						if(mark[newId] == 0)
						{
							joust.push_back(std::make_pair(pm->distance(pidx_vid, newId), newId));	
							mark[newId] = 1;
						}
					}
				}

				std::partial_sort(joust.begin(), joust.begin() + kNN, joust.end(), comparaisonFirst);
				for(int i = 0; i < kNN; ++i)
				{
					(*tempId)[i] = joust[i].second;
					(*tempDist)[i] = joust[i].first;	
				}
			}
}

