#include "patchmatchBin.h"

static int rand_gen(int i)
{
	return std::rand() % i;
}

PatchmatchBin::PatchmatchBin(Video<float>& v, PatchManager& pm_, int kNN_, int sp_, int st_, unsigned startX_, unsigned startY_, unsigned startT_, unsigned width_, unsigned height_, unsigned frames_)
{
	vid = &v;
	pm = &pm_;
	kNN = kNN_;

	sp = sp_;
	st = st_;

	startX = startX_;
	startY = startY_;
	startT = startT_;

	width = width_;
	height = height_;
	frames = frames_;
}

// type = 0 -> kdtree
// type = 1 -> vptree
int PatchmatchBin::initializeForest(int nbTrees, bool rerank, int type, PrmTree& prm)
{
	std::vector<PartitionTree*> trees; 
	if(type == 0)
	{
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic) \
			shared(trees) 
#endif
		for(int i = 0; i < nbTrees; ++i)
		{
			trees.push_back(new KDTree(*pm, prm));	
		}

	}
	else if(type == 1)
	{
#ifdef _OPENMP
#pragma omp parallel for schedule(dynamic) \
			shared(trees) 
#endif
		for(int i = 0; i < nbTrees; ++i)
		{
			trees.push_back(new VPTree(*pm, prm));	
		}
	}
	else
	{
		return -1;
	}

	fm = new ForestManager(*pm, trees, kNN, prm.prms);
	std::vector<unsigned> guessId(kNN);
	std::vector<float> guessDistances(kNN);

	for (unsigned pt = startT; pt <= frames; pt+=st)
		for (unsigned py = startY; py <= height; py+=sp)
			for (unsigned px = startX; px <= width ; px+=sp)
			{
				unsigned pidx = vid->sz.index(px,py,pt,0);
				fm->estimateDistanceSimilarPatchesStep1(guessDistances, guessId, pidx, rerank);
				mappingId[pidx] = guessId;
				mappingDistances[pidx] = guessDistances;
			}
	return 0;
}

void PatchmatchBin::propagate()
{
	for (unsigned pt = startT; pt <= frames-st; pt+=st)
		for (unsigned py = startY; py <= height-sp; py+=sp)
			for (unsigned px = startX; px <= width-sp; px+=sp)
			{
				unsigned pidx = vid->sz.index(px,py,pt,0);
				// Propagate to the right
				if(px + sp < vid->sz.width)
				{
					unsigned next = vid->sz.index(px+sp,py,pt,0);
					propToId(pidx, next, 0);
				}

				// Propagate to the bottom
				if(py + sp < vid->sz.height)
				{
					unsigned next = vid->sz.index(px,py+sp,pt,0);
					propToId(pidx, next, 1);
				}

				// Propagate to the temporally next
				if(pt + st < vid->sz.frames) 
				{
					unsigned next = vid->sz.index(px,py,pt+st,0);
					propToId(pidx, next, 2);
				}
			}
}

void PatchmatchBin::backpropagate()
{
	for (unsigned pt = frames; pt >= startT + st; pt-=st)
		for (unsigned py = height; py >= startY + sp; py-=sp)
			for (unsigned px = width; px >= startX + sp; px-=sp)
			{
				unsigned pidx = vid->sz.index(px,py,pt,0);
				// Propagate to the right
				if(px - sp >= 0)
				{
					unsigned next = vid->sz.index(px-sp,py,pt,0);
					propToId(pidx, next, 3);
				}

				// Propagate to the bottom
				if(py - sp >= 0)
				{
					unsigned next = vid->sz.index(px,py-sp,pt,0);
					propToId(pidx, next, 4);
				}

				// Propagate to the temporally next
				if(pt - st >= 0) 
				{
					unsigned next = vid->sz.index(px,py,pt-st,0);
					propToId(pidx, next, 5);
				}
			}
}

void PatchmatchBin::propagateNtimes(int N)
{
	printf("Start propagate\n");
	for(int i = 0; i < N; ++i)
	{
		propagate();
		backpropagate();
	}	
}

void PatchmatchBin::extract(std::vector<unsigned>& output, std::vector<float>& distances, unsigned pidx)
{
	std::vector<unsigned>* temp = &mappingId[pidx];
	std::vector<float>* temp2 = &mappingDistances[pidx];
	for(int i = 0; i < kNN; ++i)
	{
		output[i] = (*temp)[i];
		distances[i] = (*temp2)[i];
	}
}

void PatchmatchBin::propToId(unsigned currentId, unsigned nextId, int type)
{
	std::priority_queue<std::pair<float,unsigned> , std::vector<std::pair<float, unsigned> >, ComparePairs> joust;
	std::unordered_map<unsigned, int> mark;

	std::vector<unsigned>* tempId = &mappingId[nextId];
	std::vector<float>* tempDist = &mappingDistances[nextId];
	// load the previous elements
	for(int i = 0; i < kNN; ++i)
	{
		joust.push(std::make_pair((*tempDist)[i], (*tempId)[i]));	
		mark[(*tempId)[i]] = 1;
		if(joust.size() > kNN)
			joust.pop();
	}

	tempId = &mappingId[currentId];
	tempDist = &mappingDistances[currentId];

	std::vector<float> output(kNN);
	std::vector<unsigned> index(kNN);
	// load the new elements
	for(int i = 0; i < kNN; ++i)
	{
		unsigned newId = pm->getNeighboringPatch((*tempId)[i], type, sp, st);
		std::vector<std::vector<unsigned> > cbin(fm->getNbTrees(), std::vector<unsigned>(1));
		std::vector<unsigned> cbin2(fm->getNbTrees());
	        fm->getBins(cbin2, newId);
		for(int j = 0; j < fm->getNbTrees(); ++j)
		{
			cbin[j][0] = cbin2[j];
			//printf("bin %d\n", cbin[j][0]);
		}

		fm->estimateClassicFromBins(output, index, cbin, nextId, false);
		for(int j = 0; j < kNN; ++j)
		{
			if(mark[index[j]] == 0)
			{
				joust.push(std::make_pair(output[j], index[j]));	
				mark[index[j]] = 1;
				if(joust.size() > kNN)
					joust.pop();
			}
		}
		//printf("newId ? %d %f %f\n", mark[newId], pm->distance(newId, nextId), joust.top().first);
	}

	tempId = &mappingId[nextId];
	tempDist = &mappingDistances[nextId];
	// Keep the kNN of the elements
	//std::partial_sort(joust.begin(), joust.begin() + kNN, joust.end(), comparaisonFirst);
	for(int i = 0; i < kNN; ++i)
	{
		(*tempId)[kNN - 1 - i] = joust.top().second;
		(*tempDist)[kNN - 1 - i] = joust.top().first;	
		joust.pop();
	}
}
