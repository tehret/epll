#include "propagationGraph.h"

void PropGraph::insertNode(unsigned idx)
{
	std::pair<float, unsigned> currentNode = std::make_pair(pm->distance(idx, entryPoint), entryPoint);

	std::default_random_engine generator;
	std::exponential_distribution<float> distrib(std::log(connectivity)); 

	int firstLevel = std::max(0, maxLevel - (int) std::floor(distrib(generator))); // TODO

	// Coarse levels < find the best current node at each level
	for(int lvl = 0; lvl < (firstLevel - 1); ++lvl) 
	{
		currentNode = exploreLevel(currentNode, idx, lvl);
	}

	// Coarse levels where we need to insert the point
	for(int lvl = firstLevel; lvl < (maxLevel - 1); ++lvl)
	{
		// explore the level 
		currentNode = updateLevel(currentNode, idx, lvl);
	}
	// fine level < find best links for the new node
	updateFineLevel(currentNode, idx);
}

std::pair<float, unsigned> PropGraph::exploreLevel(std::pair<float, unsigned> initNode, unsigned idx, int level)
{
	unsigned bestCandidate = initNode.first;
	float minDist = initNode.second;

	std::priority_queue<std::pair<float, unsigned>, std::vector<std::pair<float, unsigned> >, ComparePairs> currentNodes;
	currentNodes.push(initNode);
	std::unordered_map<unsigned, bool> visited; 
	visited[initNode.first] = true;


	int totalNodeVisited = 0;

	while(!currentNodes.empty() && //Stop criterion TODO
			totalNodeVisited < maxNodeVisited
			)
	{
		std::vector<unsigned>* list_edges = &edges[level][currentNodes.top().first];
		for(int i = 0; i < list_edges->size(); ++i)
		{
			if(!visited[(*list_edges)[i]])
			{
				visited[(*list_edges)[i]] = true;
				float dist = pm->distance(idx, (*list_edges)[i]); 
				currentNodes.push(std::make_pair((*list_edges)[i], dist));
				if(dist < minDist)
				{
					bestCandidate = (*list_edges)[i];
					minDist = dist;
				}

				totalNodeVisited++;
			}
		}
	}

	return std::make_pair(minDist, bestCandidate);
}

std::pair<float, unsigned> PropGraph::updateLevel(std::pair<float, unsigned> initNode, unsigned idx, int level)
{
	std::priority_queue<std::pair<float, unsigned>, std::vector<std::pair<float, unsigned> >,ComparePairs> currentNodes;
	currentNodes.push(initNode);
	std::unordered_map<float, unsigned> visited; 
	visited[initNode.first] = true;

	std::priority_queue<std::pair<float, unsigned>, std::vector<std::pair<float, unsigned> >,ComparePairsInv> result;
	int totalNodeVisited = 0;

	while(!currentNodes.empty() && //Stop criterion TODO
			totalNodeVisited < maxNodeVisited
			)
	{
		unsigned currentNode = currentNodes.top().first;
		currentNodes.pop();
		std::vector<unsigned>* list_edges = &edges[level][currentNode];
		for(int i = 0; i < list_edges->size(); ++i)
		{
			if(!visited[(*list_edges)[i]])
			{
				visited[(*list_edges)[i]] = true;
				float dist = pm->distance(idx, (*list_edges)[i]); 
				currentNodes.push(std::make_pair(dist, (*list_edges)[i]));
				result.push(std::make_pair(dist, (*list_edges)[i]));
				if(result.size() > connectivity)
					result.pop();

				totalNodeVisited++;
			}
		}
	}

	std::vector<unsigned> links(connectivity);
	for(int i = 0; i < connectivity; ++i)
	{
		links[connectivity - i - 1] = result.top().first;
		if(i < (connectivity - 1))
			result.pop();
	}
	links[0] = result.top().first;

	return result.top();
}

void PropGraph::updateFineLevel(std::pair<float, unsigned> initNode, unsigned idx)
{
	std::priority_queue<std::pair<float, unsigned>, std::vector<std::pair<float, unsigned> >,ComparePairs> currentNodes;
	currentNodes.push(initNode);
	std::unordered_map<unsigned, bool> visited; 
	visited[initNode.first] = true;

	std::priority_queue<std::pair<float, unsigned>, std::vector<std::pair<float, unsigned> >,ComparePairsInv> result;
	int totalNodeVisited = 0;

	while(!currentNodes.empty() && //Stop criterion TODO
			totalNodeVisited < maxNodeVisited
			)
	{
		unsigned currentNode = currentNodes.top().first;
		currentNodes.pop();
		std::vector<unsigned>* list_edges = &edgesFineLevel(currentNode);
		for(int i = 0; i < list_edges->size(); ++i)
		{
			if(!visited[(*list_edges)[i]])
			{
				visited[(*list_edges)[i]] = true;
				float dist = pm->distance(idx, (*list_edges)[i]); 
				currentNodes.push(std::make_pair(dist, (*list_edges)[i]));
				result.push(std::make_pair(dist, (*list_edges)[i]));
				if(result.size() > connectivity)
					result.pop();

				totalNodeVisited++;
			}
		}
	}

	std::vector<unsigned> links(connectivity);
	for(int i = 0; i < connectivity; ++i)
	{
		links[connectivity - i - 1] = result.top().first;
		if(i < (connectivity - 1))
			result.pop();
	}
	links[0] = result.top().first;
	return;
}


int PropGraph::searchNearestNeighbors(unsigned idx, int kNN, std::vector<unsigned> indexes, std::vector<float> distances)
{
	// Here we use the property that if 
	std::unordered_map<unsigned, bool> visited; 
	visited[idx] = true;
	std::priority_queue<std::pair<float, unsigned>, std::vector<std::pair<float, unsigned> >, ComparePairs> currentNodes;
	currentNodes.push(std::make_pair(pm->distance(idx, entryPoint), idx));
	std::priority_queue<std::pair<float, unsigned>, std::vector<std::pair<float, unsigned> >,ComparePairsInv> result;
	int totalNodeVisited = 0;


	while(!currentNodes.empty() && //Stop criterion TODO
			totalNodeVisited < maxNodeVisited
			)
	{
		unsigned currentNode = currentNodes.top().first;
		currentNodes.pop();
		std::vector<unsigned>* list_edges = &edgesFineLevel(currentNode);
		for(int i = 0; i < list_edges->size(); ++i)
		{
			if(!visited[(*list_edges)[i]])
			{
				visited[(*list_edges)[i]] = true;
				float dist = pm->distance(idx, (*list_edges)[i]); 
				currentNodes.push(std::make_pair(dist, (*list_edges)[i]));
				result.push(std::make_pair(dist, (*list_edges)[i]));
				if(result.size() > kNN)
					result.pop();

				totalNodeVisited++;
			}
		}
	}
	for(int i = 0; i < kNN; ++i)
	{
		std::pair<float, unsigned> cur = result.top();
		result.pop();
		indexes[kNN - i - 1] = cur.first;
		distances[kNN - i - 1] = cur.second;
	}

	return kNN;
}
