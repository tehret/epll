//
// TOP
//

#ifndef PGRAPH_HEADER_H
#define PGRAPH_HEADER_H

#include "PartitionTree/partitionTree.h"
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <queue>
#include "Utilities.h"
#include "patchManager.h"
#include "../NlBayes/nlbParams.h"
#include "prmTree.h"
#include <cfloat>
#include <stack>

#include "comparators.h"

// FIXME We might need some comparaison class used in partitionTree.h and forestManager.h


class PropGraph
{
	public:
		// Create a propagation graph from scratch
		PropGraph(PatchManager& _pm, int _maxLevels, int _connec, int _connecFine, int _maxNodeVisited) {pm = &_pm; maxLevel = _maxLevels; connectivity = _connec; connectivityFine = _connecFine; maxNodeVisited = _maxNodeVisited;};

		// Insert a node into the propagation graph
		// It requires that the corresponding index exist in the patchmanager
		void insertNode(unsigned idx);

		// Not implemented currently
		int deleteNode(unsigned idx) {};

		int searchNearestNeighbors(unsigned idx, int kNN, std::vector<unsigned> indexes, std::vector<float> distances);

	private:

		std::pair<float, unsigned> exploreLevel(std::pair<float, unsigned> initNode, unsigned idx, int level);
		std::pair<float, unsigned> updateLevel(std::pair<float, unsigned> initNode, unsigned idx, int level);
		void updateFineLevel(std::pair<float, unsigned> initNode, unsigned idx);

		Video<std::vector<unsigned> > edgesFineLevel;
		std::vector<std::unordered_map<unsigned, std::vector<unsigned> > > edges; 

		unsigned entryPoint;

		PatchManager* pm;
		int maxLevel;

		int connectivity;
		int connectivityFine;


		// Might be useless
		int nbPSaved;

		int maxNodeVisited;
};

#endif
